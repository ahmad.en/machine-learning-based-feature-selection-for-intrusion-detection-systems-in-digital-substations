"""
This module implements common functions for generating SV (Sampled Values) and MMS (Manufacturing Message Specification) datasets from PCAP captures.
These include, for example, functions for extracting columns from CSV files, printing metadata about the dataset,
shuffling and concatenating data points, etc.

This module has been created as part of a bachelor's thesis on the topic
"Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations".
The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.
The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:
https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.

Caution: Please note that the code in this module does not include extensive input validation or error checking.
The inputs and function parameters have been provided by the author in a controlled environment.
Consider adding error checking and handling functionalities as needed.

Disclaimer: This code is provided "as-is", without any express or implied warranty. In no event shall the author or
contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.

Author: Ahmad Eynawi
Institute: Institute for Automation and Applied Informatics (IAI)
Department: Department of Informatics
University: Karlsruhe Institute of Technology (KIT)
Date: April 10, 2024
"""


import csv
import os
import glob
import pandas as pd
import random
random.seed(42)
import numpy as np



def concatenate_csv_files(csv_source_directories, csv_destination_file):
    """
    Concatenate multiple CSV files from specified directories into a single CSV file.

    Parameters:
    csv_source_directories (list of str): List of directories containing source CSV files.
    csv_destination_file (str): The path to save the concatenated CSV file.

    Returns:
    None
    """

    dataframes = []
    print("Concatenating csv files:")
    for directory in csv_source_directories:
        print(f"Processing csv directory: {directory}")
        for filename in os.listdir(directory):
            file_path = os.path.join(directory, filename)
            # Check if the current file is a CSV file.
            if os.path.isfile(file_path) and file_path.endswith('.csv'):
                # Read the CSV file and append it to the list.
                dataframe = pd.read_csv(file_path)
                dataframes.append(dataframe)

    # Concatenate all data frames in the list.
    concatenated_dataframe = pd.concat(dataframes, ignore_index=True)

    # Save the concatenated data frame to the destination CSV file.
    concatenated_dataframe.to_csv(csv_destination_file, index=False)


def clean_up_temporary_csv_files(temporary_csv_directories):
    """
    Remove all temporary CSV files from specified directories.

    Parameters:
    temporary_csv_directories (list of str): List of directories containing temporary CSV files.

    Returns:
    None
    """

    for temporary_directory in temporary_csv_directories:
        for temporary_file in glob.glob(os.path.join(temporary_directory, '*.csv')):
            os.remove(temporary_file)


def generate_random_number(minimum, maximum, data_type='float'):
    """
    Generate a random number within a specified range and of a specified data type.

    Parameters:
    minimum (int or float): The minimum value of the range.
    maximum (int or float): The maximum value of the range.
    data_type (str, optional): The type of the random number ('float' or 'integer'). Defaults to 'float'.

    Returns:
    int or float: The generated random number.
    """

    random_number = 0
    if data_type == 'float':
        # The minimum is inclusive, but the maximum is exclusive.
        random_number = random.uniform(minimum, maximum)
    elif data_type == 'integer':
        # Both the minimum and the maximum are inclusive.
        random_number = random.randint(minimum, maximum)

    return random_number


def generate_random_equidistant_numbers(count, start, end):
    """
    Generate a specified number of random equidistant numbers within a range.

    Parameters:
    count (int): The number of random numbers to generate.
    start (int or float): The start of the range.
    end (int or float): The end of the range.

    Returns:
    list of float: A list of generated random equidistant numbers.
    """

    # Calculate the size of each segment.
    segment_size = (end - start) / count

    # Generate a random float within each segment.
    random_points = [np.random.uniform(low=start + i * segment_size, high=start + (i + 1) * segment_size) for i in
                     range(count)]

    return random_points


def sample_elements_without_replacement(elements_list, number_of_elements_to_sample):
    """
    Sample a specified number of elements from a list without replacement.

    Parameters:
    elements_list (list): The list of elements to sample from.
    number_of_elements_to_sample (int): The number of elements to sample.

    Returns:
    list: A list of sampled elements.
    """

    sampled_elements = random.sample(population=elements_list, k=number_of_elements_to_sample)
    return sampled_elements


def remove_columns(path_csv_file, columns_to_remove):
    """
    Remove specified columns from a CSV file.

    Parameters:
    path_csv_file (str): The path to the CSV file.
    columns_to_remove (list of str): List of column names or substrings to identify columns to be removed.

    Returns:
    None
    """

    # Load the CSV file.
    dataframe = pd.read_csv(path_csv_file)

    # Exclude a column if its name matches or is included in any string in the given list.
    filtered_columns = [column for column in dataframe.columns if
                        all(column != match for match in columns_to_remove) and all(
                            substring not in column for substring in columns_to_remove)]

    # Select only the filtered columns.
    dataframe_filtered = dataframe[filtered_columns]

    # Save the modified DataFrame back to the CSV file.
    dataframe_filtered.to_csv(path_csv_file, index=False)


def extract_column_from_csv(csv_file_path, column_name):
    """
    Extract a specific column from a CSV file.

    Parameters:
    csv_file_path (str): The path to the CSV file.
    column_name (str): The name of the column to extract.

    Returns:
    list: A list containing the data from the specified column.
    """

    column_data = []

    # Open the CSV file for reading.
    with open(csv_file_path, mode='r', newline='', encoding='utf-8') as csvfile:
        # Use DictReader to easily access columns by name.
        reader = csv.DictReader(csvfile)

        for row in reader:
            # Append the column data if the column name is found.
            if column_name in row:
                column_data.append(row[column_name])
            else:
                # Print an error message and exit the loop if the column name is not found.
                print(f"Column '{column_name}' not found in the CSV file!")
                break

    return column_data


def set_column_to_one_value(path_csv_file, column, value):
    """
    Modify a specified column in a CSV file to have a single value.

    Parameters:
    path_csv_file (str): The path to the CSV file.
    column (str): The name of the column to modify.
    value: The value to set for the column.

    Returns:
    None
    """

    # Load the CSV file.
    dataframe = pd.read_csv(path_csv_file)

    # Set all values in the specified column to the given value.
    dataframe[column] = value

    # Save the modified DataFrame back to a CSV file.
    dataframe.to_csv(path_csv_file, index=False)


def print_metadata(path_csv_file):
    """
    Print metadata of a CSV file, including shape, data types, general info, descriptive statistics, and head and tail of the DataFrame.

    Parameters:
    path_csv_file (str): The path to the CSV file.

    Returns:
    None
    """

    # Load the CSV file.
    dataframe = pd.read_csv(path_csv_file)

    # Shape of the DataFrame.
    print("Dataset shape (rows, columns):", dataframe.shape)

    # Data types of columns.
    print("\nData types of columns:")
    print(dataframe.dtypes)

    # General info.
    print("\nGeneral Info:")
    dataframe.info()
    print(dataframe.dtypes)

    # Descriptive statistics for numeric columns.
    print("\nDescriptive Statistics (Numeric Columns):")
    print(dataframe.describe())

    # Descriptive statistics for all columns.
    print("Descriptive Statistics (All Columns):")
    print(dataframe.describe(include='all'))

    # Print the data type of each feature.
    print("Feature \t\t\t\t\t Data Type")
    for column in dataframe.columns:
        print(f"{column} \t\t\t\t\t {dataframe[column].dtype}")

    # Head of the DataFrame.
    print("\nHead of the DataFrame:")
    print(dataframe.head())

    # Tail of the DataFrame.
    print("\nTail of the DataFrame:")
    print(dataframe.tail())
    print()


def shuffle_data(path_csv_file):
    """
    Shuffle the rows of a CSV file.

    Parameters:
    path_csv_file (str): The path to the CSV file.

    Returns:
    None
    """

    # Load the CSV file into a DataFrame.
    df = pd.read_csv(path_csv_file)

    # Shuffle the DataFrame.
    shuffled_df = df.sample(frac=1).reset_index(drop=True)

    # Save the shuffled DataFrame back to the CSV file.
    shuffled_df.to_csv(path_csv_file, index=False)


def convert_bool_feature_to_int_feature(path_csv_file, feature):
    """
    Convert a boolean feature in a CSV file to an integer feature (0 or 1).

    Parameters:
    path_csv_file (str): The path to the CSV file.
    feature (str): The name of the boolean feature to convert.

    Returns:
    None
    """

    # Load the CSV file into a DataFrame.
    df = pd.read_csv(path_csv_file)

    # Convert the boolean column to 0 or 1.
    df[feature] = df[feature].astype(int)

    # Save the modified DataFrame back to the CSV file.
    df.to_csv(path_csv_file, index=False)


def print_iterable(iterable, number_of_items_to_print):
    """
    Print a specified number of items from an iterable.

    Parameters:
    iterable (iterable): The iterable to print items from.
    number_of_items_to_print (int): The number of items to print from the iterable.

    Returns:
    None
    """

    for i in range(number_of_items_to_print):
        print(f"Item {i + 1}: {iterable[i]}")


def count_elements_in_row(csv_file_path, row_index):
    """
    Count the number of elements in a specified row of a CSV file.

    Parameters:
    csv_file_path (str): The path to the CSV file.
    row_index (int): The index of the row to count elements in.

    Returns:
    int: The number of elements in the specified row, or None if the row does not exist.
    """

    with open(csv_file_path, mode='r', newline='', encoding='utf-8') as file:
        reader = csv.reader(file)
        for i, row in enumerate(reader):
            if i == row_index:
                return len(row)
    return None