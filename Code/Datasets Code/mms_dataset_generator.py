"""
This module implements functions that are used to generate the Manufacturing Message Specification (MMS) dataset
used for building and evaluating a machine learning model to select important features for the MMS protocol.
It extracts features from MMS packets captured in PCAP files and saves the processed data in CSV format.
The dataset generation process includes converting PCAP files to CSV files, extracting measurements from raw payloads,
labeling data points, etc.

This module has been created as part of a bachelor's thesis on the topic
"Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations".
The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.
The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:
https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.

Caution: Please note that the code in this module does not include extensive input validation or error checking.
The inputs and function parameters have been provided by the author in a controlled environment.
Consider adding error checking and handling functionalities as needed.

Disclaimer: This code is provided "as-is", without any express or implied warranty. In no event shall the author or
contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.

Author: Ahmad Eynawi
Institute: Institute for Automation and Applied Informatics (IAI)
Department: Department of Informatics
University: Karlsruhe Institute of Technology (KIT)
Date: April 10, 2024
"""


import pyshark
import csv
import os
import pandas as pd
import warnings
from utility import concatenate_csv_files, clean_up_temporary_csv_files, generate_random_equidistant_numbers, \
    sample_elements_without_replacement, generate_random_number, remove_columns, print_metadata, \
    convert_bool_feature_to_int_feature, shuffle_data

# Suppress warnings related to the asynchronous handling within the Python's asyncio library,
# which arise from resources, like sockets or file descriptors, not being properly closed before
# garbage collection when using the Proactor event loop on Windows.
warnings.filterwarnings("ignore", category=ResourceWarning)


TSHARK_PATH = "C:\\Program Files\\Wireshark\\tshark.exe"

INDEX_EXAMPLE_PACKET = 0

LABEL_NORMAL_DATA = "Normal"
LABEL_FAULT_DATA = "Fault"
LABEL_MODIFICATION_ATTACK_DATA = "Modification"
LABEL_DELAY_ATTACK_DATA = "Delay"
LABELS = [LABEL_NORMAL_DATA, LABEL_FAULT_DATA, LABEL_MODIFICATION_ATTACK_DATA, LABEL_DELAY_ATTACK_DATA]
LABEL_COLUMN_NAME = "class"

# The name of the filter that should be used to search for MMS packets depends on the version of Wireshark used.
# FILTER_PROTOCOL = 'mms'
FILTER_PROTOCOL = 'iec61850'

# Names of columns manually added to the MMS dataset.
MMS_FIELD_NAMES = ['mms.current_measurement_phase_1', 'mms.current_measurement_phase_2', 'mms.current_measurement_phase_3',
                     'mms.voltage_measurement_phase_1', 'mms.voltage_measurement_phase_2', 'mms.voltage_measurement_phase_3',
                     'mms.tripping_signal', 'mms.id', 'timestamp', 'timestamp_delta']

PCAP_ONE_SAMPLE_MMS_PACKET_REAL_RTU = "..\\Captures\\MMS Captures\\MMS_Normal_Data_One_Carrier_Packet_MU_11,55kV_500A_15-03-2024.pcapng"

CSV_DIRECTORY_LABELED_NORMAL_DATA = "..\\Datasets\\MMS Datasets\\Labeled Datasets\\Normal Data"
CSV_DIRECTORY_LABELED_FAULT_DATA = "..\\Datasets\\MMS Datasets\\Labeled Datasets\\Fault Data"
CSV_DIRECTORY_LABELED_MODIFICATION_DATA = "..\\Datasets\\MMS Datasets\\Labeled Datasets\\Modification Attack Data"
CSV_DIRECTORY_LABELED_DELAY_DATA = "..\\Datasets\\MMS Datasets\\Labeled Datasets\\Delay Attack Data"
CSV_DIRECTORIES_LABELED_DATA = [CSV_DIRECTORY_LABELED_NORMAL_DATA, CSV_DIRECTORY_LABELED_FAULT_DATA, CSV_DIRECTORY_LABELED_MODIFICATION_DATA, CSV_DIRECTORY_LABELED_DELAY_DATA]

CSV_FULL_DATASET = "..\\Datasets\\MMS Datasets\\Full Dataset\\mms_normal_fault_modification_delay_full_dataset.csv"

UNNAMED_COLUMN = "Unnamed"
TRIPPING_SIGNAL_COLUMN = "mms.tripping_signal"
COLUMNS_TO_REMOVE = ['Unnamed', 'tcp.flags.str', 'tcp.payload', 'tcp.time_relative', 'tcp.time_delta',
                     'pres.dissector_not_available', '_ws.expert']

# Timestamps are provided in the unit "seconds".
# According to the captures from the lab, the time step between two subsequent measurement reports is 10 seconds.
timestamp_delta = 10.0

timestamp_normal_data = 0
current_measurement_normal_data = 0.0
current_measurement_delta_normal_data = 1.0
voltage_measurement_normal_data = 11550.0
timestamp_delta_normal_data = 10.0

timestamp_fault_data = 0
current_measurement_fault_data = 770.0
current_measurement_delta_fault_data = 1.0
voltage_measurement_fault_data = 11550.0
timestamp_delta_fault_data = 10.0

timestamp_modification_data = 0.0
timestamp_delta_modification_data = 0.0
previous_timestamp_modification_data = 0.0

timestamp_delay_data = 0.0
timestamp_delta_delay_data = 0.0
previous_timestamp_delay_data = 0.0

TRIPPING_SIGNAL_NORMAL_DATA = False
TRIPPING_SIGNAL_FAULT_DATA = True
MALICIOUS_TRIPPING_SIGNAL_NORMAL_DATA = True
MALICIOUS_TRIPPING_SIGNAL_FAULT_DATA = False

NUM_NORMAL_PACKETS = 770
NUM_FAULT_PACKETS = 770
NUM_MODIFICATION_PACKETS = 500
NUM_DELAY_PACKETS = 270

NUM_PACKETS_MODIFIED_TRIPPING_SIGNAL = 100
NUM_PACKETS_MODIFIED_MEASUREMENTS = 150

TRIPPING_THRESHOLD = 770.0

LOWER_BOUND_NORMAL_DATA = 0.0
UPPER_BOUND_NORMAL_DATA = 769.99
UPPER_BOUND_FAULT_DATA = 5000.0
LOWER_BOUND_FAULT_DATA = 770

LOWER_BOUND_VOLTAGE = 0
UPPER_BOUND_VOLTAGE = 13856.4

NUM_NORMAL_PACKETS_MODIFIED_MEASUREMENTS = 150
NUM_FAULT_PACKETS_MODIFIED_MEASUREMENTS = 150

index = 0
index_current_measurement_normal_data = 0
index_current_measurement_fault_data = 0
index_current_measurement_modification_data = 0
index_current_measurement_delay_data = 0

num_fault_packets = 0
num_modification_packets = 0
num_delay_packets = 0

mms_id_normal_data = 0
mms_id_fault_data = 0
mms_id_modification_data = 0
mms_id_delay_data = 0

# Manually generate measurements representing normal, fault, modification, and delay data.
# The generated measurements are in line with the ranges of measurements observed at the lab for each class of data.
current_measurements_normal_data = generate_random_equidistant_numbers(count=NUM_NORMAL_PACKETS, start=LOWER_BOUND_NORMAL_DATA, end=TRIPPING_THRESHOLD)
current_measurements_fault_data = generate_random_equidistant_numbers(count=NUM_FAULT_PACKETS, start=TRIPPING_THRESHOLD, end=UPPER_BOUND_FAULT_DATA)

current_measurements_normal_data_tripping_signal_modification = sample_elements_without_replacement(elements_list=current_measurements_normal_data,
                                                                                                    number_of_elements_to_sample=NUM_PACKETS_MODIFIED_TRIPPING_SIGNAL)
current_measurements_fault_data_tripping_signal_modification = sample_elements_without_replacement(elements_list=current_measurements_fault_data,
                                                                                                   number_of_elements_to_sample=NUM_PACKETS_MODIFIED_TRIPPING_SIGNAL)

current_measurements_normal_data_modification = generate_random_equidistant_numbers(count=NUM_NORMAL_PACKETS_MODIFIED_MEASUREMENTS, start=LOWER_BOUND_NORMAL_DATA, end=TRIPPING_THRESHOLD)
current_measurements_fault_data_modification = generate_random_equidistant_numbers(count=NUM_FAULT_PACKETS_MODIFIED_MEASUREMENTS, start=TRIPPING_THRESHOLD, end=UPPER_BOUND_FAULT_DATA)

current_measurements_normal_data_delay = sample_elements_without_replacement(elements_list=current_measurements_normal_data,
                                                                             number_of_elements_to_sample=int(NUM_DELAY_PACKETS / 2))
current_measurements_fault_data_delay = sample_elements_without_replacement(elements_list=current_measurements_fault_data,
                                                                            number_of_elements_to_sample=int(NUM_DELAY_PACKETS / 2))



def convert_pcap_file_to_csv_file(pcap_file, destination_directory, label, number_of_packets):
    """
    Generate MMS data based on the MMS protocol structure defined in the given PCAP file and save it to a CSV file in the specified destination directory.

    Parameters:
    pcap_file (str): The path to the PCAP file.
    destination_directory (str): The directory to save the generated CSV file.
    label (str): The label to apply to the data (e.g., normal data, fault data, modification attack data, delay attack data).
    number_of_packets (int): The number of packets to generate.

    Returns:
    None
    """

    csv_file_name = ""
    if label == LABEL_NORMAL_DATA:
        csv_file_name = "MMS_Normal_Data.csv"
    elif label == LABEL_FAULT_DATA:
        csv_file_name = "MMS_Fault_Data.csv"
    elif label == LABEL_MODIFICATION_ATTACK_DATA:
        csv_file_name = "MMS_Modification_Data.csv"
    elif label == LABEL_DELAY_ATTACK_DATA:
        csv_file_name = "MMS_Delay_Data.csv"

    csv_file = os.path.join(destination_directory, csv_file_name)
    os.makedirs(destination_directory, exist_ok=True)

    cap = pyshark.FileCapture(pcap_file, tshark_path=TSHARK_PATH, only_summaries=False, keep_packets=True)

    packet = cap[INDEX_EXAMPLE_PACKET]
    field_names = []
    for layer in packet.layers:
        field_names.extend(layer._all_fields.keys())

    with open(csv_file, mode='w', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        columns_names = field_names + MMS_FIELD_NAMES + [LABEL_COLUMN_NAME]
        writer.writerow(columns_names)

        global index
        layer_based_features = get_layer_based_features(packet=packet)

        for i in range(number_of_packets):
            values = []
            values.extend(layer_based_features)
            if label == LABEL_NORMAL_DATA:
                values.extend(get_mms_based_features_from_normal_data())
            elif label == LABEL_FAULT_DATA:
                values.extend(get_mms_based_features_from_fault_data())
            elif label == LABEL_MODIFICATION_ATTACK_DATA:
                values.extend(get_mms_based_features_from_modification_data())
            elif label == LABEL_DELAY_ATTACK_DATA:
                values.extend(get_mms_based_features_from_delay_data())

            values.append(label)
            writer.writerow(values)

    index = 0

    cap.close()


def get_layer_based_features(packet):
    """
    Extract layer-based features from a packet.

    Parameters:
    packet (pyshark.packet.Packet): The packet to extract features from.

    Returns:
    list: A list of feature values extracted from the packet layers.
    """

    values = []
    for layer in packet.layers:
        values.extend(layer._all_fields.values())
    return values


def get_mms_based_features_from_normal_data():
    """
    Generate MMS-based features for normal data.

    Returns:
    list: A list of MMS-based feature values for normal data.
    """

    global timestamp_normal_data
    global mms_id_normal_data

    values = generate_mms_based_features(
        current_measurements=current_measurements_normal_data,
        tripping_signal=TRIPPING_SIGNAL_NORMAL_DATA,
        mms_id=mms_id_normal_data,
        timestamp=timestamp_normal_data,
        balanced_load=True,
        normal_data=True,
        fault_data=False
    )

    timestamp_normal_data += timestamp_delta
    mms_id_normal_data += 1
    return values


def get_mms_based_features_from_fault_data():
    """
    Generate MMS-based features for fault data.

    Returns:
    list: A list of MMS-based feature values for fault data.
    """

    global timestamp_fault_data
    global mms_id_fault_data
    global num_fault_packets
    values = []

    if 0 <= num_fault_packets < int(NUM_FAULT_PACKETS / 2):
        values = generate_mms_based_features(
            current_measurements=current_measurements_fault_data,
            tripping_signal=TRIPPING_SIGNAL_FAULT_DATA,
            mms_id=mms_id_fault_data,
            timestamp=timestamp_fault_data,
            balanced_load=True,
            normal_data=False,
            fault_data=True
        )

    elif int(NUM_FAULT_PACKETS / 2) <= num_fault_packets < NUM_FAULT_PACKETS:
        values = generate_mms_based_features(
            current_measurements=current_measurements_fault_data,
            tripping_signal=TRIPPING_SIGNAL_FAULT_DATA,
            mms_id=mms_id_fault_data,
            timestamp=timestamp_fault_data,
            balanced_load=False,
            normal_data=False,
            fault_data=True
        )

    timestamp_fault_data += timestamp_delta
    mms_id_fault_data += 1
    num_fault_packets += 1
    return values


def get_mms_based_features_from_modification_data():
    """
    Generate MMS-based features for modification attack data.

    Returns:
    list: A list of MMS-based feature values for modification attack data.
    """

    global timestamp_modification_data
    global previous_timestamp_modification_data
    global timestamp_delta_modification_data
    global mms_id_modification_data
    global index
    global num_modification_packets
    values = []

    if previous_timestamp_modification_data == timestamp_modification_data:
        timestamp_delta_modification_data = 0.0
    else:
        timestamp_delta_modification_data = abs(timestamp_modification_data - previous_timestamp_modification_data)

    previous_timestamp_modification_data = timestamp_modification_data

    if num_modification_packets in [100, 200, 350, 500]:
        index = 0

    if 0 <= num_modification_packets < 100:
        values = generate_mms_based_features(
            current_measurements=current_measurements_normal_data_tripping_signal_modification,
            tripping_signal=MALICIOUS_TRIPPING_SIGNAL_NORMAL_DATA,
            mms_id=mms_id_modification_data,
            timestamp=timestamp_modification_data,
            timestamp_delta=timestamp_delta_modification_data,
            balanced_load=True,
            normal_data=True,
            fault_data=False
        )

    if 100 <= num_modification_packets < 200:
        values = generate_mms_based_features(
            current_measurements=current_measurements_fault_data_tripping_signal_modification,
            tripping_signal=MALICIOUS_TRIPPING_SIGNAL_FAULT_DATA,
            mms_id=mms_id_modification_data,
            timestamp=timestamp_modification_data,
            timestamp_delta=timestamp_delta_modification_data,
            balanced_load=True,
            normal_data=False,
            fault_data=True
        )

    if 200 <= num_modification_packets < 350:
        values = generate_mms_based_features(
            current_measurements=current_measurements_normal_data_modification,
            tripping_signal=MALICIOUS_TRIPPING_SIGNAL_NORMAL_DATA,
            mms_id=mms_id_modification_data,
            timestamp=timestamp_modification_data,
            timestamp_delta=timestamp_delta_modification_data,
            balanced_load=False,
            normal_data=True,
            fault_data=False
        )

    if 350 <= num_modification_packets < 500:
        values = generate_mms_based_features(
            current_measurements=current_measurements_fault_data_modification,
            tripping_signal=MALICIOUS_TRIPPING_SIGNAL_FAULT_DATA,
            mms_id=mms_id_modification_data,
            timestamp=timestamp_modification_data,
            timestamp_delta=timestamp_delta_modification_data,
            balanced_load=False,
            normal_data=False,
            fault_data=True
        )

    timestamp_modification_data += timestamp_delta
    num_modification_packets += 1
    mms_id_modification_data += 1
    if num_modification_packets == 500:
        index = 0

    return values


def get_mms_based_features_from_delay_data():
    """
    Generate MMS-based features for delay attack data.

    Returns:
    list: A list of MMS-based feature values for delay attack data.
    """

    global timestamp_delay_data
    global previous_timestamp_delay_data
    global timestamp_delta_delay_data
    global mms_id_delay_data
    global num_delay_packets
    global index
    values = []

    if previous_timestamp_delay_data == timestamp_delay_data:
        timestamp_delta_delay_data = 0
    else:
        timestamp_delta_delay_data = abs(timestamp_delay_data - previous_timestamp_delay_data)

    previous_timestamp_delay_data = timestamp_delay_data

    if num_delay_packets == int(NUM_DELAY_PACKETS / 2):
        index = 0

    if num_delay_packets in range(0, int(NUM_DELAY_PACKETS / 2)):
        values = generate_mms_based_features(
            current_measurements=current_measurements_normal_data_delay,
            tripping_signal=TRIPPING_SIGNAL_NORMAL_DATA,
            mms_id=mms_id_delay_data,
            timestamp=timestamp_delay_data,
            timestamp_delta=timestamp_delta_delay_data,
            balanced_load=True,
            normal_data=True,
            fault_data=False
        )

    elif num_delay_packets in range(int(NUM_DELAY_PACKETS / 2), NUM_DELAY_PACKETS):
        values = generate_mms_based_features(
            current_measurements=current_measurements_fault_data_delay,
            tripping_signal=TRIPPING_SIGNAL_FAULT_DATA,
            mms_id=mms_id_delay_data,
            timestamp=timestamp_delay_data,
            timestamp_delta=timestamp_delta_delay_data,
            balanced_load=True,
            normal_data=False,
            fault_data=True
        )

    timestamp_delay_data += timestamp_delta + generate_random_number(minimum=1, maximum=6, data_type='float')
    num_delay_packets += 1
    mms_id_delay_data += 1
    return values


def generate_mms_based_features(current_measurements, tripping_signal, mms_id, balanced_load, normal_data,
                                fault_data, timestamp, timestamp_delta=timestamp_delta):
    """
    Generate MMS-based features for the given parameters. These features include current and voltage measurements
    and a tripping signal.

    Parameters:
    current_measurements (list of float): List of current measurements.
    tripping_signal (float): The tripping signal value.
    mms_id (int): The MMS ID.
    balanced_load (bool): Flag indicating if the load is balanced.
    normal_data (bool): Flag indicating if the data is normal.
    fault_data (bool): Flag indicating if the data is fault data.
    timestamp (float): The timestamp of the data.
    timestamp_delta (float, optional): The delta of the timestamp. Defaults to 10.

    Returns:
    list: A list of MMS-based feature values.
    """

    global index
    voltage_measurement_1 = generate_random_number(minimum=LOWER_BOUND_VOLTAGE, maximum=UPPER_BOUND_VOLTAGE,
                                                   data_type='float')
    voltage_measurement_2 = min(
        max((voltage_measurement_1 + generate_random_number(minimum=-10, maximum=10, data_type='float')),
            LOWER_BOUND_VOLTAGE), UPPER_BOUND_VOLTAGE)
    voltage_measurement_3 = min(
        max((voltage_measurement_1 + generate_random_number(minimum=-10, maximum=10, data_type='float')),
            LOWER_BOUND_VOLTAGE), UPPER_BOUND_VOLTAGE)
    current_measurement_1 = current_measurements[index]
    current_measurement_2 = 0
    current_measurement_3 = 0

    if balanced_load:
        if normal_data:
            current_measurement_2 = min(
                max((current_measurements[index] + generate_random_number(minimum=-1, maximum=+1, data_type='float')),
                    LOWER_BOUND_NORMAL_DATA), UPPER_BOUND_NORMAL_DATA)
            current_measurement_3 = min(
                max((current_measurements[index] + generate_random_number(minimum=-1, maximum=+1, data_type='float')),
                    LOWER_BOUND_NORMAL_DATA), UPPER_BOUND_NORMAL_DATA)
        elif fault_data:
            current_measurement_2 = min(
                max((current_measurements[index] + generate_random_number(minimum=-1, maximum=+1, data_type='float')),
                    LOWER_BOUND_FAULT_DATA), UPPER_BOUND_FAULT_DATA)
            current_measurement_3 = min(
                max((current_measurements[index] + generate_random_number(minimum=-1, maximum=+1, data_type='float')),
                    LOWER_BOUND_FAULT_DATA), UPPER_BOUND_FAULT_DATA)

    else:
        if normal_data:
            current_measurement_2 = min(max((current_measurements[index] + generate_random_number(minimum=-100,
                                                                                                  maximum=+100,
                                                                                                  data_type='float')),
                                            LOWER_BOUND_NORMAL_DATA), UPPER_BOUND_NORMAL_DATA)
            current_measurement_3 = min(max((current_measurements[index] + generate_random_number(minimum=-100,
                                                                                                  maximum=+100,
                                                                                                  data_type='float')),
                                            LOWER_BOUND_NORMAL_DATA), UPPER_BOUND_NORMAL_DATA)
        elif fault_data:
            current_measurement_2 = min(
                max((current_measurements[index] + generate_random_number(minimum=-100, maximum=+100,
                                                                          data_type='float')),
                    LOWER_BOUND_FAULT_DATA), UPPER_BOUND_FAULT_DATA)
            current_measurement_3 = min(
                max((current_measurements[index] + generate_random_number(minimum=-100, maximum=+100,
                                                                          data_type='float')),
                    LOWER_BOUND_FAULT_DATA), UPPER_BOUND_FAULT_DATA)

    values = [
        current_measurement_1,
        current_measurement_2,
        current_measurement_3,
        voltage_measurement_1,
        voltage_measurement_2,
        voltage_measurement_3,
        tripping_signal,
        mms_id,
        timestamp,
        timestamp_delta if timestamp != 0 else 0
    ]

    index += 1
    return values


def remove_unnamed_columns(path_csv_file):
    """
    Remove columns containing 'Unnamed' from a CSV file.

    Parameters:
    path_csv_file (str): The path to the CSV file.

    Returns:
    None
    """

    dataframe = pd.read_csv(path_csv_file)

    # Remove columns that contain 'Unnamed' and save the modified DataFrame back to the same CSV file.
    dataframe = dataframe.loc[:, ~dataframe.columns.str.contains(UNNAMED_COLUMN)]
    dataframe.to_csv(path_csv_file, index=False)


def process_csv_directories(pcap_file, csv_directories, labels):
    """
    Process multiple CSV directories by generating MMS data based on the specified labels.

    Parameters:
    pcap_file (str): The path to the PCAP file.
    csv_directories (list of str): List of directories to save the generated CSV files.
    labels (list of str): List of labels to apply to the data (e.g., normal data, fault data, modification attack data, delay attack data).

    Returns:
    None
    """

    print("Processing csv directories:")
    for csv_directory, label in zip(csv_directories, labels):
        print(f"Processing csv directory: {csv_directory}")
        if label == LABEL_NORMAL_DATA:
            convert_pcap_file_to_csv_file(pcap_file=pcap_file, destination_directory=csv_directory, label=label,
                                          number_of_packets=NUM_NORMAL_PACKETS)
        elif label == LABEL_FAULT_DATA:
            convert_pcap_file_to_csv_file(pcap_file=pcap_file, destination_directory=csv_directory, label=label,
                                          number_of_packets=NUM_FAULT_PACKETS)
        elif label == LABEL_MODIFICATION_ATTACK_DATA:
            convert_pcap_file_to_csv_file(pcap_file=pcap_file, destination_directory=csv_directory, label=label,
                                          number_of_packets=NUM_MODIFICATION_PACKETS)
        elif label == LABEL_DELAY_ATTACK_DATA:
            convert_pcap_file_to_csv_file(pcap_file=pcap_file, destination_directory=csv_directory, label=label,
                                          number_of_packets=NUM_DELAY_PACKETS)


def prepare_dataset(pcap_file, csv_directories_labeled_data, csv_full_dataset, labels):
    """
    Prepare the MMS dataset by processing CSV directories, concatenating files, removing columns, converting features, and shuffling data.

    Parameters:
    pcap_file (str): The path to the PCAP file.
    csv_directories_labeled_data (list of str): List of directories containing labeled CSV data.
    csv_full_dataset (str): The path to save the full concatenated dataset.
    labels (list of str): List of labels to add to the CSV files.

    Returns:
    None
    """

    process_csv_directories(pcap_file=pcap_file, csv_directories=csv_directories_labeled_data, labels=labels)
    concatenate_csv_files(csv_source_directories=csv_directories_labeled_data,
                          csv_destination_file=csv_full_dataset)
    remove_columns(path_csv_file=csv_full_dataset, columns_to_remove=COLUMNS_TO_REMOVE)
    convert_bool_feature_to_int_feature(path_csv_file=csv_full_dataset, feature=TRIPPING_SIGNAL_COLUMN)
    shuffle_data(path_csv_file=CSV_FULL_DATASET)
    print_metadata(csv_full_dataset)
    clean_up_temporary_csv_files(temporary_csv_directories=csv_directories_labeled_data)




prepare_dataset(pcap_file=PCAP_ONE_SAMPLE_MMS_PACKET_REAL_RTU, csv_directories_labeled_data=CSV_DIRECTORIES_LABELED_DATA,
                csv_full_dataset=CSV_FULL_DATASET, labels=LABELS)

