"""
This module implements functions that are used to generate the Sampled Values (SV) dataset
used for building and evaluating a machine learning model to select important features for the SV protocol.
It extracts features from SV packets captured in PCAP files and saves the processed data in CSV format.
The dataset generation process includes converting PCAP files to CSV files, extracting measurements from raw payloads,
labeling data points, etc.

This module has been created as part of a bachelor's thesis on the topic
"Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations".
The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.
The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:
https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.

Caution: Please note that the code in this module does not include extensive input validation or error checking.
The inputs and function parameters have been provided by the author in a controlled environment.
Consider adding error checking and handling functionalities as needed.

Disclaimer: This code is provided "as-is", without any express or implied warranty. In no event shall the author or
contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.

Author: Ahmad Eynawi
Institute: Institute for Automation and Applied Informatics (IAI)
Department: Department of Informatics
University: Karlsruhe Institute of Technology (KIT)
Date: April 10, 2024
"""


import random
random.seed(42)
import pyshark
import csv
import os
from scapy.all import wrpcap, PcapReader
from utility import concatenate_csv_files, generate_random_number, print_metadata, print_iterable, extract_column_from_csv, \
    set_column_to_one_value, clean_up_temporary_csv_files


TSHARK_PATH = "C:\\Program Files\\Wireshark\\tshark.exe"
INDEX_EXAMPLE_PACKET = 0

LABEL_NORMAL_DATA = "Normal"
LABEL_FAULT_DATA = "Fault"
LABEL_REPLAY_DATA = "Replay"
LABEL_INJECTION_DATA = "Injection"
LABELS = [LABEL_NORMAL_DATA, LABEL_FAULT_DATA, LABEL_REPLAY_DATA, LABEL_INJECTION_DATA]

# Paths to the directories storing the raw SV network traffic captures in PCAP format.
PCAP_SOURCE_DIRECTORY_NORMAL_DATA = "..\\Captures\\SV Captures\\Normal Data"
PCAP_SOURCE_DIRECTORY_FAULT_DATA = "..\\Captures\\SV Captures\\Fault Data"
PCAP_SOURCE_DIRECTORY_REPLAY_DATA = "..\\Captures\\SV Captures\\Replay Attack Data"
PCAP_SOURCE_DIRECTORY_INJECTION_DATA = "..\\Captures\\SV Captures\\Injection Attack Data"
PCAP_SOURCE_DIRECTORIES = [PCAP_SOURCE_DIRECTORY_NORMAL_DATA, PCAP_SOURCE_DIRECTORY_FAULT_DATA, PCAP_SOURCE_DIRECTORY_REPLAY_DATA, PCAP_SOURCE_DIRECTORY_INJECTION_DATA]

# Paths to the directories storing the CSV files with the raw SV data extracted from the PCAP files.
# The measurements are encoded according to the ASN.1 Basic Encoding Rules.
CSV_DIRECTORY_NORMAL_DATA_RAW_MEASUREMENTS = "..\\Datasets\\SV Datasets\\Datasets with Raw Measurements\\Normal Data"
CSV_DIRECTORY_FAULT_DATA_RAW_MEASUREMENTS = "..\\Datasets\\SV Datasets\\Datasets with Raw Measurements\\Fault Data"
CSV_DIRECTORY_REPLAY_DATA_RAW_MEASUREMENTS = "..\\Datasets\\SV Datasets\\Datasets with Raw Measurements\\Replay Attack Data"
CSV_DIRECTORY_INJECTION_DATA_RAW_MEASUREMENTS = "..\\Datasets\\SV Datasets\\Datasets with Raw Measurements\\Injection Attack Data"
CSV_DIRECTORIES_RAW_MEASUREMENTS = [CSV_DIRECTORY_NORMAL_DATA_RAW_MEASUREMENTS, CSV_DIRECTORY_FAULT_DATA_RAW_MEASUREMENTS, CSV_DIRECTORY_REPLAY_DATA_RAW_MEASUREMENTS, CSV_DIRECTORY_INJECTION_DATA_RAW_MEASUREMENTS]

# Paths to the directories storing the CSV files with the processed data.
# The measurements are converted from the encoded strings to the corresponding current and voltage values and stored in separate columns.
CSV_DIRECTORY_NORMAL_DATA_PROCESSED_MEASUREMENTS = "..\\Datasets\\SV Datasets\\Datasets with Processed Measurements\\Normal Data"
CSV_DIRECTORY_FAULT_DATA_PROCESSED_MEASUREMENTS = "..\\Datasets\\SV Datasets\\Datasets with Processed Measurements\\Fault Data"
CSV_DIRECTORY_REPLAY_DATA_PROCESSED_MEASUREMENTS = "..\\Datasets\\SV Datasets\\Datasets with Processed Measurements\\Replay Attack Data"
CSV_DIRECTORY_INJECTION_DATA_PROCESSED_MEASUREMENTS = "..\\Datasets\\SV Datasets\\Datasets with Processed Measurements\\Injection Attack Data"
CSV_DIRECTORIES_PROCESSED_MEASUREMENTS = [CSV_DIRECTORY_NORMAL_DATA_PROCESSED_MEASUREMENTS, CSV_DIRECTORY_FAULT_DATA_PROCESSED_MEASUREMENTS, CSV_DIRECTORY_REPLAY_DATA_PROCESSED_MEASUREMENTS, CSV_DIRECTORY_INJECTION_DATA_PROCESSED_MEASUREMENTS]

# Paths to the directories storing the CSV files with the labeled data.
CSV_DIRECTORY_LABELED_NORMAL_DATA = "..\\Datasets\\SV Datasets\\Labeled Datasets\\Normal Data"
CSV_DIRECTORY_LABELED_FAULT_DATA = "..\\Datasets\\SV Datasets\\Labeled Datasets\\Fault Data"
CSV_DIRECTORY_LABELED_REPLAY_DATA = "..\\Datasets\\SV Datasets\\Labeled Datasets\\Replay Attack Data"
CSV_DIRECTORY_LABELED_INJECTION_DATA = "..\\Datasets\\SV Datasets\\Labeled Datasets\\Injection Attack Data"
CSV_DIRECTORIES_LABELED_DATA = [CSV_DIRECTORY_LABELED_NORMAL_DATA, CSV_DIRECTORY_LABELED_FAULT_DATA, CSV_DIRECTORY_LABELED_REPLAY_DATA, CSV_DIRECTORY_LABELED_INJECTION_DATA]

# Path to the processed and labeled full dataset.
CSV_FULL_DATASET = "..\\Datasets\\SV Dataset\\sv_normal_fault_replay_injection_full_dataset.csv"

# Note that each hexadecimal character corresponds to one index in the frame payload, and each byte stores two
# hexadecimal characters.
HEX_DIGITS_IN_BYTE = 2

# Some dataset-specific indices and characteristics that are used, for example, for extracting measurements from the raw payloads.
# Sizes and indices here refer to the corresponding numbers of hexadecimal characters.
INDEX_FIRST_MEASUREMENT = 0
MEASUREMENT_SIZE = 4 * HEX_DIGITS_IN_BYTE
DISTANCE_BETWEEN_MEASUREMENTS = 4 * HEX_DIGITS_IN_BYTE
INDEX_FIRST_QUALITY = 4 * HEX_DIGITS_IN_BYTE
QUALITY_SIZE = 4 * HEX_DIGITS_IN_BYTE
DISTANCE_BETWEEN_QUALITIES = 4 * HEX_DIGITS_IN_BYTE
MEASUREMENTS_NUM_BYTES = 32 * HEX_DIGITS_IN_BYTE
QUALITIES_NUM_BYTES = 32 * HEX_DIGITS_IN_BYTE
INDEX_SAMPLE_NUMBER = 49 * HEX_DIGITS_IN_BYTE + 4
SAMPLE_NUMBER_SIZE = 2 * HEX_DIGITS_IN_BYTE
FIRST_SAMPLE_NUMBER = 0
MEASUREMENTS_COUNT = 8
QUALITIES_COUNT = 8
HEXADECIMAL_BASE = 16
FIRST_SAMPLE_COUNT = 0

# Name of the column storing the raw encoded measurements before processing them.
MEASUREMENTS_COLUMN_NAME = "sv.seqData"
SMP_SYNCH_COLUMN_NAME = "sv.smpSynch"
LABEL_COLUMN_NAME = "class"

FILTER_PROTOCOL = "sv"

# Example string storing the encoded measurements according to the ASN.1 BER encoding scheme
EXAMPLE_HEX_STRING = "00:0a:74:45:00:00:00:00:ff:fd:0b:54:00:00:00:00:ff:f8:7d:db:00:00:00:00:ff:ff:fd:74:00:00:20:00:00:18:27:89:00:00:00:00:ff:f9:34:65:00:00:00:00:ff:ee:a1:9d:00:00:00:00:ff:ff:fd:8b:00:00:20:00"

# Names of columns storing the extracted current and voltage measurements.
MEASUREMENTS_NEW_COLUMNS_NAMES = ["sv.current_measurement_phase_1", "sv.current_quality_phase_1", "sv.current_measurement_phase_2",
                                  "sv.current_quality_phase_2",
                                  "sv.current_measurement_phase_3", "sv.current_quality_phase_3", "sv.current_measurement_4_derived",
                                  "sv.current_quality_4_derived",
                                  "sv.voltage_measurement_phase_1", "sv.voltage_quality_phase_1", "sv.voltage_measurement_phase_2",
                                  "sv.voltage_quality_phase_2",
                                  "sv.voltage_measurement_phase_3", "sv.voltage_quality_phase_3", "sv.voltage_measurement_4_derived",
                                  "sv.voltage_quality_4_derived"]

# Names of features that are added manually to the SV dataset.
frame_based_field_names = [
        "highest_layer",
        "frame_length",
        "timestamp",
        "timestamp_delta"
    ]

NUMBER_OF_PACKETS = 4000
NUMBER_OF_REPLAY_PACKETS = 2000
NUMBER_OF_ALL_COLUMNS = 36

COLUMNS_TO_EXCLUDE = [19, 20, 21]
NUMBER_OF_FILTERED_COLUMNS = 33

# Timestamps are expressed in the unit "microseconds".
TIMESTAMP_DELTA_BETWEEN_TWO_CONSECUTIVE_FRAMES = 250

MINIMUM_REPLAY_START_TIME = 0
MAXIMUM_REPLAY_START_TIME = 10000000
NUMBER_OF_RANDOM_REPLAY_START_TIMES = 10

TIMESTAMP_DELTA_NORMAL_DATA = 250
TIMESTAMP_DELTA_FAULT_DATA = 250
TIMESTAMP_DELTA_REPLAY_DATA = 250

timestamp_normal_data = 0
timestamp_fault_data = 0
timestamp_replay_data = 0
previous_timestamp_replay_data = 0



def extract_measurements_from_hex_string(hex_string):
    """
    Extract measurements and their corresponding quality values from a hexadecimal string.

    Parameters:
    hex_string (str): The hexadecimal string containing the measurements and quality values.

    Returns:
    tuple: A tuple containing two lists:
        - measurements (list of int): The extracted measurement values.
        - measurement_qualities (list of int): The corresponding quality values for each measurement.

    Notes:
    - The function expects the hexadecimal string to follow a specific format, with measurements and qualities
      placed at predefined positions.
    - The function assumes a fixed number of measurements (`MEASUREMENTS_COUNT`), and uses predefined constants
      for sizes and distances between measurements and qualities.
    - The function does not include extensive error checking. Ensure the input format and constants are correct.
    """

    # Initialize an empty list to store the extracted integers and the corresponding qualities.
    measurements = []
    measurement_qualities = []

    # Convert the input to a string in case it's not already a string.
    hex_string = str(hex_string)

    # Remove colons from the hexadecimal string to prepare for conversion.
    hex_string = hex_string.replace(":", "")

    # Loop over the number of measurements to extract each measurement and its quality.
    for i in range(MEASUREMENTS_COUNT):
        # Calculate the index of the measurement to extract next.
        current_index_measurement = INDEX_FIRST_MEASUREMENT + (i * (MEASUREMENT_SIZE + DISTANCE_BETWEEN_MEASUREMENTS))

        # Calculate the index of the quality to extract next.
        current_index_quality = INDEX_FIRST_QUALITY + (i * (QUALITY_SIZE + DISTANCE_BETWEEN_QUALITIES))

        # Extract the measurement and the corresponding quality value from the hexadecimal string.
        measurement_bytes = hex_string[current_index_measurement:current_index_measurement + MEASUREMENT_SIZE]
        quality_bytes = hex_string[current_index_quality:current_index_quality + QUALITY_SIZE]

        # Convert the extracted measurement and quality bytes to integers.
        measurement = int(measurement_bytes, HEXADECIMAL_BASE)
        quality = int(quality_bytes, HEXADECIMAL_BASE)

        # Check if the sign bit is set to handle negative numbers.
        if measurement >= 2 ** 31:
            # Adjust for two's complement to handle negative values correctly.
            measurement -= 2 ** 32

        # Append the extracted measurement and quality to their respective lists.
        measurements.append(measurement)
        measurement_qualities.append(quality)

    # Return the lists of measurements and their corresponding qualities.
    return measurements, measurement_qualities


def extract_measurements_from_hex_strings(hex_strings):
    """
    Extract measurements and their qualities from a list of hexadecimal strings.

    Parameters:
    hex_strings (list of str): A list of hexadecimal strings containing measurements.

    Returns:
    list of list: A list of lists, where each inner list corresponds to one hexadecimal string and contains concatenated
    measurement values and their qualities. Each measurement is followed directly by the corresponding quality.
    """

    all_measurements = []

    for hex_string in hex_strings:
        concatenated_values = []
        measurements, qualities = extract_measurements_from_hex_string(hex_string)

        for i in range(MEASUREMENTS_COUNT):
            concatenated_values.append(measurements[i])
            concatenated_values.append(qualities[i])

        all_measurements.append(concatenated_values)

    return all_measurements


def filter_pcap_files_by_protocol(source_directory, destination_directory, filter_protocol):
    """
    Filter PCAP files in the source directory by the specified protocol and save them to the destination directory.

    Parameters:
    source_directory (str): The directory containing the source PCAP files.
    destination_directory (str): The directory to save the filtered PCAP files.
    filter_protocol (str): The protocol to filter by (e.g., "http", "tcp").

    Returns:
    None
    """
    # Ensure the destination directory exists.
    os.makedirs(destination_directory, exist_ok=True)

    for filename in os.listdir(source_directory):
        if filename.endswith(".pcap") or filename.endswith(".pcapng"):
            source_file = os.path.join(source_directory, filename)
            destination_file = os.path.join(destination_directory, filename)

            print(f"Processing: {source_file}")
            filter_packets_by_protocol(source_file, destination_file, filter_protocol)


def filter_packets_by_protocol(source_file, destination_file, filter_protocol):
    """
    Filter packets in a PCAP file by the specified protocol and save the filtered packets to a new file.

    Parameters:
    source_file (str): The path to the source PCAP file.
    destination_file (str): The path to save the filtered PCAP file.
    filter_protocol (str): The protocol to filter by (e.g., "sv", "tcp").

    Returns:
    None
    """

    # Load the PCAP file with the filtered packets.
    cap = pyshark.FileCapture(source_file, display_filter=filter_protocol, only_summaries=False, keep_packets=True)

    packets = []
    for packet in cap:
        # Convert the packet to a scapy packet for saving.
        original_packet = PcapReader(packet.frame_info.protocols).read_packet()
        packets.append(original_packet)

    # Save the filtered packets to a new file.
    wrpcap(destination_file, packets)
    print(f"Filtered packets saved to: {destination_file}.")


def replace_hex_strings_with_measurements(source_csv_path, target_csv_path, column_to_replace=MEASUREMENTS_COLUMN_NAME):
    """
    Replace hexadecimal strings in a specified column of a CSV file with extracted measurement values.

    Parameters:
    source_csv_path (str): The path to the source CSV file.
    target_csv_path (str): The path to the target CSV file.
    column_to_replace (str, optional): The name of the column to be replaced. Defaults to 'sv.seqData'.

    Returns:
    None
    """

    # Extract the specified column data from the source CSV file.
    column_data = extract_column_from_csv(csv_file_path=source_csv_path, column_name=column_to_replace)

    # Extract measurements from the hexadecimal strings in the column data.
    all_measurements = extract_measurements_from_hex_strings(hex_strings=column_data)

    # Print the first three items for testing.
    print_iterable(iterable=all_measurements, number_of_items_to_print=3)

    # Replace the specified column in the source CSV file with the new measurement values.
    replace_encoded_measurements_with_extracted_measurements_in_csv_files(source_csv_path=source_csv_path, target_csv_path=target_csv_path,
                                                                          column_to_replace=column_to_replace, new_columns_names=MEASUREMENTS_NEW_COLUMNS_NAMES)


def replace_encoded_measurements_with_extracted_measurements_in_csv_files(source_csv_path, target_csv_path, column_to_replace=MEASUREMENTS_COLUMN_NAME,
                                                                          new_columns_names=None):
    """
    Replace the column storing the encoded measurements in a CSV file with new columns containing extracted
    measurement data.

    Parameters:
    source_csv_path (str): The path to the source CSV file.
    target_csv_path (str): The path to the target CSV file.
    column_to_replace (str, optional): The name of the column to be replaced. Defaults to 'sv.seqData'.
    new_columns_names (list of str, optional): The names of the new columns.

    Returns:
    None
    """

    if new_columns_names is None:
        new_columns_names = MEASUREMENTS_NEW_COLUMNS_NAMES

    # Extract the specified column data from the source CSV file.
    column_data = extract_column_from_csv(csv_file_path=source_csv_path, column_name=column_to_replace)
    print("Number of elements in the csv file:", len(column_data))

    # Extract measurements from the hexadecimal strings in the column data.
    measurements = extract_measurements_from_hex_strings(hex_strings=column_data)

    with open(source_csv_path, mode='r', newline='', encoding='utf-8') as infile, \
         open(target_csv_path, mode='w', newline='', encoding='utf-8') as outfile:
        reader = csv.reader(infile)
        writer = csv.writer(outfile)

        # Reading the header to find the index of the column to replace.
        header = next(reader)
        column_index = header.index(column_to_replace)

        # Replace the old column name with new column names.
        new_header = header[:column_index] + new_columns_names + header[column_index + 1:]
        writer.writerow(new_header)

        # Iterate over each row in the source CSV.
        for i, row in enumerate(reader):
            # Replace the old column value with new values from the 2D array.
            new_row = row[:column_index] + list(measurements[i]) + row[column_index + 1:]
            writer.writerow(new_row)


def replace_encoded_measurements_with_extracted_measurements_in_csv_directories(csv_source_directories, csv_destination_directories):
    """
    Replace the column with the encoded measurements with new columns for the extracted measurement values.

    Parameters:
    csv_source_directories (list of str): List of directories containing source CSV files.
    csv_destination_directories (list of str): List of directories to save the modified CSV files.

    Returns:
    None
    """

    print("Processing measurements:")
    for csv_source_directory, csv_destination_directory in zip(csv_source_directories, csv_destination_directories):
        print(f"Processing csv directory: {csv_source_directory}")
        for filename in os.listdir(csv_source_directory):
            if filename.endswith(".csv"):
                source_csv_file = os.path.join(csv_source_directory, filename)
                destination_csv_file = os.path.join(csv_destination_directory, filename)
                replace_encoded_measurements_with_extracted_measurements_in_csv_files(source_csv_path=source_csv_file, target_csv_path=destination_csv_file,
                                                                                      column_to_replace=MEASUREMENTS_COLUMN_NAME,
                                                                                      new_columns_names=MEASUREMENTS_NEW_COLUMNS_NAMES)


def convert_pcap_files_to_csv(source_directory, destination_directory, filter_protocol, first_sample_count, number_of_packets):
    """
    Convert PCAP files in the source directory to CSV files with specified filtering and sampling.

    Parameters:
    source_directory (str): The directory containing the source PCAP files.
    destination_directory (str): The directory to save the converted CSV files.
    filter_protocol (str): The protocol to filter by (e.g., "sv", "tcp").
    first_sample_count (int): The identifier of the first SV frame within a sequence (usually zero).
    number_of_packets (int): The number of frames to process.

    Returns:
    None
    """

    for filename in os.listdir(source_directory):
        if filename.endswith(".pcap") or filename.endswith(".pcapng"):
            pcap_file = os.path.join(source_directory, filename)
            print(f"Processing file: {pcap_file}")

            if (LABEL_NORMAL_DATA in source_directory) or (LABEL_FAULT_DATA in source_directory):
                convert_normal_or_fault_pcap_files_to_csv(pcap_file, destination_directory, filter_protocol, first_sample_count, number_of_packets)
            elif (LABEL_REPLAY_DATA in source_directory) or (LABEL_INJECTION_DATA in source_directory):
                random_replay_start_time = generate_random_replay_start_time()
                random_first_sample_count = generate_random_number(minimum=0, maximum=3999, data_type='integer')
                convert_injection_or_replay_pcap_files_to_csv(pcap_file=pcap_file, destination_directory=destination_directory,
                                                              replay_start_time=random_replay_start_time, first_sample_count=random_first_sample_count)


def convert_normal_or_fault_pcap_files_to_csv(pcap_file, destination_directory, filter_protocol=FILTER_PROTOCOL, first_sample_count=FIRST_SAMPLE_COUNT, number_of_packets=NUMBER_OF_PACKETS):
    """
    Convert PCAP files storing normal or fault messages to CSV format.

    Parameters:
    pcap_file (str): The path to the PCAP file.
    destination_directory (str): The directory to save the converted CSV file.
    filter_protocol (str, optional): The protocol to filter by. Defaults to 'sv'.
    first_sample_count (int, optional): The identifier of the first SV frame within a sequence. Defaults to 0.
    number_of_packets (int, optional): The number of packets to process. Defaults to 4000.

    Returns:
    None
    """

    # Compute the CSV file name based on the pcap file name.
    base_name = os.path.basename(pcap_file)
    csv_file_name = os.path.splitext(base_name)[0] + ".csv"
    csv_file = os.path.join(destination_directory, csv_file_name)

    # Ensure the destination directory exists.
    os.makedirs(destination_directory, exist_ok=True)

    cap = pyshark.FileCapture(pcap_file, display_filter=filter_protocol, tshark_path=TSHARK_PATH, only_summaries=False)

    start_index = None
    # Search for the first message with the given sample count.
    if first_sample_count is not None:
        for i, packet in enumerate(cap):
            if filter_protocol in packet and int(packet.sv.smpCnt) == first_sample_count:
                start_index = i
                break
        print("Start index:", start_index)

    packet = cap[INDEX_EXAMPLE_PACKET]
    field_names = []
    for layer in packet.layers:
        field_names.extend(layer._all_fields.keys())

    # Remove fields which are not available in all packets in the given datasets to ensure that all datasets
    # contain the same column names.
    if len(field_names) > NUMBER_OF_FILTERED_COLUMNS:
        filtered_field_names = [item for i, item in enumerate(field_names) if i not in COLUMNS_TO_EXCLUDE]
    else:
        filtered_field_names = field_names

    # Close and reopen the capture file to reset the packet iterator and ensure that packets are accessed
    # from the desired start index. Duplicates might appear if the FileCapture object is not reset.
    cap.close()
    cap = pyshark.FileCapture(pcap_file, display_filter=filter_protocol, only_summaries=False,
                              tshark_path=TSHARK_PATH, keep_packets=True)

    # Capture the specified number of messages starting from the found packet.
    with open(csv_file, mode='w', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        columns_names = filtered_field_names + frame_based_field_names
        print("Number of features:", len(columns_names))
        writer.writerow(columns_names)

        if start_index is not None:
            end_index = start_index + number_of_packets
            cap.load_packets(packet_count=end_index)
            for i in range(start_index, end_index):
                packet = cap[i]
                values = []
                values.extend(get_layer_based_features(packet=packet))
                values.extend(get_frame_based_features(packet=packet, is_normal_frame=True))
                writer.writerow(values)
        else:
            cap.load_packets()
            for packet in cap:
                values = []
                values.extend(get_layer_based_features(packet=packet))
                values.extend(get_frame_based_features(packet=packet, is_normal_frame=True))
                writer.writerow(values)

    cap.close()


def convert_injection_or_replay_pcap_files_to_csv(pcap_file, destination_directory, replay_start_time,
                                                  first_sample_count=None, number_of_packets=NUMBER_OF_REPLAY_PACKETS, filter_protocol=FILTER_PROTOCOL):
    """
    Convert PCAP files storing injection or replay attack messages to CSV format.

    Parameters:
    pcap_file (str): The path to the PCAP file.
    destination_directory (str): The directory to save the converted CSV file.
    replay_start_time (float): The start time for replaying packets.
    first_sample_count (int, optional): The identifier of the first SV frame within a sequence. Defaults to None.
    number_of_packets (int, optional): The number of packets to process. Defaults to 2000.
    filter_protocol (str, optional): The protocol to filter by. Defaults to 'sv'.

    Returns:
    None
    """

    # Compute the CSV file name based on the pcap file name.
    base_name = os.path.basename(pcap_file)
    csv_file_name = os.path.splitext(base_name)[0] + ".csv"
    csv_file = os.path.join(destination_directory, csv_file_name)

    # Ensure the destination directory exists.
    os.makedirs(destination_directory, exist_ok=True)

    cap = pyshark.FileCapture(pcap_file, display_filter=filter_protocol, tshark_path=TSHARK_PATH, only_summaries=False)

    global timestamp_replay_data
    global previous_timestamp_replay_data
    timestamp_replay_data = replay_start_time
    previous_timestamp_replay_data = replay_start_time

    start_index = None
    # Search for the first message with the given sample count.
    if first_sample_count is not None:
        for i, packet in enumerate(cap):
            if filter_protocol in packet and int(packet.sv.smpCnt) == first_sample_count:
                start_index = i
                break
        print("Start index:", start_index)

    packet = cap[INDEX_EXAMPLE_PACKET]
    field_names = []
    for layer in packet.layers:
        field_names.extend(layer._all_fields.keys())

    # Remove fields which are not available in all packets in the given datasets to ensure that all datasets
    # contain the same column names.
    if len(field_names) > NUMBER_OF_FILTERED_COLUMNS:
        filtered_field_names = [item for i, item in enumerate(field_names) if i not in COLUMNS_TO_EXCLUDE]
    else:
        filtered_field_names = field_names

    # Close and reopen the capture file to reset the packet iterator and ensure that packets are accessed
    # from the desired start index. Duplicates might appear if the FileCapture object is not reset.
    cap.close()
    cap = pyshark.FileCapture(pcap_file, display_filter=filter_protocol, only_summaries=False,
                              tshark_path=TSHARK_PATH, keep_packets=True)

    # Now, capture the specified number of messages starting from the found packet.
    with open(csv_file, mode='w', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        columns_names = filtered_field_names + frame_based_field_names
        writer.writerow(columns_names)
        print("Number of features:", len(columns_names))

        if start_index is not None:
            end_index = start_index + number_of_packets
            cap.load_packets(packet_count=end_index)
            for i in range(start_index, end_index):
                packet = cap[i]
                values = []
                values.extend(get_layer_based_features(packet=packet))
                values.extend(get_frame_based_features(packet=packet, is_replay_frame=True))
                writer.writerow(values)
        else:
            cap.load_packets()
            for packet in cap:
                values = []
                values.extend(get_layer_based_features(packet=packet))
                values.extend(get_frame_based_features(packet=packet, is_replay_frame=True))
                writer.writerow(values)

    cap.close()


def get_layer_based_features(packet):
    """
    Extract layer-based features from a packet.

    Parameters:
    packet (pyshark.packet.Packet): The packet to extract features from.

    Returns:
    list: A list of feature values extracted from the packet layers.
    """

    values = []
    for layer in packet.layers:
        values.extend(layer._all_fields.values())

    # Remove fields which are not available in all packets in the given datasets to ensure that all datasets
    # contain the same column names.
    if len(values) > NUMBER_OF_FILTERED_COLUMNS:
        values = [value for i, value in enumerate(values) if i not in COLUMNS_TO_EXCLUDE]

    return values


def get_frame_based_features(packet, is_normal_frame=False, is_fault_frame=False, is_replay_frame=False,
                             is_injection_frame=False):
    """
    Extract frame-based features from a packet, adjusting timestamps based on the class of the frame
    (i.e., normal, fault, replay, or injection).

    Parameters:
    packet (pyshark.packet.Packet): The packet to extract features from.
    is_normal_frame (bool, optional): Flag indicating if the frame is a normal frame. Defaults to False.
    is_fault_frame (bool, optional): Flag indicating if the frame is a fault frame. Defaults to False.
    is_replay_frame (bool, optional): Flag indicating if the frame is a replay frame. Defaults to False.
    is_injection_frame (bool, optional): Flag indicating if the frame is an injection frame. Defaults to False.

    Returns:
    list: A list of frame-based feature values extracted from the packet.
    """

    global timestamp_normal_data
    global timestamp_fault_data
    global timestamp_replay_data
    global TIMESTAMP_DELTA_REPLAY_DATA
    global previous_timestamp_replay_data

    highest_layer = packet.highest_layer
    length = packet.length
    values = []

    if is_normal_frame or is_fault_frame:
        values = [
            highest_layer,
            length,
            timestamp_normal_data,
            TIMESTAMP_DELTA_NORMAL_DATA if timestamp_normal_data != 0 else 0
        ]
        # For normal and fault data, the difference between each two consecutive messages is always 250 microseconds.
        timestamp_normal_data += TIMESTAMP_DELTA_NORMAL_DATA

    elif is_replay_frame or is_injection_frame:
        if previous_timestamp_replay_data == timestamp_replay_data:
            # This is the first packet, so there is no previous timestamp to compare to.
            TIMESTAMP_DELTA_REPLAY_DATA = 0
        else:
            # Calculate the delta from the previous timestamp.
            TIMESTAMP_DELTA_REPLAY_DATA = abs(timestamp_replay_data - previous_timestamp_replay_data)

        # Update the global variable with the current packet's timestamp for the next call.
        previous_timestamp_replay_data = timestamp_replay_data

        values = [
            highest_layer,
            length,
            timestamp_replay_data,
            TIMESTAMP_DELTA_REPLAY_DATA
        ]
        # For replay and injection data, the difference between each two consecutive messages is set to some
        # random number between 230 and 270 microseconds.
        timestamp_replay_data = timestamp_replay_data + TIMESTAMP_DELTA_NORMAL_DATA + generate_random_number(minimum=-20, maximum=20, data_type='integer')

    return values


def convert_pcap_directories_to_csv_directories(pcap_directories, csv_directories, filter_protocol=FILTER_PROTOCOL,
                                                first_sample_count=FIRST_SAMPLE_COUNT, number_of_packets=NUMBER_OF_PACKETS):
    """
    Convert multiple directories of PCAP files to CSV format.

    Parameters:
    pcap_directories (list of str): List of directories containing PCAP files.
    csv_directories (list of str): List of directories to save the converted CSV files.
    filter_protocol (str, optional): The protocol to filter by. Defaults to 'sv'.
    first_sample_count (int, optional): The identifier of the first SV frame within a sequence. Defaults to 0.
    number_of_packets (int, optional): The number of packets to process. Defaults to 4000.

    Returns:
    None
    """

    global timestamp_normal_data
    print("Converting pcap files to csv files:")

    for pcap_directory, csv_directory in zip(pcap_directories, csv_directories):
        timestamp_normal_data = 0
        print(f"Processing pcap directory: {pcap_directory}")
        convert_pcap_files_to_csv(source_directory=pcap_directory, destination_directory=csv_directory,
                                  filter_protocol=filter_protocol, first_sample_count=first_sample_count,
                                  number_of_packets=number_of_packets)


def add_label_to_csv_file(input_csv_file, output_csv_file, label):
    """
    Add a label column to a CSV file.

    Parameters:
    input_csv_file (str): The path to the input CSV file.
    output_csv_file (str): The path to the output CSV file.
    label (str): The label to add to each row.

    Returns:
    None
    """

    # Read data from the input CSV file.
    with open(input_csv_file, mode='r', newline='', encoding='utf-8') as input_file:
        reader = csv.reader(input_file)
        # Read the header from the input file and append the 'label' column.
        headers = next(reader, None)
        if headers:
            headers.append(LABEL_COLUMN_NAME)
        # Add the label to each row.
        data = [row + [label] for row in reader]

    # Write modified data to the output CSV file.
    with open(output_csv_file, mode='w', newline='', encoding='utf-8') as output_file:
        writer = csv.writer(output_file)
        writer.writerow(headers)
        writer.writerows(data)


def add_labels_to_csv_directories(csv_source_directories, csv_destination_directories, labels):
    """
    Add labels to CSV files in multiple directories.

    Parameters:
    csv_source_directories (list of str): List of directories containing source CSV files.
    csv_destination_directories (list of str): List of directories to save the labeled CSV files.
    labels (list of str): List of labels to add to the CSV files.

    Returns:
    None
    """

    print("Labeling data:")
    for csv_source_directory, csv_destination_directory, label in zip(csv_source_directories, csv_destination_directories, labels):
        print(f"Processing csv directory: {csv_source_directory}")
        for filename in os.listdir(csv_source_directory):
            if filename.endswith(".csv"):
                source_csv_file = os.path.join(csv_source_directory, filename)
                destination_csv_file = os.path.join(csv_destination_directory, filename)
                add_label_to_csv_file(input_csv_file=source_csv_file, output_csv_file=destination_csv_file, label=label)


def generate_random_replay_start_time(minimum=MINIMUM_REPLAY_START_TIME, maximum=MAXIMUM_REPLAY_START_TIME, not_multiple_of=TIMESTAMP_DELTA_BETWEEN_TWO_CONSECUTIVE_FRAMES):
    """
    Generate a random replay start time that is not a multiple of a specified value.

    Parameters:
    minimum (int, optional): The minimum value for the random number. Defaults to 0.
    maximum (int, optional): The maximum value for the random number. Defaults to 10000000.
    not_multiple_of (int, optional): The value that the random number should not be a multiple of. Defaults to 250.

    Returns:
    int: A random number between the minimum and maximum values that is not a multiple of the specified value.
    """

    random_replay_start_time = random.randint(minimum, maximum)
    while random_replay_start_time % not_multiple_of == 0:
        random_replay_start_time = random.randint(minimum, maximum)
    return random_replay_start_time


def generate_random_replay_start_times(count=NUMBER_OF_RANDOM_REPLAY_START_TIMES, minimum=MINIMUM_REPLAY_START_TIME, maximum=MAXIMUM_REPLAY_START_TIME, not_multiple_of=TIMESTAMP_DELTA_BETWEEN_TWO_CONSECUTIVE_FRAMES):
    """
    Generate multiple random replay start times that are not multiples of a specified value.

    Parameters:
    count (int, optional): The number of random times to generate. Defaults to 10.
    minimum (int, optional): The minimum value for the random numbers. Defaults to 0.
    maximum (int, optional): The maximum value for the random numbers. Defaults to 1000000.
    not_multiple_of (int, optional): The value that the random numbers should not be multiples of. Defaults to 250.

    Returns:
    list of int: A list of random numbers between the minimum and maximum values that are not multiples of the specified value.
    """

    random_replay_start_times = []

    while len(random_replay_start_times) < count:
        num = random.randint(minimum, maximum)
        if num % not_multiple_of != 0:
            random_replay_start_times.append(num)

    return random_replay_start_times


def prepare_dataset(pcap_source_directories, csv_directories_raw_measurements, csv_directories_processed_measurements,
                    csv_directories_labeled_data, csv_full_dataset, labels):
    """
    Prepare the SV dataset by converting PCAP files to CSV format, processing measurements, adding labels, and concatenating files.

    Parameters:
    pcap_source_directories (list of str): List of directories containing source PCAP files.
    csv_directories_raw_measurements (list of str): List of directories to save CSV files with the raw measurement.
    csv_directories_processed_measurements (list of str): List of directories to save CSV files with the processed measurements.
    csv_directories_labeled_data (list of str): List of directories to save labeled CSV files.
    csv_full_dataset (str): The path to save the CSV file storing the concatenated full dataset.
    labels (list of str): List of labels to add to the CSV files.

    Returns:
    None
    """

    convert_pcap_directories_to_csv_directories(pcap_directories=pcap_source_directories,
                                                csv_directories=csv_directories_raw_measurements)
    replace_encoded_measurements_with_extracted_measurements_in_csv_directories(csv_source_directories=csv_directories_raw_measurements,
                                                                                csv_destination_directories=csv_directories_processed_measurements)
    add_labels_to_csv_directories(csv_source_directories=csv_directories_processed_measurements,
                                  csv_destination_directories=csv_directories_labeled_data, labels=labels)
    concatenate_csv_files(csv_source_directories=csv_directories_labeled_data,
                          csv_destination_file=csv_full_dataset)
    set_column_to_one_value(path_csv_file=csv_full_dataset, column=SMP_SYNCH_COLUMN_NAME, value=1)
    print_metadata(csv_full_dataset)
    clean_up_temporary_csv_files(temporary_csv_directories = csv_directories_raw_measurements + csv_directories_processed_measurements + csv_directories_labeled_data)



prepare_dataset(pcap_source_directories=PCAP_SOURCE_DIRECTORIES, csv_directories_raw_measurements=CSV_DIRECTORIES_RAW_MEASUREMENTS,
                csv_directories_processed_measurements=CSV_DIRECTORIES_PROCESSED_MEASUREMENTS, csv_directories_labeled_data=CSV_DIRECTORIES_LABELED_DATA,
                csv_full_dataset=CSV_FULL_DATASET, labels=LABELS)
