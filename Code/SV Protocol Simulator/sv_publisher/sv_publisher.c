/*
 * This program simulates a Sampled Values (SV) publisher.
 * The simulation involves generating measurements, writing them into SV frames, and publishing these frames on a network interface.
 *
 * This module has been created as part of a bachelor's thesis on the topic
 * "Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations".
 * The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.
 * The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:
 * https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.
 *
 * Caution: Please note that the code in this module does not include extensive input validation or error checking.
 * The inputs and function parameters have been provided by the author in a controlled environment.
 * Consider adding error checking and handling functionalities as needed.
 *
 * Disclaimer: This code is provided "as-is", without any express or implied warranty. In no event shall the author or
 * contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.
 *
 * Acknowledgment: This code uses functionalities from the open-source libiec61850 library (https://libiec61850.com/)
 * for simulating the SV publisher.
 *
 * Author: Ahmad Eynawi
 * Institute: Institute for Automation and Applied Informatics (IAI)
 * Department: Department of Informatics
 * University: Karlsruhe Institute of Technology (KIT)
 * Date: April 10, 2024
 */

#define _POSIX_C_SOURCE 199309L
#include <signal.h>
#include <stdio.h>
#include "hal_thread.h"
#include "sv_publisher.h"
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <stdlib.h>

// Global pointers for resources to be freed.
CommParameters *params = NULL;
SVPublisher svPublisher = NULL;
SinusoidalValues *sinusoidal_values = NULL;
int *indices = NULL;
int **values = NULL;

// Some constants.
#define M_PI 3.14159265358979323846
#define MAGNITUDE = sqrt(2)
#define VALUES_COUNT 8
#define CURRENTS_COUNT 4
#define VOLTAGES_COUNT 4

const long NANOSECONDS_IN_SECOND = 1000000000;
const long MICROSECONDS_IN_SECOND = 1000000;
const long NANOSECONDS_PER_SAMPLE = 250000;
const long DELAY_MICROSECONDS = MICROSECONDS_IN_SECOND / SAMPLE_COUNT;
const long DELAY_NANOSECONDS = NANOSECONDS_IN_SECOND / SAMPLE_COUNT;
const uint16_t SAMPLE_COUNT = 4000;
const int NUM_VALUES_TO_PRINT = 5;

const char *NETWORK_INTERFACE = "eth0";
const char *ASDU_ID = "MU320_MU0101";

const double RMS = 1;
const int FREQUENCY = 50;
const double I_MAX = 700;
const double V_MAX = 570;
const int32_t FAULT_I_MAX = 5000;
const int32_t FAULT_V_MAX = 35000;
const int SMP_Synch = 1;
const uint8_t SMP_MOD = IEC61850_SV_SMPMOD_SAMPLES_PER_SECOND;
Quality QUALITY = 0;

const char *PATH_4000_REAL_CURRENTS_AND_VOLTAGES = "measurements/sv_4000_real_currents_and_voltages.txt";
const char *PATH_4000_MODIFIED_REAL_CURRENTS_AND_VOLTAGES = "measurements/sv_4000_modified_real_currents_and_voltages.txt";

static bool running = true;

// Structure to hold 8 values representing 4 current and 4 voltage measurements.
typedef struct
{
    double current_values[4]; // 4 current values
    double voltage_values[4]; // 4 voltage values
} SinusoidalValues;

/**
 * @brief Signal handler for SIGINT to stop the application.
 *
 * @param signalId The signal number.
 */
void sigint_handler(int signalId)
{
    running = 0;
}

/**
 * @brief Read a 2D array from a file.
 *
 * @param path The path to the file.
 * @param rows The number of rows in the array.
 * @param cols The number of columns in the array.
 * @return int** A pointer to the dynamically allocated 2D array.
 */
int **readArrayFromFile(const char *path, int rows, int cols)
{
    FILE *file = fopen(path, "r");
    if (file == NULL)
    {
        perror("Error opening file");
        return NULL;
    }

    // Dynamically allocate memory for the 2D array.
    int **array = malloc(rows * sizeof(int *));
    for (int i = 0; i < rows; i++)
    {
        array[i] = malloc(cols * sizeof(int));
    }

    // Read the array from the file.
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            fscanf(file, "%d", &array[i][j]);
        }
    }

    fclose(file);
    return array;
}

/**
 * @brief Display a 1D array.
 *
 * @param array The array to display.
 * @param values_count The number of values in the array.
 */
void display1DArray(int *array, int values_count)
{
    printf("Values:\n");
    if (array != NULL)
    {
        for (int i = 0; i < values_count; i++)
        {
            printf("%d ", array[i]);
        }
    }
    printf("\n");
}

/**
 * @brief Display a 2D array.
 *
 * @param array The array to display.
 * @param rows The number of rows in the array.
 * @param cols The number of columns in the array.
 */
void display2DArray(int **array, int rows, int cols)
{
    printf("First %d Measurements:\n", rows);
    if (array != NULL)
    {
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                printf("%d ", array[i][j]);
            }
            printf("\n");
        }
    }
}

/**
 * @brief Print the sinusoidal values.
 *
 * @param values The array of sinusoidal values.
 * @param num_values_to_print The number of values to print.
 */
void printSinusoidalValues(SinusoidalValues *values, int num_values_to_print)
{
    for (int i = 0; i < num_values_to_print; i++)
    {
        printf("Value %d:\n", i);
        printf("  Currents: %lf, %lf, %lf, %lf\n", values[i].current_values[0], values[i].current_values[1], values[i].current_values[2], values[i].current_values[3]);
        printf("  Voltages: %lf, %lf, %lf, %lf\n", values[i].voltage_values[0], values[i].voltage_values[1], values[i].voltage_values[2], values[i].voltage_values[3]);
    }
}

/**
 * @brief Calculate the current value using a sinusoidal function.
 *
 * @param rms The root mean square value.
 * @param frequency The frequency of the sinusoid.
 * @param time The time at which to evaluate the sinusoid.
 * @param phase The phase shift of the sinusoid.
 * @return int32_t The calculated current value.
 */
int32_t current_function(double rms, double frequency, double time, double phase)
{
    return rms * sqrt(2) * sin(2 * M_PI * frequency * time + phase);
}

/**
 * @brief Calculate the voltage value using a sinusoidal function.
 *
 * @param rms The root mean square value.
 * @param frequency The frequency of the sinusoid.
 * @param time The time at which to evaluate the sinusoid.
 * @param phase The phase shift of the sinusoid.
 * @return int32_t The calculated voltage value.
 */
int32_t voltage_function(double rms, double frequency, double time, double phase)
{
    return rms * sqrt(2) * sin(2 * M_PI * frequency * time + phase);
}

/**
 * @brief Generate an array of sinusoidal values.
 *
 * @param frequency The frequency of the sinusoid.
 * @param I_max The maximum current value.
 * @param V_max The maximum voltage value.
 * @param num_messages The number of messages to generate.
 * @return SinusoidalValues* A pointer to the array of generated sinusoidal values.
 */
SinusoidalValues *generate_values(int frequency, double I_max, double V_max, int num_messages)
{
    SinusoidalValues *values = malloc(num_messages * sizeof(SinusoidalValues));
    if (values == NULL)
    {
        fprintf(stderr, "Memory allocation failed.\n");
        exit(EXIT_FAILURE);
    }

    // Phases in radians (0, 240, 120 degrees).
    double phases[] = {0, 4 * M_PI / 3, 2 * M_PI / 3};
    double current_sum = 0;
    double voltage_sum = 0;
    double time = 0.0;
    // A sequence of 4000 SV messages is published every second and the measurements are repeated every second.
    double time_per_sample = 1.0 / num_messages;

    for (int t = 0; t < num_messages; t++)
    {
        current_sum = 0;
        voltage_sum = 0;

        // Calculate current values for the three phases.
        for (int i = 0; i < 3; i++)
        {
            values[t].current_values[i] = current_function(I_max, frequency, time, phases[i]);
            current_sum += values[t].current_values[i];
        }
        // The fourth current value in each message is set to the sum of the three previous current values.
        values[t].current_values[3] = current_sum;

        // Calculate voltage values for the three phases.
        for (int i = 0; i < 3; i++)
        {
            values[t].voltage_values[i] = voltage_function(V_max, frequency, time, phases[i]);
            voltage_sum += values[t].voltage_values[i];
        }
        // The fourth voltage value in each message is set to the sum of the three previous voltage values.
        values[t].voltage_values[3] = voltage_sum;

        // For a sample count of 4000 samples/second, the time between two subsequent samples must be 250 microseconds = 0.000250 seconds.
        time += time_per_sample;
    }

    return values;
}

/**
 * @brief Set the values in the ASDU of the current SV message.
 *
 * @param asdu The ASDU to set the values in.
 * @param indices The indices of the values.
 * @param values The values to set.
 * @param qualities The qualities of the values.
 */
void set_values(SVPublisher_ASDU asdu, int *indices, int32_t *values, int32_t *qualities)
{
    for (int i = 0; i < VALUES_COUNT * 2; i += 2)
    {
        SVPublisher_ASDU_setINT32(asdu, indices[i], values[i / 2]);
    }

    for (int i = 1; i < VALUES_COUNT * 2; i += 2)
    {
        SVPublisher_ASDU_setINT32(asdu, indices[i], qualities[(i - 1) / 2]);
    }
}

/**
 * @brief Set the parameters in the ASDU of the current SV message.
 *
 * @param asdu The ASDU to set the parameters in.
 * @param indices The indices of the parameters.
 * @param timestamp The timestamp to set.
 * @param quality The quality to set.
 */
void set_parameters(SVPublisher_ASDU asdu, int *indices, Timestamp timestamp, Quality quality)
{
    // Store these parameters after the current and voltage values in the message.
    int i = VALUES_COUNT * 2;
    SVPublisher_ASDU_setTimestamp(asdu, indices[i], timestamp);
    SVPublisher_ASDU_setQuality(asdu, indices[i + 1], quality);
    SVPublisher_ASDU_setRefrTmByTimestamp(asdu, &timestamp);
}

/**
 * @brief Set static current and voltage values in the ASDU of the current SV message.
 *
 * @param asdu The ASDU to set the values in.
 * @param indices The indices of the values.
 * @param current_values The static current values to set.
 * @param voltage_values The static voltage values to set.
 * @param qualities The qualities of the values.
 */
void set_static_currents_and_voltages(SVPublisher_ASDU asdu, int *indices, int32_t *current_values, int32_t *voltage_values, int32_t *qualities)
{
    for (int i = 0; i < VALUES_COUNT; i += 2)
    {
        SVPublisher_ASDU_setINT32(asdu, indices[i], current_values[i / 2]);
    }

    for (int i = VALUES_COUNT; i < VALUES_COUNT * 2; i += 2)
    {
        SVPublisher_ASDU_setINT32(asdu, indices[i], voltage_values[(i - VALUES_COUNT) / 2]);
    }

    for (int i = 1; i < VALUES_COUNT * 2; i += 2)
    {
        SVPublisher_ASDU_setINT32(asdu, indices[i], qualities[(i - 1) / 2]);
    }
}

/**
 * @brief Set sinusoidal current and voltage values in the ASDU of the current SV message.
 *
 * @param asdu The ASDU to set the values in.
 * @param indices The indices of the values.
 * @param values The sinusoidal values to set.
 * @param qualities The qualities of the values.
 */
void set_sinusoidal_values(SVPublisher_ASDU asdu, int *indices, SinusoidalValues values, int32_t *qualities)
{
    for (int i = 0; i < VALUES_COUNT; i += 2)
    {
        SVPublisher_ASDU_setINT32(asdu, indices[i], (int32_t)values.current_values[i / 2]);
    }

    for (int i = VALUES_COUNT; i < VALUES_COUNT * 2; i += 2)
    {
        SVPublisher_ASDU_setINT32(asdu, indices[i], (int32_t)values.voltage_values[(i - VALUES_COUNT) / 2]);
    }

    for (int i = 1; i < VALUES_COUNT * 2; i += 2)
    {
        SVPublisher_ASDU_setINT32(asdu, indices[i], qualities[(i - 1) / 2]);
    }
}

/**
 * @brief Reserve memory for the ASDU values.
 *
 * @param asdu The ASDU to reserve memory for.
 * @return int* The indices of the reserved memory.
 */
int *reserve_memory(SVPublisher_ASDU asdu)
{
    int *indices = malloc(VALUES_COUNT * 2 * sizeof(int));
    int index;
    int i = 0;

    for (i = 0; i < VALUES_COUNT * 2; i++)
    {
        index = SVPublisher_ASDU_addINT32(asdu);
        indices[i] = index;
    }
    return indices;
}

/**
 * @brief Free allocated resources.
 */
void freeResources()
{
    if (svPublisher != NULL)
    {
        SVPublisher_destroy(svPublisher);
    }

    if (indices != NULL)
    {
        free(indices);
    }

    if (params != NULL)
    {
        free(params);
    }

    if (values != NULL)
    {
        for (int i = 0; i < SAMPLE_COUNT; i++)
        {
            free(values[i]);
        }
        free(values);
    }

    if (sinusoidal_values != NULL)
    {
        free(sinusoidal_values);
    }
}

/**
 * @brief Signal handler for cleaning up resources on termination.
 *
 * @param signal The signal number.
 */
void handle_signal(int signal)
{
    printf("\nCaught signal %d\n", signal);
    printf("Freeing resources...\n");

    freeResources();
    exit(signal);
}

/**
 * @brief Main function to initialize and run the SV publisher application.
 *
 * This function sets up the SV publisher, installs signal handlers, generates sinusoidal values
 * or reads measurements from files, and enters the main loop to publish SV messages.
 *
 * @param argc Argument count.
 * @param argv Argument vector.
 * @return int Exit status.
 */
int main(int argc, char **argv)
{
    const char *interface;

    if (argc > 1)
        interface = argv[1];
    else
        interface = NETWORK_INTERFACE;

    printf("Using interface %s\n", interface);

    // Setup signal handlers
    signal(SIGINT, handle_signal);
    signal(SIGTSTP, handle_signal);

    params = malloc(sizeof(CommParameters));
    if (params == NULL)
    {
        fprintf(stderr, "Failed to allocate memory for CommParameters\n");
        return 1;
    }

    // Initialize the configuration parameters for creating the SV publisher.
    params->vlanPriority = 6;
    params->vlanId = 3;
    params->appId = 0x4000;
    params->dstAddress[0] = 0x01;
    params->dstAddress[1] = 0x0C;
    params->dstAddress[2] = 0xCD;
    params->dstAddress[3] = 0x04;
    params->dstAddress[4] = 0x01;
    params->dstAddress[5] = 0x01;

    svPublisher = SVPublisher_create(params, interface);
    if (svPublisher == NULL)
    {
        printf("Failed to create SV publisher\n");
        freeResources();
        return 1;
    }

    SVPublisher_ASDU asdu = SVPublisher_addASDU(svPublisher, ASDU_ID, NULL, 1);

    indices = reserve_memory(asdu);
    if (indices == NULL)
    {
        fprintf(stderr, "Failed to allocate memory for indices\n");
        freeResources();
        return 1;
    }

    // Generate normal current and voltage values based on the sinusoidal function defined above.
    SinusoidalValues *normal_sinusoidal_values = generate_values(FREQUENCY, I_MAX, V_MAX, SAMPLE_COUNT);
    printf("%d normal current and voltage values generated using a sinusoidal function:\n", NUM_VALUES_TO_PRINT);
    printSinusoidalValues(normal_sinusoidal_values, NUM_VALUES_TO_PRINT);

    // Generate fault current and voltage values based on the sinusoidal function defined above.
    SinusoidalValues *fault_sinusoidal_values = generate_values(FREQUENCY, FAULT_I_MAX, FAULT_V_MAX, SAMPLE_COUNT);
    printf("%d fault current and voltage values generated using a sinusoidal function:\n", NUM_VALUES_TO_PRINT);
    printSinusoidalValues(fault_sinusoidal_values, NUM_VALUES_TO_PRINT);

    // Read real current and voltage measurements.
    int **real_measurements = readArrayFromFile(PATH_4000_REAL_CURRENTS_AND_VOLTAGES, SAMPLE_COUNT, VALUES_COUNT);
    printf("%d real current and voltage values captured from a merging unit:\n", NUM_VALUES_TO_PRINT);
    display2DArray(real_measurements, NUM_VALUES_TO_PRINT, VALUES_COUNT);

    // Read modified real current and voltage measurements.
    int **modified_real_measurements = readArrayFromFile(PATH_4000_MODIFIED_REAL_CURRENTS_AND_VOLTAGES, SAMPLE_COUNT, VALUES_COUNT);
    printf("%d modified real current and voltage values captured from a merging unit:\n", NUM_VALUES_TO_PRINT);
    display2DArray(modified_real_measurements, NUM_VALUES_TO_PRINT, VALUES_COUNT);

    // Generate static current and voltage values to use them in messages.
    int32_t normal_static_values[VALUES_COUNT] = {0, 0, 0, 0, 0, 0, 0, 0};
    printf("%d normal static current and voltage values:\n", NUM_VALUES_TO_PRINT);
    display1DArray(normal_static_values, VALUES_COUNT);

    // Define fault values which are out of the range of permitted/normal currents and voltages.
    int32_t fault_static_values[VALUES_COUNT] = {FAULT_I_MAX, FAULT_I_MAX, FAULT_I_MAX, FAULT_I_MAX * 3, FAULT_V_MAX, FAULT_V_MAX, FAULT_V_MAX, FAULT_V_MAX * 3};
    printf("%d fault static current and voltage values:\n", NUM_VALUES_TO_PRINT);
    display1DArray(fault_static_values, VALUES_COUNT);

    // In this program, the third/last current and voltage value is the sum of the three previous current/voltage values.
    // The quality of each third value is set to "derived" to indicate that this value has been calculated based on the three previous values.
    int32_t qualities[VALUES_COUNT] = {0, 0, 0, 0x00002000, 0, 0, 0, 0x00002000};

    struct timespec startTime, currentTime;
    Timestamp timestamp;

    int num_sequences = 2000;
    int counter = 0;

    SVPublisher_setupComplete(svPublisher);

    SVPublisher_ASDU_setSmpSynch(asdu, SMP_Synch);

    while (counter < num_sequences)
    {
        for (int i = 0; i < SAMPLE_COUNT; i++)
        {
            // Store the time of starting executing the current loop iteration.
            clock_gettime(CLOCK_REALTIME, &startTime);
            Timestamp_clearFlags(&timestamp);
            Timestamp_setTimeInMilliseconds(&timestamp, Hal_getTimeInMs());

            // Set the values in the ASDU of the current SV message.
            set_values(asdu, indices, normal_static_values, qualities);
            // set_values(asdu, indices, values[i], qualities);
            // set_sinusoidal_values(asdu, indices, normal_sinusoidal_values[i], qualities);

            // Set the timestamp and the quality for the ASDU.
            // set_parameters(asdu, indices, timestamp, QUALITY);

            // Publish the SV message.
            SVPublisher_publish(svPublisher);

            // Update the sample counter to identify each SV messages within the sequence.
            SVPublisher_ASDU_increaseSmpCnt(asdu);

            // Store the time of finishing executing the current loop iteration.
            clock_gettime(CLOCK_REALTIME, &currentTime);
            // Calculate elapsed time and sleep for the remaining time to maintain the desired sample rate.
            long elapsed_nsec = (currentTime.tv_sec - startTime.tv_sec) * NANOSECONDS_IN_SECOND + (currentTime.tv_nsec - startTime.tv_nsec);
            long sleep_time = NANOSECONDS_PER_SAMPLE - elapsed_nsec;

            // This value has been determined experimentally and turned out to be the best sleep time to achieve
            // a stable sample rate of 4000 samples/second on the Raspberry Pi 4 used in the experiments.
            // long sleep_time = 32900;
            struct timespec req = {0, sleep_time};
            nanosleep(&req, NULL);
        }

        // Reset the sample counter once a sequence of SV messages has been published.
        SVPublisher_ASDU_setSmpCnt(asdu, 0);
        counter++;
    }

    printf("Freeing resources...\n");
    freeResources();
    return 0;
}
