/*
 * This program simulates a Sampled Values (SV) subscriber.
 * The simulation involves listening on a network interface for published SV frames and printing the information
 * stored in the received SV messages, such as measurements and quality values.
 *
 * This module has been created as part of a bachelor's thesis on the topic
 * "Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations".
 * The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.
 * The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:
 * https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.
 *
 * Caution: Please note that the code in this module does not include extensive input validation or error checking.
 * The inputs and function parameters have been provided by the author in a controlled environment.
 * Consider adding error checking and handling functionalities as needed.
 *
 * Disclaimer: This code is provided "as-is", without any express or implied warranty. In no event shall the author or
 * contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.
 *
 * Acknowledgment: This code uses functionalities from the open-source libiec61850 library (https://libiec61850.com/)
 * for simulating the SV subscriber.
 *
 * Author: Ahmad Eynawi
 * Institute: Institute for Automation and Applied Informatics (IAI)
 * Department: Department of Informatics
 * University: Karlsruhe Institute of Technology (KIT)
 * Date: April 10, 2024
 */

#include "hal_thread.h"
#include <signal.h>
#include <stdio.h>
#include "sv_subscriber.h"

// Global variable to control the running state of the application.
static bool running = true;

// Counters for tracking received SV messages.
static int counter1 = 1;
static int counter2 = 0;

/**
 * @brief Signal handler for SIGINT to stop the application.
 *
 * @param signalId The signal number.
 */
void sigint_handler(int signalId)
{
    running = 0;
}

/**
 * @brief Callback handler for received SV messages.
 *
 * This function is called whenever a new SV message is received. It processes
 * the message and extracts information such as svID, smpCnt, confRev, and others.
 *
 * @param subscriber The SV subscriber.
 * @param parameter User-defined parameter (unused).
 * @param asdu The Application Service Data Unit (ASDU) containing the SV message data.
 */
static void svUpdateListener(SVSubscriber subscriber, void *parameter, SVSubscriber_ASDU asdu)
{
    printf("svUpdateListener called\n");
    counter1++;

    const char *svID = SVSubscriber_ASDU_getSvId(asdu);

    printf("*** Received SV message *** %s\n", svID);

    if (svID != NULL)
        printf("  svID=(%s)\n", svID);

    printf("  smpCnt: %i\n", SVSubscriber_ASDU_getSmpCnt(asdu));
    printf("  confRev: %u\n", SVSubscriber_ASDU_getConfRev(asdu));
    printf("  smpSynch: %i\n", SVSubscriber_ASDU_getSmpSynch(asdu));

    if (SVSubscriber_ASDU_hasSmpRate(asdu))
        printf("  smpRate: %i\n", SVSubscriber_ASDU_getSmpRate(asdu));

    if (SVSubscriber_ASDU_hasSmpMod(asdu))
        printf("  smpMod: %i\n", SVSubscriber_ASDU_getSmpMod(asdu));

    /*
     * Access to the data requires a priori knowledge of the data set.
     * For this example we assume a data set consisting of FLOAT32 values.
     * A FLOAT32 value is encoded as 4 bytes. You can find the first FLOAT32
     * value at byte position 0, the second value at byte position 4, the third
     * value at byte position 8, and so on.
     *
     * To prevent damages due configuration, please check the length of the
     * data block of the SV message before accessing the data.
     */

    int asduDataSize = 16 * 4; // Assume 16 FLOAT32 values, each 4 bytes
    for (int i = 0; i < asduDataSize; i += 8)
    {
        printf("   DATA[%i]: %i\n", i, SVSubscriber_ASDU_getINT32(asdu, i));
        printf("   QUALITY[%i]: %i\n", i, SVSubscriber_ASDU_getINT32(asdu, i + 4));
    }
}

/**
 * @brief Main function to initialize and run the SV subscriber application.
 *
 * This function sets up the SV receiver and subscriber, installs the signal handler,
 * and enters the main loop to listen for SV messages.
 *
 * @param argc Argument count.
 * @param argv Argument vector.
 * @return int Exit status.
 */
int main(int argc, char **argv)
{
    SVReceiver receiver = SVReceiver_create();
    size_t arraySize = 6;
    uint8_t *ethAddr = (uint8_t *)malloc(arraySize * sizeof(uint8_t));

    ethAddr[0] = 0x01;
    ethAddr[1] = 0x0C;
    ethAddr[2] = 0xCD;
    ethAddr[3] = 0x04;
    ethAddr[4] = 0x01;
    ethAddr[5] = 0x01;

    while (running)
    {
        signal(SIGINT, sigint_handler);
        Thread_sleep(1);

        // Workaround to enable the program to receive multiple SV messages without the need to restart it.
        if (counter1 != counter2)
        {
            // Destroy the receiver object and recreate it to receive new puslibhed messages.
            SVReceiver_stop(receiver);
            SVReceiver_destroy(receiver);
            receiver = SVReceiver_create();
            SVReceiver_enableDestAddrCheck(receiver);

            if (argc > 1)
            {
                SVReceiver_setInterfaceId(receiver, argv[1]);
                printf("Set interface: %s\n", argv[1]);
            }
            else
            {
                printf("\nUsing interface eth0\n");
                SVReceiver_setInterfaceId(receiver, "eth0");
            }

            // Create a subscriber listening to SV messages with APPID 4000.
            SVSubscriber subscriber = SVSubscriber_create(ethAddr, 0x4000);

            // Install a callback handler for the subscriber.
            SVSubscriber_setListener(subscriber, svUpdateListener, NULL);

            // Connect the subscriber to the receiver.
            SVReceiver_addSubscriber(receiver, subscriber);

            // Start listening to SV messages. This starts a new receiver background thread.
            SVReceiver_start(receiver);

            Thread_sleep(2);

            printf("Receiver is running? %s\n", SVReceiver_isRunning(receiver) ? "true" : "false");

            counter2++;
        }
    }

    // Cleanup and free resources.
    printf("Freeing resources...\n");
    SVReceiver_stop(receiver);
    SVReceiver_destroy(receiver);
    free(ethAddr);
    return 0;
}
