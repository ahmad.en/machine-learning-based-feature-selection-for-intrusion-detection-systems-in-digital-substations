"""
This utility module implements functions that are shared across multiple notebooks used for selecting important features
for the SV and MMS protocols.
It includes functions for loading datasets, data visualization, hyperparameter tuning, dataset manipulation,
and model evaluation.

This notebook has been created as part of a bachelor's thesis on the topic
"Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations".
The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.
The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:
https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.

Caution: Please note that the code in this notebook does not include extensive input validation or error checking.
The inputs and function parameters have been provided by the author in a controlled environment.
Consider adding error checking and handling functionalities as needed.

Disclaimer: This code is provided "as-is", without any express or implied warranty. In no event shall the author or
contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.

Author: Ahmad Eynawi
Institute: Institute for Automation and Applied Informatics (IAI)
Department: Department of Informatics
University: Karlsruhe Institute of Technology (KIT)
Date: April 10, 2024
"""

import numpy as np
import pandas as pd
import seaborn as sns
from sklearn.metrics import accuracy_score, confusion_matrix
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
from constants import TARGET_COLUMN_NAME



def plot_feature_importances(feature_importances, title='Visualizing Important Features', xlabel='Feature Names',
                             ylabel='Feature Importances', figsize=(14, 10), rotation=45, horizontal_alignment='right',
                             color='blue', bbox_inches='tight', dpi=300, save_path=None):
    """
    Plot the feature importance scores.

    Parameters:
    feature_importances (pd.Series): Series containing feature importance scores.
    title (str): Title of the plot.
    xlabel (str): Label for the x-axis.
    ylabel (str): Label for the y-axis.
    figsize (tuple): Figure size.
    rotation (int): Rotation angle for x-axis labels.
    horizontal_alignment (str): Horizontal alignment of x-axis labels.
    color (str): Color for the plot.
    bbox_inches (str): Bounding box inches for saving the plot.
    dpi (int): Dots per inch for the saved plot.
    save_path (str): Path to save the plot.

    Returns:
    None
    """

    plt.figure(figsize=figsize)

    num_features = len(feature_importances)
    palette = sns.cubehelix_palette(start=2.7, rot=0, dark=0.4, light=0.8, n_colors=num_features, reverse=True)

    sns.barplot(x=feature_importances.index, y=feature_importances, palette=palette, color=color)

    plt.xlabel(xlabel, fontsize=15)
    plt.ylabel(ylabel, fontsize=15)
    plt.title(title, fontsize=20)
    plt.xticks(rotation=rotation, ha=horizontal_alignment)
    plt.tight_layout()

    if save_path:
        plt.savefig(save_path, dpi=dpi, bbox_inches=bbox_inches)

    plt.show()


def plot_confusion_matrix(y_test, y_pred, target_classes, xlabel='Predicted Labels', ylabel='True Labels',
                          title='Confusion Matrix', dpi=300, bbox_inches='tight', save_path=None):
    """
    Plot the confusion matrix for the given true and predicted labels.

    Parameters:
    y_test (np.array): True labels.
    y_pred (np.array): Predicted labels.
    target_classes (list): List of target class names.
    xlabel (str): Label for the x-axis.
    ylabel (str): Label for the y-axis.
    title (str): Title of the plot.
    dpi (int): Dots per inch for the saved plot.
    bbox_inches (str): Bounding box inches for saving the plot.
    save_path (str): Path to save the plot.

    Returns:
    None
    """

    cm = confusion_matrix(y_test, y_pred)

    plt.figure(figsize=(8, 6))
    sns.heatmap(data=cm, annot=True, fmt='g', cmap='Blues', xticklabels=target_classes, yticklabels=target_classes)

    plt.xlabel(xlabel, fontsize=12)
    plt.ylabel(ylabel, fontsize=12)
    plt.title(title, fontsize=14)

    if save_path:
        plt.savefig(save_path, dpi=dpi, bbox_inches=bbox_inches)

    plt.show()


def plot_scores_across_generations(scores, generations, x_lim=0.9, y_lim=1.0, color="b", title="Accuracy Evolution across Generations",
                                   dpi=300, bbox_inches='tight', save_path=None):
    """
    Plot the accuracy evolution scores across multiple generations of solutions computed using optimization methods.

    Parameters:
    scores (list): List of accuracy scores.
    generations (list): List of generations.
    x_lim (float): X-axis limit for the plot.
    y_lim (float): Y-axis limit for the plot.
    color (str): Color of the plot points.
    title (str): Title of the plot.
    dpi (int): Dots per inch for the saved figure.
    bbox_inches (str): Bounding box inches for the saved figure.
    save_path (str): Path to save the plot.

    Returns:
    None
    """

    plt.figure(figsize=(10, 8))
    ax = sns.pointplot(x=generations, y=scores, color=color)
    ax.set(xlabel="Generation", ylabel="Accuracy")
    ax.set(ylim=(x_lim, y_lim))
    ax.set_title(title)
    if save_path:
        plt.savefig(save_path, dpi=dpi, bbox_inches=bbox_inches)


def get_normalized_important_features(feature_importances, feature_names, num_important_features):
    """
    Normalize the given feature importance scores to the range [0, 1].

    Parameters:
    feature_importances (list): List of feature importance scores.
    feature_names (list): List of feature names.
    num_important_features (int): Number of important features to select.

    Returns:
    pd.Series: Normalized feature importance scores.
    """

    # Create a MinMaxScaler instance for normalizing the feature importance scores.
    scaler = MinMaxScaler()

    # Convert the list of important features to a 2D numpy array as MinMaxScaler expects 2D input.
    importance_values = np.array(feature_importances).reshape(-1, 1)

    # Fit the scaler to the importance scores and transform them.
    importance_values = scaler.fit_transform(importance_values)

    # Flatten the scaled values back into a 1D list.
    importance_values = importance_values.flatten().tolist()

    # Convert the normalized feature importance scores to a Series object for later use.
    feature_importances = pd.Series(data=importance_values, index=feature_names).sort_values(ascending=False)

    # Select the specified number of important features.
    important_features = feature_importances[:num_important_features]

    return important_features


def compare_results_with_and_without_feature_selection(model, X_train, y_train, X_test, y_test, y_pred_without_fs,
                                                       feature_importances, num_important_features):
    """
    Compare accuracies of the given model on the test dataset with and without feature selection.

    Parameters:
    model (object): The baseline evaluation model.
    X_train (pd.DataFrame): Training feature set.
    y_train (pd.Series): Training labels.
    X_test (pd.DataFrame): Test feature set.
    y_test (pd.Series): Test labels.
    y_pred_without_fs (np.array): Predictions without feature selection.
    feature_importances (list): List of feature importance scores.
    num_important_features (int): Number of important features to select.

    Returns:
    np.array: Predictions with feature selection.
    """

    # Fit the model on the training dataset with all features.
    # model.fit(X_train, y_train)

    # Predict the results on the test dataset with all features.
    # y_pred_without_fs = model.predict(X_test)

    # Get the indices of important features.
    indices_important_features = np.argsort(feature_importances)[::-1][:num_important_features]

    # Transform the datasets such that they include only important features.
    X_train_transformed = X_train.iloc[:, indices_important_features]
    X_test_transformed = X_test.iloc[:, indices_important_features]

    # Fit the model on the training dataset with selected features.
    model.fit(X_train_transformed, y_train)

    # Predict the results on the test dataset with selected features.
    y_pred_with_fs = model.predict(X_test_transformed)

    # Compare the numbers of features with and without feature selection.
    print('Number of features before feature selection: {}'.format(X_train.shape[1]))
    print('Number of features after feature selection: {}'.format(X_train_transformed.shape[1]))
    print('Names of selected features:\n', X_train_transformed.columns)

    # Compare the accuracies with and without feature selection.
    print('Accuracy without feature selection : {0:0.9f}'.format(accuracy_score(y_test, y_pred_without_fs)))
    print('Accuracy classifier with feature selection: {0:0.9f}'.format(accuracy_score(y_test, y_pred_with_fs)))

    return y_pred_with_fs


def run_hyperparameter_grid_search(model, parameters_grid, X_train, y_train):
    """
    Run a hyperparameter grid search for finding good hyperparameters for the given model.

    Parameters:
    model (object): The machine learning model.
    parameters_grid (dict): Dictionary with parameters names (str) as keys and lists of parameter settings to try as values.
    X_train (pd.DataFrame): Training feature set.
    y_train (pd.Series): Training labels.

    Returns:
    object: Tuned model.
    """

    # Initialize the grid search object.
    model_gs = GridSearchCV(model, param_grid=parameters_grid,
                            cv=5, verbose=0, n_jobs=-1)
    model_gs.fit(X_train, y_train)

    # Create a variable for the tuned model according to the hyperparameter grid search.
    tuned_model = model_gs.best_estimator_
    tuned_model.fit(X_train, y_train)

    # Print the tuned hyperparameters.
    print('Grid Search - Tuned Hyperparameters Values:')
    for key, value in model_gs.best_params_.items():
        print(f"{key}: {value}")

    return tuned_model


def run_hyperparameter_randomized_search(model, parameters_grid, X_train, y_train):
    """
    Run a hyperparameter randomized search for finding good hyperparameters for the given model.

    Parameters:
    model (object): The machine learning model.
    parameters_grid (dict): Dictionary with parameters names (str) as keys and lists of parameter settings to try as values.
    X_train (pd.DataFrame): Training feature set.
    y_train (pd.Series): Training labels.

    Returns:
    object: Tuned model.
    """

    # Initialize the randomized search object.
    model_rs = RandomizedSearchCV(
        estimator=model, param_distributions=parameters_grid,
        n_iter=100, cv=5, verbose=0, random_state=42, n_jobs=-1
    )
    model_rs.fit(X_train, y_train)

    # Create a variable for the tuned model according to the hyperparameter randomized search.
    tuned_model = model_rs.best_estimator_

    # Print the tuned hyperparameters.
    print('Randomized Search - Tuned Hyperparameter Values:')
    for key, value in model_rs.best_params_.items():
        print(f"{key}: {value}")

    return tuned_model


def load_dataset(train_set_path, test_set_path):
    """
    Load the training and test datasets at the given path along with feature names and target classes.

    Parameters:
    train_set_path (str): Path to the training set CSV file.
    test_set_path (str): Path to the test set CSV file.

    Returns:
    tuple: Training feature set, training labels, test feature set, test labels, feature names, and target classes.
    """

    # Load the datasets.
    train_set = pd.read_csv(train_set_path)
    test_set = pd.read_csv(test_set_path)

    # Declare input samples and target variables.
    X_train = train_set.drop([TARGET_COLUMN_NAME], axis=1)
    y_train = train_set[TARGET_COLUMN_NAME]
    X_test = test_set.drop([TARGET_COLUMN_NAME], axis=1)
    y_test = test_set[TARGET_COLUMN_NAME]

    # Get feature names.
    feature_names = X_train.columns

    # Get class names.
    target_classes = y_train.unique()

    return X_train, y_train, X_test, y_test, feature_names, target_classes


def print_metadata_about_dataset(X_train, y_train, X_test, y_test):
    """
    Print some metadata about the training and test datasets.

    Parameters:
    X_train (pd.DataFrame): Training feature set.
    y_train (pd.Series): Training labels.
    X_test (pd.DataFrame): Test feature set.
    y_test (pd.Series): Test labels.

    Returns:
    None
    """

    print("Dimension of the training dataset:", X_train.shape)
    print("First 5 elements of the training dataset:")
    X_train.head()

    print("Dimension of the test dataset:", X_test.shape)
    print("First 5 elements of the test dataset:")
    X_test.head()

    # View the names of the class labels.
    target_classes = y_train.unique()
    print("Class label names:\n", target_classes)

    # View the frequency of the class labels.
    label_counts_train = y_train.value_counts()
    print("Frequency of class labels in the training dataset:")
    print(label_counts_train)
    label_counts_test = y_test.value_counts()
    print("Frequency of class labels in the test dataset:")
    print(label_counts_test)

    # View the number of unique labels.
    num_unique_labels = y_train.nunique()
    print(f"Number of unique labels in '{TARGET_COLUMN_NAME}': {num_unique_labels}")


def calculate_accuracy(model, X_train, y_train, X_test, y_test):
    """
    Fit the given model on the training dataset and calculate its accuracy on the test dataset.

    Parameters:
    model (object): The machine learning model.
    X_train (pd.DataFrame): Training feature set.
    y_train (pd.Series): Training labels.
    X_test (pd.DataFrame): Test feature set.
    y_test (pd.Series): Test labels.

    Returns:
    float: Accuracy score of the model on the test dataset.
    """

    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    return accuracy


def is_boolean_list(input_list):
    """
    Check if the given list consists only of `True`, `False`, `0`, and `1` values.

    Parameters:
    input_list (list): The list to be checked.

    Returns:
    bool: `True` if the list consists only of `True`, `False`, `0`, and `1` values, otherwise `False`.
    """

    for element in input_list:
        if element not in [True, False, 0, 1]:
            return False
    return True

