"""
This module stores constants that are used across multiple notebooks in this package.
The constants are related to the algorithms used for selecting important features for the SV and MMS protocols.

This notebook has been created as part of a bachelor's thesis on the topic
"Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations".
The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.
The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:
https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.

Disclaimer: This code is provided "as-is", without any express or implied warranty. In no event shall the author or
contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.

Author: Ahmad Eynawi
Institute: Institute for Automation and Applied Informatics (IAI)
Department: Department of Informatics
University: Karlsruhe Institute of Technology (KIT)
Date: April 10, 2024
"""


# Dataset-specific constants.
TARGET_COLUMN_NAME = 'class'
NUM_IMPORTANT_FEATURES = 4
# FEATURE_IMPORTANCE_THRESHOLD = 0.20

# Classifier-specific constants.
RFC_N_ESTIMATORS = 100
RFC_BOOTSTRAP = False

# General paths.
COMMON_DATASETS_PATH = "..\\..\\Datasets\\"
COMMON_PLOTS_PATH = '..\\..\\Plots\\'
COMMON_BINARY_OBJECTS_PATH = '..\\..\\Binary Objects\\'

# Adapt these paths to access the dataset that should be used for feature selection.
FULL_DATASET_PATH = COMMON_DATASETS_PATH + 'MMS Datasets\\Full Dataset\\mms_normal_fault_modification_delay_full_dataset.csv'
TRAINING_DATASET_PATH = COMMON_DATASETS_PATH + 'MMS Datasets\\Training Dataset\\mms_normal_fault_modification_delay_training_dataset.csv'
TEST_DATASET_PATH = COMMON_DATASETS_PATH + 'MMS Datasets\\Test Dataset\\mms_normal_fault_modification_delay_test_dataset.csv'
PLOTS_PATH = COMMON_PLOTS_PATH + 'MMS\\'
FEATURE_IMPORTANCES_PATH = COMMON_BINARY_OBJECTS_PATH + 'Feature Importances\\MMS\\'
CLASSIFIERS_PATH = COMMON_BINARY_OBJECTS_PATH + 'Classifiers\\MMS\\'

# Paths for the SV dataset.
SV_FULL_DATASET_PATH = COMMON_DATASETS_PATH + 'SV Datasets\\Full Dataset\\sv_normal_fault_replay_injection_full_dataset.csv'
SV_TRAINING_DATASET_PATH = COMMON_DATASETS_PATH + 'SV Datasets\\Training Dataset\\sv_normal_fault_replay_injection_training_dataset.csv'
SV_TEST_DATASET_PATH = COMMON_DATASETS_PATH + 'SV Datasets\\Test Dataset\\sv_normal_fault_replay_injection_test_dataset.csv'
SV_PLOTS_PATH = COMMON_PLOTS_PATH + 'SV\\'
SV_FEATURE_IMPORTANCES_PATH = COMMON_BINARY_OBJECTS_PATH + 'Feature Importances\\SV\\'
SV_CLASSIFIERS_PATH = COMMON_BINARY_OBJECTS_PATH + 'Classifiers\\SV\\'

# Paths for the MMS dataset.
MMS_FULL_DATASET_PATH = COMMON_DATASETS_PATH + 'MMS Datasets\\Full Dataset\\mms_normal_fault_modification_delay_full_dataset.csv'
MMS_TRAINING_DATASET_PATH = COMMON_DATASETS_PATH + 'MMS Datasets\\Training Dataset\\mms_normal_fault_modification_delay_training_dataset.csv'
MMS_TEST_DATASET_PATH = COMMON_DATASETS_PATH + 'MMS Datasets\\Test Dataset\\mms_normal_fault_modification_delay_test_dataset.csv'
MMS_PLOTS_PATH = COMMON_PLOTS_PATH + 'MMS\\'
MMS_FEATURE_IMPORTANCES_PATH = COMMON_BINARY_OBJECTS_PATH + 'Feature Importances\\MMS\\'
MMS_CLASSIFIERS_PATH = COMMON_BINARY_OBJECTS_PATH + 'Classifiers\\MMS\\'

# Paths for the open-source ERENO dataset.
ERENO_TRAINING_DATASET_PATH = COMMON_DATASETS_PATH + 'ERENO Datasets\\Training Dataset\\ereno_normal_fault_injection_replay_masquerade_training_dataset.csv'
ERENO_TEST_DATASET_PATH = COMMON_DATASETS_PATH + 'ERENO Datasets\\Test Dataset\\ereno_normal_fault_injection_replay_masquerade_test_dataset.csv'
ERENO_PLOTS_PATH = COMMON_PLOTS_PATH + 'ERENO\\'
ERENO_FEATURE_IMPORTANCES_PATH = COMMON_BINARY_OBJECTS_PATH + 'Feature Importances\\ERENO\\'
ERENO_CLASSIFIERS_PATH = COMMON_BINARY_OBJECTS_PATH + 'Classifiers\\ERENO\\'

