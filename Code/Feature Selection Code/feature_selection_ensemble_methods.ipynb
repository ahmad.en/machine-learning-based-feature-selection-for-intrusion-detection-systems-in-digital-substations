{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "This notebook performs feature selection using an ensemble model.\n",
    "The feature selection process involves selecting important features, plotting the feature importance scores, and saving the results for later use.\n",
    "The methods used in the ensemble model are Random Forest, Extremely Randomized Trees, Variance Threshold, Mutual Information, Sequential Forward Feature Selection, Sequential Backward Feature Selection, Recursive Feature Elimination, Genetic Algorithm, and Particle Swarm Optimization.\n",
    "\n",
    "This notebook has been created as part of a bachelor's thesis on the topic\n",
    "\"Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations\".\n",
    "The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.\n",
    "The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:\n",
    "https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.\n",
    "\n",
    "Caution: Please note that the code in this notebook does not include extensive input validation or error checking.\n",
    "The inputs and function parameters have been provided by the author in a controlled environment.\n",
    "Consider adding error checking and handling functionalities as needed.\n",
    "\n",
    "Disclaimer: This code is provided \"as-is\", without any express or implied warranty. In no event shall the author or\n",
    "contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.\n",
    "\n",
    "Author: Ahmad Eynawi\n",
    "Institute: Institute for Automation and Applied Informatics (IAI)\n",
    "Department: Department of Informatics\n",
    "University: Karlsruhe Institute of Technology (KIT)\n",
    "Date: April 10, 2024\n",
    "\"\"\""
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import pickle\n",
    "from itertools import islice\n",
    "from sklearn.preprocessing import MinMaxScaler\n",
    "from sklearn.metrics import accuracy_score\n",
    "\n",
    "from ..utility import plot_feature_importances, plot_confusion_matrix, compare_results_with_and_without_feature_selection, load_dataset, print_metadata_about_dataset, get_normalized_important_features\n",
    "\n",
    "from ..constants import FEATURE_IMPORTANCES_PATH, CLASSIFIERS_PATH, NUM_IMPORTANT_FEATURES, TRAINING_DATASET_PATH, TEST_DATASET_PATH, PLOTS_PATH\n",
    "\n",
    "%matplotlib inline\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")\n",
    "\n",
    "# from google.colab import drive\n",
    "# drive.mount('/content/drive')\n"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Datasets"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Get input samples and target variables.\n",
    "X_train, y_train, X_test, y_test, feature_names, target_classes = load_dataset(train_set_path=TRAINING_DATASET_PATH, test_set_path=TEST_DATASET_PATH)\n",
    "\n",
    "# Print some metadata about the test and training datasets.\n",
    "print_metadata_about_dataset(X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Baseline Evaluation Model"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "outputs": [],
   "source": [
    "# Load the random forest classifier to use it as a baseline model for evaluating all feature selectors.\n",
    "with open(CLASSIFIERS_PATH + 'Random-Forest-Classifier.pkl', 'rb') as f:\n",
    "    rfc = pickle.load(f)\n",
    "\n",
    "# Fit the random forest classifier on the training dataset with all features.\n",
    "rfc.fit(X_train, y_train)\n",
    "\n",
    "# Make predictions on the test dataset without feature selection.\n",
    "y_pred_without_fs = rfc.predict(X_test)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Ensemble Model"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "outputs": [],
   "source": [
    "# Load the feature importance scores obtained by all feature selectors.\n",
    "\n",
    "with open(FEATURE_IMPORTANCES_PATH + 'Feature-Importances-Random-Forest.pkl', 'rb') as f:\n",
    "    feature_importances_rfc = pickle.load(f)\n",
    "\n",
    "with open(FEATURE_IMPORTANCES_PATH + 'Feature-Importances-Extremely-Randomized-Trees.pkl', 'rb') as f:\n",
    "    feature_importances_etc = pickle.load(f)\n",
    "\n",
    "with open(FEATURE_IMPORTANCES_PATH + 'Feature-Importances-Variance-Threshold.pkl', 'rb') as f:\n",
    "    feature_importances_variance = pickle.load(f)\n",
    "\n",
    "with open(FEATURE_IMPORTANCES_PATH + 'Feature-Importances-Mutual-Information.pkl', 'rb') as f:\n",
    "    feature_importances_mi = pickle.load(f)\n",
    "\n",
    "with open(FEATURE_IMPORTANCES_PATH + 'Feature-Importances-Sequential-Forward-Selection.pkl', 'rb') as f:\n",
    "    feature_importances_sfs = pickle.load(f)\n",
    "\n",
    "with open(FEATURE_IMPORTANCES_PATH + 'Feature-Importances-Sequential-Backward-Selection.pkl', 'rb') as f:\n",
    "    feature_importances_sbs = pickle.load(f)\n",
    "\n",
    "with open(FEATURE_IMPORTANCES_PATH + 'Feature-Importances-Recursive-Feature-Elimination.pkl', 'rb') as f:\n",
    "    feature_importances_rfe = pickle.load(f)\n",
    "\n",
    "with open(FEATURE_IMPORTANCES_PATH + 'Feature-Importances-Genetic-Algorithm.pkl', 'rb') as f:\n",
    "    feature_importances_ga = pickle.load(f)\n",
    "\n",
    "with open(FEATURE_IMPORTANCES_PATH + 'Feature-Importances-Particle-Swarm-Optimization.pkl', 'rb') as f:\n",
    "    feature_importances_pso = pickle.load(f)\n",
    "\n",
    "# with open(FEATURE_IMPORTANCES_PATH + 'Feature-Importances-Extreme-Gradient-Boosting.pkl', 'rb') as f:\n",
    "#     feature_importances_xgbc = pickle.load(f)\n",
    "#\n",
    "# with open(FEATURE_IMPORTANCES_PATH + 'Feature-Importances-ANOVA.pkl', 'rb') as f:\n",
    "#     feature_importances_anova = pickle.load(f)\n",
    "#\n",
    "# with open(FEATURE_IMPORTANCES_PATH + 'Feature-Importances-Exhaustive-Feature-Selection.pkl', 'rb') as f:\n",
    "#     feature_importances_efs = pickle.load(f)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "id": "j492BVqB4B7f"
   },
   "outputs": [],
   "source": [
    "def normalize_accuracies(accuracies):\n",
    "    \"\"\"\n",
    "    Normalize the accuracies of feature selectors such that the maximum accuracy becomes 1.\n",
    "\n",
    "    Parameters:\n",
    "    accuracies (dict): Dictionary where keys are feature selector names and values are their corresponding accuracies.\n",
    "\n",
    "    Returns:\n",
    "    dict: Dictionary with normalized accuracies.\n",
    "    \"\"\"\n",
    "\n",
    "    # Initialize a dictionary to store the normalized accuracies.\n",
    "    accuracies_transformed = {}\n",
    "\n",
    "    # Calculate the maximum accuracy.\n",
    "    max_accuracy = max(accuracies.values())\n",
    "\n",
    "    # Normalize the feature selector accuracies.\n",
    "    for feature_selector_name, accuracy in accuracies.items():\n",
    "        accuracies_transformed[feature_selector_name] = accuracy / max_accuracy\n",
    "\n",
    "    return accuracies_transformed\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "outputs": [],
   "source": [
    "def evaluate_feature_selectors(feature_importances_by_selector, base_model):\n",
    "    \"\"\"\n",
    "    Evaluate the accuracy of different feature selectors using a baseline evaluation model.\n",
    "\n",
    "    Parameters:\n",
    "    feature_importances_by_selector (dict): Dictionary where keys are feature selector names and values are feature importances already calculated by these feature selectors.\n",
    "    base_model (sklearn model): The baseline evaluation model to be used (e.g., RandomForestClassifier).\n",
    "\n",
    "    Returns:\n",
    "    dict: Dictionary with normalized accuracies of feature selectors.\n",
    "    \"\"\"\n",
    "\n",
    "    # Initialize a dictionary to store the accuracy of each feature selector.\n",
    "    accuracies = {}\n",
    "\n",
    "    # Evaluate each feature selector using the provided baseline model.\n",
    "    for feature_selector_name, features in feature_importances_by_selector.items():\n",
    "        X_train_transformed = X_train[features.index]\n",
    "        X_test_transformed = X_test[features.index]\n",
    "        base_model.fit(X_train_transformed, y_train)\n",
    "        y_pred = base_model.predict(X_test_transformed)\n",
    "        accuracy = accuracy_score(y_test, y_pred)\n",
    "        accuracies[feature_selector_name] = accuracy\n",
    "\n",
    "    # Normalize the accuracies such that the maximum accuracy becomes 1.\n",
    "    accuracies = normalize_accuracies(accuracies)\n",
    "\n",
    "    return accuracies\n"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "id": "ATsyH-4SSiNy"
   },
   "outputs": [],
   "source": [
    "def calculate_feature_weights(feature_importances_by_selector, feature_selector_accuracies):\n",
    "    \"\"\"\n",
    "    Calculate the final feature weights as a weighted sum of the scores assigned by all feature selectors.\n",
    "\n",
    "    Parameters:\n",
    "    feature_importances_by_selector (dict): Dictionary where keys are feature selector names and values are feature importances already calculated by these feature selectors.\n",
    "    feature_selector_accuracies (dict): Dictionary where keys are feature selector names and values are their normalized accuracies.\n",
    "\n",
    "    Returns:\n",
    "    dict: Dictionary with sorted feature weights in descending order.\n",
    "    \"\"\"\n",
    "\n",
    "    # Initialize a dictionary to store the final feature weights.\n",
    "    feature_weights = {}\n",
    "\n",
    "    # Compute the final importance score for each feature as a weighted sum of the scores assigned to the feature by all feature selectors. The feature importance assigned by each feature selector is multiplied by the accuracy of that selector to account for the strengths of different feature selectors.\n",
    "    for feature_selector_name, feature_importances in feature_importances_by_selector.items():\n",
    "        accuracy = feature_selector_accuracies.get(feature_selector_name, 0)\n",
    "\n",
    "        for feature_name, feature_importance in feature_importances.items():\n",
    "            if feature_name not in feature_weights:\n",
    "                feature_weights[feature_name] = 0\n",
    "            feature_weights[feature_name] += feature_importance * accuracy\n",
    "\n",
    "    # Sort the feature weights in descending order.\n",
    "    sorted_feature_weights = dict(sorted(feature_weights.items(), key=lambda x: x[1], reverse=True))\n",
    "    return sorted_feature_weights\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "id": "r1od2QSpEiYQ"
   },
   "outputs": [],
   "source": [
    "# Create a dictionary to store all feature importances with named feature selectors.\n",
    "feature_importances_by_selector = {\n",
    "    'Random Forest': feature_importances_rfc,\n",
    "    'Extremely Randomized Trees': feature_importances_etc,\n",
    "    'Variance Threshold': feature_importances_variance,\n",
    "    'Mutual Information': feature_importances_mi,\n",
    "    'Sequential Forward Selector': feature_importances_sfs,\n",
    "    'Sequential Backward Selector': feature_importances_sbs,\n",
    "    'Recursive Feature Elimination': feature_importances_rfe,\n",
    "    'Genetic Algorithm': feature_importances_ga,\n",
    "    'Particle Swarm Optimization': feature_importances_pso\n",
    "    # 'ANOVA': feature_importances_anova,\n",
    "    # 'Extreme Gradient Boosting': feature_importances_xgbc,\n",
    "    # 'Exhaustive Feature Selector': feature_importances_efs\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "id": "9niDVvMJGsa6"
   },
   "outputs": [],
   "source": [
    "# Compute the accuracies of all feature selectors.\n",
    "feature_selector_accuracies = evaluate_feature_selectors(feature_importances_by_selector=feature_importances_by_selector, base_model=rfc)\n",
    "\n",
    "# Compute the final weight for each feature.\n",
    "feature_weights = calculate_feature_weights(feature_importances_by_selector=feature_importances_by_selector, feature_selector_accuracies=feature_selector_accuracies)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "TeNBKJ773wz7",
    "outputId": "0b2fd131-c211-47b1-9701-c830fc6c531d",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "print(\"Accuracies of the feature selectors using a random forest classifier as a baseline evaluation model:\\n\")\n",
    "for key, value in feature_selector_accuracies.items():\n",
    "    print(f'{key}: {value}')\n",
    "\n",
    "print(f\"\\n\\nThe {NUM_IMPORTANT_FEATURES} most important features:\\n\")\n",
    "for key, value in islice(feature_weights.items(), NUM_IMPORTANT_FEATURES):\n",
    "    print(f'{key}: {value}')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Convert the feature weights dictionary to a DataFrame.\n",
    "feature_weights_df = pd.DataFrame.from_dict(feature_weights, orient='index', columns=['feature_importances'])\n",
    "\n",
    "# Initialize a MinMaxScaler to normalize the feature weights to the range [0, 1].\n",
    "scaler = MinMaxScaler()\n",
    "\n",
    "# Normalize the feature weights.\n",
    "scaled_values = scaler.fit_transform(feature_weights_df[['feature_importances']])\n",
    "\n",
    "# Create a Series with normalized feature importance scores.\n",
    "feature_importances_ensemble = pd.Series(data=scaled_values.flatten(), index=feature_weights_df.index).sort_values(ascending=False)\n"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "is_executing": true
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Fit the random forest classifier on the datasets with and without feature selection and print the results.\n",
    "y_pred_with_fs_ensemble = compare_results_with_and_without_feature_selection(model=rfc, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, y_pred_without_fs=y_pred_without_fs, feature_importances=feature_importances_ensemble, num_important_features=NUM_IMPORTANT_FEATURES)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix without feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_without_fs, title='Ensemble Model - Confusion Matrix without Feature Selection', target_classes=target_classes,\n",
    "                     save_path=PLOTS_PATH + 'Ensemble Model Confusion Matrix without Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "is_executing": true
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix with feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_with_fs_ensemble, title='Ensemble Model - Confusion Matrix with Feature Selection', target_classes=target_classes,\n",
    "                     save_path=PLOTS_PATH + 'Ensemble Model Confusion Matrix with Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "is_executing": true
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Normalize the feature importance scores to the range [0, 1].\n",
    "important_features_ensemble = get_normalized_important_features(feature_importances=feature_importances_ensemble, feature_names=feature_names, num_important_features=NUM_IMPORTANT_FEATURES)\n",
    "\n",
    "# Save the feature importance scores to a pickle file.\n",
    "with open('../Feature-Importances/Feature-Importances-Ensemble-Model.pkl', 'wb') as f:\n",
    "    pickle.dump(feature_importances_ensemble, f)\n",
    "\n",
    "# Plot the feature importance scores in a bar graph.\n",
    "plot_feature_importances(feature_importances=important_features_ensemble, title='Ensemble Model - Visualizing Important Features', save_path=PLOTS_PATH + 'Ensemble Model Important Features.png')\n"
   ],
   "metadata": {
    "collapsed": false
   }
  }
 ],
 "metadata": {
  "accelerator": "TPU",
  "colab": {
   "provenance": []
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
