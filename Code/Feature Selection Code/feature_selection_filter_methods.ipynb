{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "This notebook performs feature selection using filter methods.\n",
    "The feature selection process involves selecting important features, plotting the feature importance scores, and saving the results for later use.\n",
    "The filter methods used in this notebook are ANOVA, Variance Threshold, and Mutual Information.\n",
    "\n",
    "This notebook has been created as part of a bachelor's thesis on the topic\n",
    "\"Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations\".\n",
    "The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.\n",
    "The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:\n",
    "https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.\n",
    "\n",
    "Caution: Please note that the code in this notebook does not include extensive input validation or error checking.\n",
    "The inputs and function parameters have been provided by the author in a controlled environment.\n",
    "Consider adding error checking and handling functionalities as needed.\n",
    "\n",
    "Disclaimer: This code is provided \"as-is\", without any express or implied warranty. In no event shall the author or\n",
    "contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.\n",
    "\n",
    "Author: Ahmad Eynawi\n",
    "Institute: Institute for Automation and Applied Informatics (IAI)\n",
    "Department: Department of Informatics\n",
    "University: Karlsruhe Institute of Technology (KIT)\n",
    "Date: April 10, 2024\n",
    "\"\"\""
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "outputs": [],
   "source": [
    "import pickle\n",
    "from sklearn.feature_selection import f_classif, VarianceThreshold, mutual_info_classif\n",
    "\n",
    "from ..constants import NUM_IMPORTANT_FEATURES, TRAINING_DATASET_PATH, TEST_DATASET_PATH, PLOTS_PATH\n",
    "\n",
    "from ..utility import plot_confusion_matrix, plot_feature_importances, get_normalized_important_features, compare_results_with_and_without_feature_selection, load_dataset, print_metadata_about_dataset\n",
    "\n",
    "%matplotlib inline\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings('ignore')\n",
    "\n",
    "# from google.colab import drive\n",
    "# drive.mount('/content/drive')\n"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "mKnIhOT8lt6T"
   },
   "source": [
    "# Datasets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 255
    },
    "id": "au8681T_R600",
    "outputId": "909996af-2a4c-44eb-e7e0-f8ef28847888",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Get input samples and target variables.\n",
    "X_train, y_train, X_test, y_test, feature_names, target_classes = load_dataset(train_set_path=TRAINING_DATASET_PATH, test_set_path=TEST_DATASET_PATH)\n",
    "\n",
    "# Print some metadata about the test and training datasets.\n",
    "print_metadata_about_dataset(X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Baseline Evaluation Model"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the random forest classifier to use it as a baseline evaluation model.\n",
    "with open('../Binary-Objects/Random-Forest-Classifier.pkl', 'rb') as f:\n",
    "    rfc = pickle.load(f)\n",
    "\n",
    "# Fit the random forest classifier on the training dataset with all features.\n",
    "rfc.fit(X_train, y_train)\n",
    "\n",
    "# Make predictions on the test dataset without feature selection.\n",
    "y_pred_without_fs = rfc.predict(X_test)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "# ANOVA"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "outputs": [],
   "source": [
    "# Calculate the ANOVA F-values which represent the feature importance scores.\n",
    "anova_f_values = f_classif(X_train, y_train)\n",
    "feature_importances_anova = anova_f_values[0]\n",
    "\n",
    "# Fit the random forest classifier on the datasets with and without feature selection and print the results.\n",
    "y_pred_with_fs_anova = compare_results_with_and_without_feature_selection(model=rfc, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, y_pred_without_fs=y_pred_without_fs, feature_importances=feature_importances_anova, num_important_features=NUM_IMPORTANT_FEATURES)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix without feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_without_fs, title='ANOVA - Confusion Matrix without Feature Selection', target_classes=target_classes, save_path=PLOTS_PATH + 'ANOVA Confusion Matrix without Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix with feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_with_fs_anova, title='ANOVA - Confusion Matrix with Feature Selection', target_classes=target_classes, save_path=PLOTS_PATH + 'ANOVA Confusion Matrix with Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Normalize the feature importance scores to the range [0, 1].\n",
    "important_features_anova = get_normalized_important_features(feature_importances=feature_importances_anova, feature_names=feature_names, num_important_features=NUM_IMPORTANT_FEATURES)\n",
    "\n",
    "# Save the feature importance scores to a pickle file.\n",
    "with open('../Feature-Importances/Feature-Importances-ANOVA.pkl', 'wb') as f:\n",
    "    pickle.dump(feature_importances_anova, f)\n",
    "\n",
    "# Plot the feature importance scores in a bar graph.\n",
    "plot_feature_importances(feature_importances=important_features_anova, title=\"ANOVA - Visualizing Important Features\", save_path=PLOTS_PATH + 'ANOVA Important Features.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "NYRxsVK2lbOj"
   },
   "source": [
    "# Variance Threshold"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 725
    },
    "id": "a8cK80lGEd-D",
    "outputId": "2ad30c2d-4915-4e28-c5a6-f984872900e0"
   },
   "outputs": [],
   "source": [
    "# Create a VarianceThreshold object to perform variance thresholding.\n",
    "selector = VarianceThreshold()\n",
    "\n",
    "# Perform variance thresholding.\n",
    "selector.fit_transform(X_train)\n",
    "\n",
    "# Get the feature importance scores.\n",
    "feature_importances_vs = selector.variances_\n",
    "\n",
    "# Fit the random forest classifier on the datasets with and without feature selection and print the results.\n",
    "y_pred_with_fs_vs = compare_results_with_and_without_feature_selection(model=rfc, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, y_pred_without_fs=y_pred_without_fs, feature_importances=feature_importances_vs, num_important_features=NUM_IMPORTANT_FEATURES)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix without feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_without_fs, title='Variance Threshold - Confusion Matrix without Feature Selection', target_classes=target_classes, save_path=PLOTS_PATH + 'Variance Threshold Confusion Matrix without Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix with feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_with_fs_vs, title='Variance Threshold - Confusion Matrix with Feature Selection', target_classes=target_classes, save_path=PLOTS_PATH + 'Variance Threshold Confusion Matrix with Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Normalize the feature importance scores to the range [0, 1].\n",
    "important_features_vs = get_normalized_important_features(feature_importances=feature_importances_vs, feature_names=feature_names, num_important_features=NUM_IMPORTANT_FEATURES)\n",
    "\n",
    "# Save the feature importance scores to a pickle file.\n",
    "with open('../Feature-Importances/Feature-Importances-Variance-Threshold.pkl', 'wb') as f:\n",
    "    pickle.dump(feature_importances_vs, f)\n",
    "\n",
    "# Plot the feature importance scores in a bar graph.\n",
    "plot_feature_importances(feature_importances=important_features_vs, title=\"Variance Threshold - Visualizing Important Features\", save_path=PLOTS_PATH + 'Variance Threshold Important Features.png')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "RT2Y2sMGlnTC"
   },
   "source": [
    "# Mutual Information"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 735
    },
    "id": "gT7pExkZWUoe",
    "outputId": "7d3afd53-5c04-4ccf-a6d7-38d5f9171a3f"
   },
   "outputs": [],
   "source": [
    "# Compute feature importance scores using the mutual information method.\n",
    "feature_importances_mi = mutual_info_classif(X_train, y_train, random_state=42)\n",
    "\n",
    "# Fit the random forest classifier on the datasets with and without feature selection and print the results.\n",
    "y_pred_with_fs_mi = compare_results_with_and_without_feature_selection(model=rfc, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, y_pred_without_fs=y_pred_without_fs, feature_importances=feature_importances_mi, num_important_features=NUM_IMPORTANT_FEATURES)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix without feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_without_fs, title='Mutual Information - Confusion Matrix without Feature Selection', target_classes=target_classes, save_path=PLOTS_PATH + 'Mutual Information Confusion Matrix without Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix with feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_with_fs_mi, title='Mutual Information - Confusion Matrix with Feature Selection', target_classes=target_classes, save_path=PLOTS_PATH + 'Mutual Information Confusion Matrix with Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Normalize the feature importance scores to the range [0, 1].\n",
    "important_features_mi = get_normalized_important_features(feature_importances=feature_importances_mi, feature_names=feature_names, num_important_features=NUM_IMPORTANT_FEATURES)\n",
    "\n",
    "# Save the feature importance scores to a pickle file.\n",
    "with open('../Feature-Importances/Feature-Importances-Mutual-Information.pkl', 'wb') as f:\n",
    "    pickle.dump(feature_importances_mi, f)\n",
    "\n",
    "# Plot the feature importance scores in a bar graph.\n",
    "plot_feature_importances(feature_importances=important_features_mi, title=\"Mutual Information - Visualizing Important Features\", save_path=PLOTS_PATH + 'Mutual Information Important Features.png')"
   ]
  }
 ],
 "metadata": {
  "accelerator": "TPU",
  "colab": {
   "provenance": []
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
