{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "This notebook performs feature selection using wrapper methods.\n",
    "The feature selection process involves selecting important features, plotting the feature importance scores, and saving the results for later use.\n",
    "The wrapper methods used in this notebook are Recursive Feature Elimination, Sequential Forward Feature Selection, Sequential Backward Feature Selection, and Exhaustive Feature Selection.\n",
    "\n",
    "This notebook has been created as part of a bachelor's thesis on the topic\n",
    "\"Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations\".\n",
    "The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.\n",
    "The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:\n",
    "https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.\n",
    "\n",
    "Caution: Please note that the code in this notebook does not include extensive input validation or error checking.\n",
    "The inputs and function parameters have been provided by the author in a controlled environment.\n",
    "Consider adding error checking and handling functionalities as needed.\n",
    "\n",
    "Disclaimer: This code is provided \"as-is\", without any express or implied warranty. In no event shall the author or\n",
    "contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.\n",
    "\n",
    "Author: Ahmad Eynawi\n",
    "Institute: Institute for Automation and Applied Informatics (IAI)\n",
    "Department: Department of Informatics\n",
    "University: Karlsruhe Institute of Technology (KIT)\n",
    "Date: April 10, 2024\n",
    "\"\"\""
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "import pickle\n",
    "from mlxtend.feature_selection import ExhaustiveFeatureSelector as EFS\n",
    "from mlxtend.feature_selection import SequentialFeatureSelector as SFS\n",
    "from sklearn.feature_selection import RFECV\n",
    "\n",
    "from ..constants import NUM_IMPORTANT_FEATURES, TRAINING_DATASET_PATH, TEST_DATASET_PATH, PLOTS_PATH\n",
    "\n",
    "from ..utility import plot_confusion_matrix, plot_feature_importances, get_normalized_important_features, compare_results_with_and_without_feature_selection, load_dataset, print_metadata_about_dataset\n",
    "\n",
    "%matplotlib inline\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings('ignore')\n",
    "\n",
    "# from google.colab import drive\n",
    "# drive.mount('/content/drive')"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "is_executing": true
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "id": "ADNLo-FIfF1j"
   },
   "outputs": [],
   "source": [
    "# Some constants.\n",
    "\n",
    "MIN_FEATURES_TO_SELECT = 1\n",
    "MAX_FEATURES_TO_SELECT = 3\n",
    "ACCURACY_METRIC = 'accuracy'\n",
    "NUM_CROSS_VALIDATIONS = 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "pVeL4yaKi_o2"
   },
   "source": [
    "# Datasets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 292
    },
    "id": "IWefdyoJeW-I",
    "outputId": "1f634eee-3450-4a3b-922a-35fd43015317",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Get input samples and target variables.\n",
    "X_train, y_train, X_test, y_test, feature_names, target_classes = load_dataset(train_set_path=TRAINING_DATASET_PATH, test_set_path=TEST_DATASET_PATH)\n",
    "\n",
    "# Print some metadata about the test and training datasets.\n",
    "print_metadata_about_dataset(X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test)\n",
    "\n",
    "# Get the number of features.\n",
    "NUM_FEATURES = X_train.shape[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Baseline Evaluation Model"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "outputs": [],
   "source": [
    "# Load the random forest classifier to use it as a baseline evaluation model.\n",
    "with open('../Binary-Objects/Random-Forest-Classifier.pkl', 'rb') as f:\n",
    "    rfc = pickle.load(f)\n",
    "\n",
    "# Fit the random forest classifier on the training dataset with all features.\n",
    "rfc.fit(X_train, y_train)\n",
    "\n",
    "# Make predictions on the test dataset.\n",
    "y_pred_without_fs = rfc.predict(X_test)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Recursive Feature Elimination"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Create an object for performing Recursive Feature Elimination (RFE) on the dataset.\n",
    "rfe = RFECV(estimator=rfc, step=1, min_features_to_select=MIN_FEATURES_TO_SELECT, cv=NUM_CROSS_VALIDATIONS, scoring=ACCURACY_METRIC, verbose=1, n_jobs=-1, importance_getter='auto')\n",
    "\n",
    "# Fit the RFE object on the dataset.\n",
    "rfe.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save the fitted RFE object.\n",
    "with open('../Binary-Objects/Recursive-Feature-Elimination-Feature-Selector.pkl', 'wb') as f:\n",
    "    pickle.dump(rfe, f)\n",
    "\n",
    "# Load the fitted RFE object.\n",
    "with open('../Binary-Objects/Recursive-Feature-Elimination-Feature-Selector.pkl', 'rb') as f:\n",
    "    rfe = pickle.load(f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Transform the training and test datasets such that they include only selected features.\n",
    "X_train_transformed_rfe = rfe.transform(X_train)\n",
    "X_test_transformed_rfe = rfe.transform(X_test)\n",
    "\n",
    "# Fit the random forest classifier on the training dataset with selected features.\n",
    "rfc.fit(X_train_transformed_rfe, y_train)\n",
    "\n",
    "# Get the feature importance scores.\n",
    "feature_importances_rfe = rfc.feature_importances_\n",
    "\n",
    "# Fit the random forest classifier on the datasets with and without feature selection and print the results.\n",
    "y_pred_with_fs_rfe = compare_results_with_and_without_feature_selection(model=rfc, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, y_pred_without_fs=y_pred_without_fs, feature_importances=feature_importances_rfe, num_important_features=NUM_IMPORTANT_FEATURES)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix without feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_without_fs,\n",
    "                      title='Recursive Feature Elimination - Confusion Matrix without Feature Selection',\n",
    "                      target_classes=target_classes,\n",
    "                      save_path=PLOTS_PATH + 'Recursive Feature Elimination Confusion Matrix without Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix with feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_with_fs_rfe,\n",
    "                      title='Recursive Feature Elimination - Confusion Matrix with Feature Selection',\n",
    "                      target_classes=target_classes,\n",
    "                      save_path=PLOTS_PATH + 'Recursive Feature Elimination Confusion Matrix with Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Normalize the feature importance scores to the range [0, 1].\n",
    "important_features_rfe = get_normalized_important_features(feature_importances=feature_importances_rfe,\n",
    "                                                          feature_names=feature_names,\n",
    "                                                          num_important_features=NUM_IMPORTANT_FEATURES)\n",
    "\n",
    "# Save the feature importance scores to a pickle file.\n",
    "with open('../Feature-Importances/Feature-Importances-Recursive-Feature-Elimination.pkl', 'wb') as f:\n",
    "    pickle.dump(feature_importances_rfe, f)\n",
    "\n",
    "# Plot the feature importance scores in a bar graph.\n",
    "plot_feature_importances(feature_importances=important_features_rfe,\n",
    "                         title='Recursive Feature Elimination - Visualizing Important Features',\n",
    "                         save_path=PLOTS_PATH + 'Recursive Feature Elimination Important Features.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "8rE7a_DFjKV9"
   },
   "source": [
    "# Sequential Forward Feature Selection"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "T7_PmwEchuna",
    "outputId": "21534e91-9e25-4c5c-bdb4-6fec68369f6c",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Create an object for performing Sequential Forward Feature Selection (SFS) on the dataset.\n",
    "sfs = SFS(estimator=rfc,\n",
    "          k_features='best',  # 'k_features' is set to 'best' to choose the best feature subset\n",
    "          forward=True,       # 'forward' is set to True to perform SFS\n",
    "          floating=False,\n",
    "          verbose=1,\n",
    "          scoring=ACCURACY_METRIC,\n",
    "          cv=NUM_CROSS_VALIDATIONS,\n",
    "          n_jobs=-1,\n",
    "          pre_dispatch='2*n_jobs',\n",
    "          clone_estimator=True,\n",
    "          fixed_features=None,\n",
    "          feature_groups=None)\n",
    "\n",
    "# Fit the SFS object on the dataset.\n",
    "sfs = sfs.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save the fitted SFS object.\n",
    "with open('../Binary-Objects/Sequential-Forward-Selection-Feature-Selector.pkl', 'wb') as f:\n",
    "    pickle.dump(sfs, f)\n",
    "\n",
    "# Load the fitted SFS object.\n",
    "with open('../Binary-Objects/Sequential-Forward-Selection-Feature-Selector.pkl', 'rb') as f:\n",
    "    sfs = pickle.load(f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "SNhWNw-MiDlv",
    "outputId": "e23ef15d-18d3-4f63-c3fe-1fa4b2693fcd",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Transform the training and test datasets such that they include only important features.\n",
    "X_train_transformed_sfs = sfs.transform(X_train)\n",
    "X_test_transformed_sfs = sfs.transform(X_test)\n",
    "\n",
    "# Fit the random forest classifier on the training dataset with selected features.\n",
    "rfc.fit(X_train_transformed_sfs, y_train)\n",
    "\n",
    "# Get the feature importance scores.\n",
    "feature_importances_sfs = rfc.feature_importances_\n",
    "\n",
    "# Fit the random forest classifier on the datasets with and without feature selection and print the results.\n",
    "y_pred_with_fs_sfs = compare_results_with_and_without_feature_selection(model=rfc, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, y_pred_without_fs=y_pred_without_fs, feature_importances=feature_importances_sfs, num_important_features=NUM_IMPORTANT_FEATURES)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix without feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_without_fs,\n",
    "                      title='Sequential Forward Feature Selection - Confusion Matrix without Feature Selection',\n",
    "                      target_classes=target_classes,\n",
    "                      save_path=PLOTS_PATH + 'Sequential Forward Feature Selection Confusion Matrix without Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix with feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_with_fs_sfs,\n",
    "                      title='Sequential Forward Feature Selection - Confusion Matrix with Feature Selection',\n",
    "                      target_classes=target_classes,\n",
    "                      save_path=PLOTS_PATH + 'Sequential Forward Feature Selection Confusion Matrix with Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Normalize the feature importance scores to the range [0, 1].\n",
    "important_features_sfs = get_normalized_important_features(feature_importances=feature_importances_sfs,\n",
    "                                                          feature_names=feature_names,\n",
    "                                                          num_important_features=NUM_IMPORTANT_FEATURES)\n",
    "\n",
    "# Save the feature importance scores to a pickle file.\n",
    "with open('../Feature-Importances/Feature-Importances-Sequential-Forward-Feature-Selection.pkl', 'wb') as f:\n",
    "    pickle.dump(feature_importances_sfs, f)\n",
    "\n",
    "# Plot the feature importance scores in a bar graph.\n",
    "plot_feature_importances(feature_importances=important_features_sfs,\n",
    "                         title='Sequential Forward Feature Selection - Visualizing Important Features',\n",
    "                         save_path=PLOTS_PATH + 'Sequential Forward Feature Selection Important Features.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "A0sj2fyPjQS4"
   },
   "source": [
    "# Sequential Backward Feature Selection"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "ojxCCYn4ilNq",
    "outputId": "b66d8d7e-8ac4-4149-ce62-544a94e2099f",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Create an object for performing Sequential Backward Selection (SFS) on the dataset.\n",
    "sbs = SFS(estimator=rfc,\n",
    "          k_features='best',  # 'k_features' is set to 'best' to choose the best feature subset\n",
    "          forward=False,      # 'forward' is set to 'False' to perform SBS\n",
    "          floating=False,\n",
    "          verbose=1,\n",
    "          scoring=ACCURACY_METRIC,\n",
    "          cv=NUM_CROSS_VALIDATIONS,\n",
    "          n_jobs=-1,\n",
    "          pre_dispatch='2*n_jobs',\n",
    "          clone_estimator=True,\n",
    "          fixed_features=None,\n",
    "          feature_groups=None)\n",
    "\n",
    "# Fit the SBS object on the dataset.\n",
    "sbs = sbs.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save the fitted SBS object.\n",
    "with open('../Binary-Objects/Sequential-Backward-Selection-Feature-Selector.pkl', 'wb') as f:\n",
    "    pickle.dump(sbs, f)\n",
    "\n",
    "# Load the fitted SBS object.\n",
    "with open('../Binary-Objects/Sequential-Backward-Selection-Feature-Selector.pkl', 'rb') as f:\n",
    "    sbs = pickle.load(f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "r0B41S9ri_yd",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Transform the training and test datasets such that they include only important features.\n",
    "X_train_transformed_sbs = sbs.transform(X_train)\n",
    "X_test_transformed_sbs = sbs.transform(X_test)\n",
    "\n",
    "# Fit the random forest classifier on the training dataset with selected features.\n",
    "rfc.fit(X_train_transformed_sbs, y_train)\n",
    "\n",
    "# Get the feature importance scores.\n",
    "feature_importances_sbs = rfc.feature_importances_\n",
    "\n",
    "# Fit the random forest classifier on the datasets with and without feature selection and print the results.\n",
    "y_pred_with_fs_sbs = compare_results_with_and_without_feature_selection(model=rfc, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, y_pred_without_fs=y_pred_without_fs, feature_importances=feature_importances_sbs, num_important_features=NUM_IMPORTANT_FEATURES)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix without feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_without_fs,\n",
    "                      title='Sequential Backward Feature Selection - Confusion Matrix without Feature Selection',\n",
    "                      target_classes=target_classes,\n",
    "                      save_path=PLOTS_PATH + 'Sequential Backward Feature Selection Confusion Matrix without Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix with feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_with_fs_sbs,\n",
    "                      title='Sequential Backward Feature Selection - Confusion Matrix with Feature Selection',\n",
    "                      target_classes=target_classes,\n",
    "                      save_path=PLOTS_PATH + 'Sequential Backward Feature Selection Confusion Matrix with Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Normalize the feature importance scores to the range [0, 1].\n",
    "important_features_sbs = get_normalized_important_features(feature_importances=feature_importances_sbs,\n",
    "                                                           feature_names=feature_names,\n",
    "                                                           num_important_features=NUM_IMPORTANT_FEATURES)\n",
    "\n",
    "# Save the feature importance scores to a pickle file.\n",
    "with open('../Feature-Importances/Feature-Importances-Sequential-Backward-Feature-Selection.pkl', 'wb') as f:\n",
    "    pickle.dump(feature_importances_sbs, f)\n",
    "\n",
    "# Plot the feature importance scores in a bar graph.\n",
    "plot_feature_importances(feature_importances=important_features_sbs,\n",
    "                         title='Sequential Backward Feature Selection - Visualizing Important Features',\n",
    "                         save_path=PLOTS_PATH + 'Sequential Backward Feature Selection Important Features.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "XI-8-SG7j4ts"
   },
   "source": [
    "# Exhaustive Feature Selection"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "kXCZBqJ5cho7",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Create an object for performing Exhaustive Feature Selection (EFS) on the dataset.\n",
    "efs = EFS(estimator=rfc,\n",
    "          min_features=MIN_FEATURES_TO_SELECT,\n",
    "          max_features=MAX_FEATURES_TO_SELECT,\n",
    "          print_progress=True,\n",
    "          scoring=ACCURACY_METRIC,\n",
    "          cv=NUM_CROSS_VALIDATIONS,\n",
    "          n_jobs=-1,\n",
    "          pre_dispatch='2*n_jobs',\n",
    "          clone_estimator=True,\n",
    "          fixed_features=None,\n",
    "          feature_groups=None)\n",
    "\n",
    "# Fit the EFS object on the dataset.\n",
    "efs = efs.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save the fitted EFS object.\n",
    "with open('../Binary-Objects/Exhaustive-Feature-Selection-Feature-Selector.pkl', 'wb') as f:\n",
    "    pickle.dump(efs, f)\n",
    "\n",
    "# Load the fitted EFS object.\n",
    "with open('../Binary-Objects/Exhaustive-Feature-Selection-Feature-Selector.pkl', 'rb') as f:\n",
    "    efs = pickle.load(f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "HaXXbkLEg7cF",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Transform the training and test datasets such that they include only important features.\n",
    "X_train_transformed_efs = efs.transform(X_train)\n",
    "X_test_transformed_efs = efs.transform(X_test)\n",
    "\n",
    "# Fit the random forest classifier on the training dataset with selected features.\n",
    "rfc.fit(X_train_transformed_efs, y_train)\n",
    "\n",
    "# Get the feature importance scores.\n",
    "feature_importances_efs = rfc.feature_importances_\n",
    "\n",
    "# Fit the random forest classifier on the datasets with and without feature selection and print the results.\n",
    "y_pred_with_fs_efs = compare_results_with_and_without_feature_selection(model=rfc, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, y_pred_without_fs=y_pred_without_fs, feature_importances=feature_importances_efs, num_important_features=NUM_IMPORTANT_FEATURES)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "id": "GCvEj9vQvJQY"
   },
   "outputs": [],
   "source": [
    "# Plot the confusion matrix without feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_without_fs,\n",
    "                      title='Exhaustive Feature Selection - Confusion Matrix without Feature Selection',\n",
    "                      target_classes=target_classes,\n",
    "                      save_path=PLOTS_PATH + 'Exhaustive Feature Selection Confusion Matrix without Feature Selection.png')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix with feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_with_fs_efs,\n",
    "                      title='Exhaustive Feature Selection - Confusion Matrix with Feature Selection',\n",
    "                      target_classes=target_classes,\n",
    "                      save_path=PLOTS_PATH + 'Exhaustive Feature Selection Confusion Matrix with Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Normalize the feature importance scores to the range [0, 1].\n",
    "important_features_efs = get_normalized_important_features(feature_importances=feature_importances_efs,\n",
    "                                                           feature_names=feature_names,\n",
    "                                                           num_important_features=NUM_IMPORTANT_FEATURES)\n",
    "\n",
    "# Save the feature importance scores to a pickle file.\n",
    "with open('../Feature-Importances/Feature-Importances-Exhaustive-Feature-Selection.pkl', 'wb') as f:\n",
    "    pickle.dump(feature_importances_efs, f)\n",
    "\n",
    "# Plot the feature importance scores in a bar graph.\n",
    "plot_feature_importances(feature_importances=important_features_efs,\n",
    "                         title='Exhaustive Feature Selection - Visualizing Important Features',\n",
    "                         save_path=PLOTS_PATH + 'Exhaustive Feature Selection Important Features.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "2lxTGVPGjHpR",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Compare the feature sets selected by the SFS, SBS and EFS algorithms.\n",
    "print('Features selected by SFS:', sfs.k_feature_names_)\n",
    "print('Features selected by SBS:', sbs.k_feature_names_)\n",
    "print('Features selected by EFS:', efs.best_feature_names_)"
   ]
  }
 ],
 "metadata": {
  "accelerator": "TPU",
  "colab": {
   "provenance": []
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
