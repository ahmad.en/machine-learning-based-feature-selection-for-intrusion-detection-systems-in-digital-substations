{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "This notebook performs feature selection using optimization methods.\n",
    "The feature selection process involves selecting important features, plotting the feature importance scores, and saving the results for later use.\n",
    "The optimization methods used in this notebook are Genetic Algorithm and Particle Swarm Optimization.\n",
    "\n",
    "This notebook has been created as part of a bachelor's thesis on the topic\n",
    "\"Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations\".\n",
    "The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.\n",
    "The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:\n",
    "https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.\n",
    "\n",
    "Caution: Please note that the code in this notebook does not include extensive input validation or error checking.\n",
    "The inputs and function parameters have been provided by the author in a controlled environment.\n",
    "Consider adding error checking and handling functionalities as needed.\n",
    "\n",
    "Disclaimer: This code is provided \"as-is\", without any express or implied warranty. In no event shall the author or\n",
    "contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.\n",
    "\n",
    "Acknowledgment: This notebook includes code adapted from the [PSO-Feature-Selection repository](https://github.com/muhammadshaffay/PSO-Feature-Selection) by Muhammad Shaffay.\n",
    "\n",
    "Author: Ahmad Eynawi\n",
    "Institute: Institute for Automation and Applied Informatics (IAI)\n",
    "Department: Department of Informatics\n",
    "University: Karlsruhe Institute of Technology (KIT)\n",
    "Date: April 10, 2024\n",
    "\"\"\""
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "outputs": [],
   "source": [
    "import pickle\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "from numpy import random\n",
    "from random import randint\n",
    "from sklearn import svm\n",
    "from sklearn.linear_model import LogisticRegression\n",
    "from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier\n",
    "from sklearn.tree import DecisionTreeClassifier\n",
    "from sklearn.neighbors import KNeighborsClassifier\n",
    "from sklearn.ensemble import GradientBoostingClassifier\n",
    "from sklearn.metrics import accuracy_score\n",
    "\n",
    "from ..constants import NUM_IMPORTANT_FEATURES, TRAINING_DATASET_PATH, TEST_DATASET_PATH, PLOTS_PATH\n",
    "\n",
    "from ..utility import plot_confusion_matrix, plot_feature_importances, get_normalized_important_features, compare_results_with_and_without_feature_selection, load_dataset, print_metadata_about_dataset, calculate_accuracy, plot_scores_across_generations\n",
    "\n",
    "%matplotlib inline\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")\n",
    "\n",
    "# from google.colab import drive\n",
    "# drive.mount('/content/drive')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "SY6dgJZxmQMc"
   },
   "source": [
    "# Datasets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "5-tRi848bVow",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Get input samples and target variables.\n",
    "X_train, y_train, X_test, y_test, feature_names, target_classes = load_dataset(train_set_path=TRAINING_DATASET_PATH, test_set_path=TEST_DATASET_PATH)\n",
    "\n",
    "# Print some metadata about the test and training datasets.\n",
    "print_metadata_about_dataset(X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test)\n",
    "\n",
    "# Get the number of features.\n",
    "NUM_FEATURES = X_train.shape[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Baseline Evaluation Model"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Load the random forest classifier to use it as a baseline evaluation model.\n",
    "with open('../Binary-Objects/Random-Forest-Classifier.pkl', 'rb') as f:\n",
    "    rfc = pickle.load(f)\n",
    "\n",
    "# Fit the random forest classifier on the training dataset with all features.\n",
    "rfc.fit(X_train, y_train)\n",
    "\n",
    "# Make predictions on the test dataset without feature selection.\n",
    "y_pred_without_fs = rfc.predict(X_test)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "Fq8O843LmU69"
   },
   "source": [
    "# Genetic Algorithm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "outputs": [],
   "source": [
    "# Some constants for the genetic algorithm.\n",
    "\n",
    "POPULATION_SIZE_GA = 20\n",
    "# POPULATION_SIZE = 60\n",
    "PARENTS_NUMBER_GA = 10\n",
    "NUM_GENERATIONS_GA = 4\n",
    "INITIAL_MUTATION_RATE_GA = 0.30\n",
    "MUTATION_RATE_GA = 0.20\n",
    "GENERATIONS = [1, 2, 3, 4]\n"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "id": "hgBGzbYgtYqD"
   },
   "outputs": [],
   "source": [
    "# Define a list of classifiers to compare their accuracies on the datasets.\n",
    "classifiers = ['LinearSVM', 'RadialSVM',\n",
    "               'Logistic', 'RandomForest',\n",
    "               'AdaBoost', 'DecisionTree',\n",
    "               'KNeighbors', 'GradientBoosting']\n",
    "\n",
    "# Define corresponding models for the classifiers.\n",
    "models = [svm.SVC(kernel='linear'),\n",
    "          svm.SVC(kernel='rbf'),\n",
    "          LogisticRegression(max_iter=50),\n",
    "          RandomForestClassifier(n_estimators=100, random_state=0),\n",
    "          AdaBoostClassifier(random_state=0),\n",
    "          DecisionTreeClassifier(random_state=0),\n",
    "          KNeighborsClassifier(),\n",
    "          GradientBoostingClassifier(random_state=0)]\n",
    "\n",
    "\n",
    "def calculate_classifiers_scores(X_train, y_train, X_test, y_test):\n",
    "    \"\"\"\n",
    "    Evaluate and compare the accuracies of different classifiers on the given datasets.\n",
    "\n",
    "    Parameters:\n",
    "    X_train (DataFrame): Training dataset.\n",
    "    y_train (Series): Training target labels.\n",
    "    X_test (DataFrame): Test dataset.\n",
    "    y_test (Series): Test target labels.\n",
    "\n",
    "    Returns:\n",
    "    DataFrame: DataFrame containing the classifiers and their corresponding accuracies, sorted by accuracy.\n",
    "    \"\"\"\n",
    "\n",
    "    scores = pd.DataFrame({\"Classifier\": classifiers})\n",
    "    accuracies = []\n",
    "\n",
    "    # Train each model and calculate accuracy.\n",
    "    for model in models:\n",
    "        model.fit(X_train, y_train)\n",
    "        predictions = model.predict(X_test)\n",
    "        accuracies.append(accuracy_score(y_test, predictions))\n",
    "\n",
    "    scores[\"Accuracy\"] = accuracies\n",
    "    scores.sort_values(by=\"Accuracy\", ascending=False, inplace=True)\n",
    "    scores.reset_index(drop=True, inplace=True)\n",
    "    return scores\n",
    "\n",
    "\n",
    "def initialize_population(population_size, features_number, init_mutation_rate=INITIAL_MUTATION_RATE_GA):\n",
    "    \"\"\"\n",
    "    Initialize the population for the genetic algorithm.\n",
    "\n",
    "    Parameters:\n",
    "    population_size (int): Number of chromosomes in the population.\n",
    "    features_number (int): Number of features in each chromosome.\n",
    "    init_mutation_rate (float): Initial mutation rate.\n",
    "\n",
    "    Returns:\n",
    "    list: List of initialized chromosomes.\n",
    "    \"\"\"\n",
    "\n",
    "    population = []\n",
    "    for i in range(population_size):\n",
    "        chromosome = np.ones(features_number, dtype=bool)\n",
    "        chromosome[:int(init_mutation_rate * features_number)] = False\n",
    "        np.random.shuffle(chromosome)\n",
    "        population.append(chromosome)\n",
    "    return population\n",
    "\n",
    "\n",
    "def calculate_fitness_score(population, X_train, y_train, X_test, y_test):\n",
    "    \"\"\"\n",
    "    Calculate the fitness score for each chromosome in the population.\n",
    "\n",
    "    Parameters:\n",
    "    population (list): List of chromosomes.\n",
    "    X_train (DataFrame): Training dataset.\n",
    "    y_train (Series): Training target labels.\n",
    "    X_test (DataFrame): Test dataset.\n",
    "    y_test (Series): Test target labels.\n",
    "\n",
    "    Returns:\n",
    "    tuple: Tuple containing the sorted scores and the sorted population.\n",
    "    \"\"\"\n",
    "\n",
    "    scores = []\n",
    "    for chromosome in population:\n",
    "        rfc.fit(X_train.iloc[:, chromosome], y_train)\n",
    "        predictions = rfc.predict(X_test.iloc[:, chromosome])\n",
    "        scores.append(accuracy_score(y_test, predictions))\n",
    "\n",
    "    # Sort scores and population in descending order.\n",
    "    scores, population = np.array(scores), np.array(population)\n",
    "    indices = np.argsort(scores)\n",
    "    return list(scores[indices][::-1]), list(population[indices, :][::-1])\n",
    "\n",
    "\n",
    "def select_best_chromosomes(population_after_fit, parents_number):\n",
    "    \"\"\"\n",
    "    Select the top chromosomes to be parents for the next generation.\n",
    "\n",
    "    Parameters:\n",
    "    population_after_fit (list): Population sorted by fitness scores.\n",
    "    parents_number (int): Number of parents to select.\n",
    "\n",
    "    Returns:\n",
    "    list: List of selected parent chromosomes.\n",
    "    \"\"\"\n",
    "\n",
    "    population_next_generation = []\n",
    "    for i in range(parents_number):\n",
    "        population_next_generation.append(population_after_fit[i])\n",
    "    return population_next_generation\n",
    "\n",
    "\n",
    "def perform_crossover(population_after_selection):\n",
    "    \"\"\"\n",
    "    Perform crossover operation to generate new offspring.\n",
    "\n",
    "    Parameters:\n",
    "    population_after_selection (list): Selected parent chromosomes.\n",
    "\n",
    "    Returns:\n",
    "    list: Population after crossover.\n",
    "    \"\"\"\n",
    "\n",
    "    population_next_generation = population_after_selection\n",
    "    for i in range(0, len(population_after_selection), 2):\n",
    "        child_1, child_2 = population_next_generation[i], population_next_generation[i+1]\n",
    "        new_parents = np.concatenate((child_1[:len(child_1)//2], child_2[len(child_1)//2:]))\n",
    "        population_next_generation.append(new_parents)\n",
    "    return population_next_generation\n",
    "\n",
    "\n",
    "def mutate_population(population_after_crossover, mutation_rate, features_number):\n",
    "    \"\"\"\n",
    "    Perform mutation operation on the population.\n",
    "\n",
    "    Parameters:\n",
    "    population_after_crossover (list): Population after crossover.\n",
    "    mutation_rate (float): Mutation rate.\n",
    "    features_number (int): Number of features in each chromosome.\n",
    "\n",
    "    Returns:\n",
    "    list: Population after mutation.\n",
    "    \"\"\"\n",
    "\n",
    "    mutation_range = int(mutation_rate * features_number)\n",
    "    population_next_generation = []\n",
    "    for n in range(0, len(population_after_crossover)):\n",
    "        chromosome = population_after_crossover[n]\n",
    "        random_positions = []\n",
    "        for i in range(0, mutation_range):\n",
    "            position = randint(0, features_number-1)\n",
    "            random_positions.append(position)\n",
    "        # Flip the values at the random positions.\n",
    "        for j in random_positions:\n",
    "            chromosome[j] = not chromosome[j]\n",
    "        population_next_generation.append(chromosome)\n",
    "    return population_next_generation\n",
    "\n",
    "\n",
    "def genetic_algorithm(population_size, features_number, parents_number, mutation_rate, generations_number, X_train, y_train, X_test, y_test):\n",
    "    \"\"\"\n",
    "    Run the genetic algorithm for a specified number of generations.\n",
    "\n",
    "    Parameters:\n",
    "    population_size (int): Number of chromosomes in the population.\n",
    "    features_number (int): Number of features in each chromosome.\n",
    "    parents_number (int): Number of parents to select.\n",
    "    mutation_rate (float): Mutation rate.\n",
    "    generations_number (int): Number of generations to run.\n",
    "    X_train (DataFrame): Training dataset.\n",
    "    y_train (Series): Training target labels.\n",
    "    X_test (DataFrame): Test dataset.\n",
    "    y_test (Series): Test target labels.\n",
    "\n",
    "    Returns:\n",
    "    tuple: Tuple containing the best chromosomes and their corresponding scores.\n",
    "    \"\"\"\n",
    "\n",
    "    best_chromosomes = []\n",
    "    best_scores = []\n",
    "    population_next_generation = initialize_population(population_size, features_number)\n",
    "\n",
    "    for i in range(generations_number):\n",
    "        scores, population_after_fit = calculate_fitness_score(population_next_generation, X_train, y_train, X_test, y_test)\n",
    "        population_after_selection = select_best_chromosomes(population_after_fit, parents_number)\n",
    "        population_after_crossover = perform_crossover(population_after_selection)\n",
    "        population_next_generation = mutate_population(population_after_crossover, mutation_rate, features_number)\n",
    "        best_chromosomes.append(population_after_fit[0])\n",
    "        best_scores.append(scores[0])\n",
    "    return best_chromosomes, best_scores\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Compute the accuracies for each classifier.\n",
    "classifier_scores = calculate_classifiers_scores(X_train, y_train, X_test, y_test)\n",
    "\n",
    "# Print each classifier and its accuracy.\n",
    "for index, row in classifier_scores.iterrows():\n",
    "    print(f\"{row['Classifier']}: {row['Accuracy']:.3f}%\")"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "YGUCPbfngE9o",
    "outputId": "bc260ea3-84cc-4b2e-c0b1-b8b3fda2642d"
   },
   "outputs": [],
   "source": [
    "# Run the genetic algorithm to compute the best feature sets.\n",
    "best_chromosomes, best_scores = genetic_algorithm(population_size=POPULATION_SIZE_GA, features_number=NUM_FEATURES, parents_number=PARENTS_NUMBER_GA, generations_number=NUM_GENERATIONS_GA, mutation_rate=MUTATION_RATE_GA, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "jADnkL_8izHL",
    "outputId": "6965d9e8-6b57-464e-80bb-f85ff1fec338",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Print the best scores in each generation.\n",
    "for index, score in enumerate(best_scores, start=1):\n",
    "    print('Best score in generation', index, ':', score)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "outputs": [],
   "source": [
    "with open('../Binary-Objects/Genetic-Algorithm-Best-Chromosomes.pkl', 'wb') as f:\n",
    "    pickle.dump(best_chromosomes, f)\n",
    "\n",
    "with open('../Binary-Objects/Genetic-Algorithm-Best-Scores.pkl', 'wb') as f:\n",
    "    pickle.dump(best_chromosomes, f)\n",
    "\n",
    "with open('../Binary-Objects/Genetic-Algorithm-Best-Chromosomes.pkl', 'rb') as f:\n",
    "    best_chromosomes = pickle.load(f)\n",
    "\n",
    "with open('../Binary-Objects/Genetic-Algorithm-Best-Scores.pkl', 'rb') as f:\n",
    "    best_scores = pickle.load(f)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "DwUfKpunpkXy",
    "outputId": "075d0d32-7d8c-4b14-e961-c12cd7719802",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Extract the names and indices of selected features. The indices of selected features are set to \"True\"\n",
    "# in the best chromosome.\n",
    "selected_features_ga = best_chromosomes[-1]\n",
    "selected_features_names_ga = feature_names[selected_features_ga]\n",
    "\n",
    "# Transform the datasets such that they include only important features.\n",
    "X_train_transformed_ga = X_train.iloc[:, selected_features_ga]\n",
    "X_test_transformed_ga = X_test.iloc[:, selected_features_ga]\n",
    "\n",
    "# Fit the random forest classifier on the training dataset with selected features.\n",
    "rfc.fit(X_train_transformed_ga, y_train)\n",
    "\n",
    "# Get the feature importance scores.\n",
    "feature_importances_ga = rfc.feature_importances_\n",
    "\n",
    "# Fit the random forest classifier on the datasets with and without feature selection and print the results.\n",
    "y_pred_with_fs_ga = compare_results_with_and_without_feature_selection(model=rfc, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, y_pred_without_fs=y_pred_without_fs, feature_importances=feature_importances_ga, num_important_features=NUM_IMPORTANT_FEATURES)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the accuracy evolution across generations.\n",
    "plot_scores_across_generations(scores=best_scores, generations=GENERATIONS, title='Genetic Algorithm - Accuracy Evolution across Generations', save_path=PLOTS_PATH + 'Genetic Algorithm Accuracy Evolution across Generations.png')"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "is_executing": true
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix without feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_without_fs, title='Genetic Algorithm - Confusion Matrix without Feature Selection', target_classes=target_classes, save_path=PLOTS_PATH + 'Genetic Algorithm Confusion Matrix without Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix with feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_with_fs_ga, title='Genetic Algorithm - Confusion Matrix with Feature Selection', target_classes=target_classes, save_path=PLOTS_PATH + 'Genetic Algorithm Confusion Matrix with Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "outputs": [],
   "source": [
    "# Normalize the feature importance scores to the range [0, 1].\n",
    "important_features_ga = get_normalized_important_features(feature_importances=feature_importances_ga, feature_names=feature_names, num_important_features=NUM_IMPORTANT_FEATURES)\n",
    "\n",
    "# Save the feature importance scores to a pickle file.\n",
    "with open('../Feature-Importances/Feature-Importances-Genetic-Algorithm.pkl', 'wb') as f:\n",
    "    pickle.dump(feature_importances_ga, f)\n",
    "\n",
    "# Plot the feature importance scores in a bar graph.\n",
    "plot_feature_importances(feature_importances=important_features_ga, title=\"Genetic Algorithm - Visualizing Important Features\", save_path=PLOTS_PATH + 'Genetic Algorithm Important Features.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "fLReuPm-jhEL"
   },
   "source": [
    "# Particle Swarm Optimization"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Some constants and variables for the particle swarm optimization algorithm.\n",
    "\n",
    "NUM_PARTICLES_PSO = 20\n",
    "NUM_GENERATIONS_PSO = 4\n",
    "FEATURE_SELECTION_THRESHOLD_PSO = 0.5\n",
    "MINIMUM_NUMBER_OF_ITERATIONS_PSO = 20\n",
    "MAX_NUM_CONTINUOUS_ACCURACIES_BEFORE_TERMINATION_PSO = 4\n",
    "\n",
    "velocities = np.random.randn(NUM_PARTICLES_PSO, NUM_FEATURES) * 0.4\n",
    "best_particle = []\n",
    "best_accuracy = 0\n",
    "accuracy_threshold = 0\n",
    "num_continuous_accuracies = 1\n",
    "last_best_accuracy = 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [],
   "source": [
    "def initialize_population():\n",
    "    \"\"\"\n",
    "    Initialize a single particle for the population.\n",
    "\n",
    "    Returns:\n",
    "    tuple: A tuple containing the particle and its accuracy.\n",
    "    \"\"\"\n",
    "\n",
    "    particle = random.randint(2, size=(NUM_FEATURES))\n",
    "    bool_particle = particle.astype(bool)\n",
    "\n",
    "    # Transform the training and test datasets based on the particle.\n",
    "    X_train_transformed = X_train.iloc[:, bool_particle]\n",
    "    X_test_transformed = X_test.iloc[:, bool_particle]\n",
    "\n",
    "    # Calculate the accuracy of the particle.\n",
    "    accuracy = calculate_accuracy(rfc, X_train_transformed, y_train, X_test_transformed, y_test)\n",
    "\n",
    "    return particle, accuracy\n",
    "\n",
    "\n",
    "def generate_population():\n",
    "    \"\"\"\n",
    "    Generate a population of particles and their corresponding accuracies.\n",
    "\n",
    "    Returns:\n",
    "    tuple: A tuple containing the list of particles and their accuracies.\n",
    "    \"\"\"\n",
    "\n",
    "    particles = []\n",
    "    accuracies = []\n",
    "\n",
    "    for i in range(NUM_PARTICLES_PSO):\n",
    "        particle, accuracy = initialize_population()\n",
    "        particles.append(particle)\n",
    "        accuracies.append(accuracy)\n",
    "\n",
    "    return particles, accuracies\n",
    "\n",
    "\n",
    "def sort_particles(particles, accuracies):\n",
    "    \"\"\"\n",
    "    Sort particles and accuracies in ascending order based on accuracies.\n",
    "\n",
    "    Parameters:\n",
    "    particles (list): List of particles.\n",
    "    accuracies (list): List of accuracies.\n",
    "\n",
    "    Returns:\n",
    "    tuple: Sorted particles and accuracies.\n",
    "    \"\"\"\n",
    "\n",
    "    # Zip accuracies and particles together and sort them by accuracies in ascending order.\n",
    "    sorted_pairs = sorted(zip(particles, accuracies), key=lambda x: x[1], reverse=False)\n",
    "\n",
    "    # Unzip the pairs back into two tuples.\n",
    "    sorted_particles, sorted_accuracies = zip(*sorted_pairs)\n",
    "\n",
    "    return list(sorted_particles), list(sorted_accuracies)\n",
    "\n",
    "\n",
    "def update_particles(current_particles, pbest, gbest):\n",
    "    \"\"\"\n",
    "    Update the particles' positions based on their personal best and global best positions.\n",
    "\n",
    "    Parameters:\n",
    "    current_particles (array): Current particles.\n",
    "    pbest (array): Personal best positions.\n",
    "    gbest (array): Global best position.\n",
    "\n",
    "    Returns:\n",
    "    array: Updated particles.\n",
    "    \"\"\"\n",
    "\n",
    "    c1 = 0.3\n",
    "    c2 = 0.7\n",
    "    w = 0.8\n",
    "    r1 = np.random.random(1)\n",
    "    r2 = np.random.random(1)\n",
    "    global velocities\n",
    "\n",
    "    # Update the velocity and position of all particles.\n",
    "    velocities = w * velocities + c1 * r1 * (pbest - current_particles) + c2 * r2 * (gbest - current_particles)\n",
    "    updated_particles = current_particles + velocities\n",
    "\n",
    "    return updated_particles\n",
    "\n",
    "\n",
    "def particle_swarm_optimization():\n",
    "    \"\"\"\n",
    "    Perform Particle Swarm Optimization (PSO) to select the best features.\n",
    "\n",
    "    Returns:\n",
    "    tuple: Best particles and their corresponding accuracies.\n",
    "    \"\"\"\n",
    "\n",
    "    best_particles = []\n",
    "    best_accuracies = []\n",
    "\n",
    "    # Generate initial particles and calculate accuracies.\n",
    "    particles, accuracies = generate_population()\n",
    "    particles, accuracies = sort_particles(particles, accuracies)\n",
    "    updated_particles = np.array(particles)\n",
    "    last_best_accuracy = accuracies[-1]\n",
    "    num_continuous_accuracies = 0\n",
    "\n",
    "    for iteration in range(NUM_GENERATIONS_PSO):\n",
    "        # Sort particles according to accuracies.\n",
    "        particles, accuracies = sort_particles(particles, accuracies)\n",
    "        print(f\"\\nBest accuracy in generation {iteration + 1}:\", accuracies[-1])\n",
    "\n",
    "        # Update particles.\n",
    "        particles = np.array(particles)\n",
    "        gbest = np.array(particles[-1])\n",
    "        updated_particles = update_particles(updated_particles, particles, gbest)\n",
    "        print(\"\\nUpdated particles before transformation:\\n\", updated_particles)\n",
    "\n",
    "        # Find maximum values in each particle.\n",
    "        max_values = []\n",
    "        for particle in updated_particles:\n",
    "            max_value = 0\n",
    "            for value in particle:\n",
    "                if max_value < value:\n",
    "                    max_value = value\n",
    "            max_values.append(max_value)\n",
    "\n",
    "        num_particles = len(particles)\n",
    "        num_features = len(particles[0])\n",
    "\n",
    "        # Divide each row by the respective maximum value.\n",
    "        for i in range(num_particles):\n",
    "            for j in range(num_features):\n",
    "                updated_particles[i][j] = updated_particles[i][j] / max_values[i]\n",
    "\n",
    "        # Convert values to 0 or 1 based on the threshold.\n",
    "        for i in range(num_particles):\n",
    "            for j in range(num_features):\n",
    "                if updated_particles[i][j] < FEATURE_SELECTION_THRESHOLD_PSO:\n",
    "                    updated_particles[i][j] = 0\n",
    "                elif updated_particles[i][j] >= FEATURE_SELECTION_THRESHOLD_PSO:\n",
    "                    updated_particles[i][j] = 1\n",
    "\n",
    "        print(\"\\nUpdated particles after transformation:\\n\", updated_particles)\n",
    "\n",
    "        # Calculate updated populations' accuracies.\n",
    "        updated_accuracies = []\n",
    "        for particle in updated_particles:\n",
    "            bool_particle = particle.astype(bool)\n",
    "            X_train_transformed = X_train.iloc[:, bool_particle]\n",
    "            X_test_transformed = X_test.iloc[:, bool_particle]\n",
    "            updated_accuracy = calculate_accuracy(rfc, X_train_transformed, y_train, X_test_transformed, y_test)\n",
    "            updated_accuracies.append(updated_accuracy)\n",
    "\n",
    "        # Store the particles with the best accuracies.\n",
    "        pbest_particles = []\n",
    "        pbest_accuracies = []\n",
    "\n",
    "        for i in range(num_particles):\n",
    "            if accuracies[i] <= updated_accuracies[i]:\n",
    "                pbest_particles.append(updated_particles[i])\n",
    "                pbest_accuracies.append(updated_accuracies[i])\n",
    "            else:\n",
    "                pbest_particles.append(particles[i])\n",
    "                pbest_accuracies.append(accuracies[i])\n",
    "\n",
    "        particles, accuracies = sort_particles(particles, accuracies)\n",
    "        best_particle = particles[-1]\n",
    "        particles = pbest_particles\n",
    "        accuracies = pbest_accuracies\n",
    "        best_accuracy = accuracies[-1]\n",
    "\n",
    "        # Terminate if the best accuracy did not improve for a predefined number of iterations.\n",
    "        if iteration > MINIMUM_NUMBER_OF_ITERATIONS_PSO:\n",
    "            if last_best_accuracy == best_accuracy:\n",
    "                num_continuous_accuracies += 1\n",
    "            else:\n",
    "                num_continuous_accuracies = 1\n",
    "\n",
    "            if num_continuous_accuracies > MAX_NUM_CONTINUOUS_ACCURACIES_BEFORE_TERMINATION_PSO:\n",
    "                print(f\"Optimization finished because of {MAX_NUM_CONTINUOUS_ACCURACIES_BEFORE_TERMINATION_PSO} continuous equal results!\")\n",
    "                break\n",
    "\n",
    "            last_best_accuracy = best_accuracy\n",
    "\n",
    "        # Terminate if the best accuracy is equal to or better than the predefined threshold.\n",
    "        if best_accuracy >= accuracy_threshold:\n",
    "            print(f\"Optimization finished because an accuracy threshold of {accuracy_threshold} has been reached!\")\n",
    "            break\n",
    "\n",
    "        best_particles.append(best_particle)\n",
    "        best_accuracies.append(best_accuracy)\n",
    "\n",
    "    return best_particles, best_accuracies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Run the particle swarm optimization algorithm to compute the best feature sets.\n",
    "best_particles, best_accuracies = particle_swarm_optimization()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Print the best scores in each generation.\n",
    "for index, accuracy in enumerate(best_accuracies, start=1):\n",
    "    print('Best accuracy in generation', index, ':', accuracy)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "with open('../Binary-Objects/Particle-Swarm-Optimization-Best-Particles.pkl', 'wb') as f:\n",
    "    pickle.dump(best_particles, f)\n",
    "\n",
    "with open('../Binary-Objects/Particle-Swarm-Optimization-Best-Accuracies.pkl', 'wb') as f:\n",
    "    pickle.dump(best_accuracies, f)\n",
    "\n",
    "with open('../Binary-Objects/Particle-Swarm-Optimization-Best-Accuracies.pkl', 'rb') as f:\n",
    "    best_particles = pickle.load(f)\n",
    "\n",
    "with open('../Binary-Objects/Particle-Swarm-Optimization-Best-Accuracies.pkl', 'rb') as f:\n",
    "    best_accuracies = pickle.load(f)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Extract the names and indices of selected features. The indices of selected features are set to \"True\" in the best particle.\n",
    "selected_features_pso = best_particles[-1].astype(bool)\n",
    "selected_features_names_pso = feature_names[selected_features_pso]\n",
    "best_accuracy = best_accuracies[-1]\n",
    "\n",
    "# Transform the datasets such that they include only important features.\n",
    "X_train_transformed_pso = X_train.iloc[:, selected_features_pso]\n",
    "X_test_transformed_pso = X_test.iloc[:, selected_features_pso]\n",
    "\n",
    "# Fit the random forest classifier on the training dataset with selected features.\n",
    "rfc.fit(X_train_transformed_pso, y_train)\n",
    "\n",
    "# Get the feature importance scores.\n",
    "feature_importances_pso = rfc.feature_importances_\n",
    "\n",
    "# Fit the random forest classifier on the datasets with and without feature selection and print the results.\n",
    "y_pred_with_fs_pso = compare_results_with_and_without_feature_selection(model=rfc, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, y_pred_without_fs=y_pred_without_fs, feature_importances=feature_importances_pso, num_important_features=NUM_IMPORTANT_FEATURES)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the accuracy evolution across generations.\n",
    "plot_scores_across_generations(scores=best_accuracies, generations=GENERATIONS, title='Particle Swarm Optimization - Accuracy Evolution across Generations', save_path=PLOTS_PATH + 'Particle Swarm Optimization Accuracy Evolution across Generations.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix without feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_without_fs, title='Particle Swarm Optimization - Confusion Matrix without Feature Selection', target_classes=target_classes, save_path=PLOTS_PATH + 'Particle Swarm Optimization Confusion Matrix without Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix with feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_with_fs_pso, title='Particle Swarm Optimization - Confusion Matrix with Feature Selection', target_classes=target_classes, save_path=PLOTS_PATH + 'Particle Swarm Optimization Confusion Matrix with Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Normalize the feature importance scores to the range [0, 1].\n",
    "important_features_pso = get_normalized_important_features(feature_importances=feature_importances_pso, feature_names=feature_names, num_important_features=NUM_IMPORTANT_FEATURES)\n",
    "\n",
    "# Save the feature importance scores to a pickle file.\n",
    "with open('../Feature-Importances/Feature-Importances-Particle-Swarm-Optimization.pkl', 'wb') as f:\n",
    "    pickle.dump(feature_importances_pso, f)\n",
    "\n",
    "# Plot the feature importance scores in a bar graph.\n",
    "plot_feature_importances(feature_importances=important_features_pso, title='Particle Swarm Optimization - Visualizing Important Features', save_path=PLOTS_PATH + 'Particle Swarm Optimization Important Features.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  }
 ],
 "metadata": {
  "accelerator": "TPU",
  "colab": {
   "provenance": []
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
