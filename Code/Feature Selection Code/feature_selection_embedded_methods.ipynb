{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "This notebook performs feature selection using embedded methods.\n",
    "The feature selection process involves selecting important features, plotting the feature importance scores, and saving the results for later use.\n",
    "The embedded methods used in this notebook are Random Forest, Extremely Randomized Trees, and Extreme Gradient Boosting.\n",
    "\n",
    "This notebook has been created as part of a bachelor's thesis on the topic\n",
    "\"Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations\".\n",
    "The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.\n",
    "The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:\n",
    "https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.\n",
    "\n",
    "Caution: Please note that the code in this notebook does not include extensive input validation or error checking.\n",
    "The inputs and function parameters have been provided by the author in a controlled environment.\n",
    "Consider adding error checking and handling functionalities as needed.\n",
    "\n",
    "Disclaimer: This code is provided \"as-is\", without any express or implied warranty. In no event shall the author or\n",
    "contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.\n",
    "\n",
    "Author: Ahmad Eynawi\n",
    "Institute: Institute for Automation and Applied Informatics (IAI)\n",
    "Department: Department of Informatics\n",
    "University: Karlsruhe Institute of Technology (KIT)\n",
    "Date: April 10, 2024\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "L8xBDqpQppNR",
    "outputId": "0d50fd39-0b03-41a8-8b7a-8b104eaf3f95",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import pickle\n",
    "from xgboost import XGBClassifier\n",
    "from sklearn.preprocessing import LabelEncoder\n",
    "from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier\n",
    "from scipy.stats import randint\n",
    "\n",
    "from ..constants import TARGET_COLUMN_NAME, NUM_IMPORTANT_FEATURES, RFC_N_ESTIMATORS, RFC_BOOTSTRAP, TRAINING_DATASET_PATH, TEST_DATASET_PATH, PLOTS_PATH\n",
    "\n",
    "from ..utility import plot_confusion_matrix, plot_feature_importances, get_normalized_important_features, compare_results_with_and_without_feature_selection, run_hyperparameter_grid_search, run_hyperparameter_randomized_search, load_dataset, print_metadata_about_dataset\n",
    "\n",
    "%matplotlib inline\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings('ignore')\n",
    "\n",
    "# from google.colab import drive\n",
    "# drive.mount('/content/drive')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "8mLXSmmjFHk2"
   },
   "source": [
    "# Datasets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 292
    },
    "id": "IWefdyoJeW-I",
    "outputId": "2e8b4902-4093-4f8e-e1be-27c9cf50f0be",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Get input samples and target variables.\n",
    "X_train, y_train, X_test, y_test, feature_names, target_classes = load_dataset(train_set_path=TRAINING_DATASET_PATH, test_set_path=TEST_DATASET_PATH)\n",
    "\n",
    "# Print some metadata about the test and training datasets.\n",
    "print_metadata_about_dataset(X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Baseline Evaluation Model"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Load the random forest classifier to use it as a baseline evaluation model.\n",
    "with open('../Binary-Objects/Random-Forest-Classifier.pkl', 'rb') as f:\n",
    "    rfc = pickle.load(f)\n",
    "\n",
    "# Fit the random forest classifier on the training dataset with all features.\n",
    "rfc.fit(X_train, y_train)\n",
    "\n",
    "# Make predictions on the test dataset without feature selection.\n",
    "y_pred_without_fs = rfc.predict(X_test)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Random Forest"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "H6-gLDyPFNoy"
   },
   "source": [
    "## Random Forest Classifier - Empirical Hyperparameter Tuning"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "IvEEzjP-Guko",
    "outputId": "ef929cbd-71d7-4fdb-8846-e2591a9590a8",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# This configuration turned empirically out to be the best configuration for the extremely randomized trees classifier.\n",
    "rfc = RandomForestClassifier(n_estimators=RFC_N_ESTIMATORS, bootstrap=RFC_BOOTSTRAP, random_state=42, n_jobs=-1)\n",
    "\n",
    "# Save the random forest classifier that showed the best performance. This classifier is used as a baseline for evaluating the performance of the feature selectors used in the ensemble model.\n",
    "with open('../Binary-Objects/Random-Forest-Classifier.pkl', 'wb') as f:\n",
    "    pickle.dump(rfc, f)\n",
    "\n",
    "# Get the feature importance scores.\n",
    "feature_importances_rfc = rfc.feature_importances_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Fit the random forest classifier on the datasets with and without feature selection and print the results.\n",
    "y_pred_with_fs_rfc = compare_results_with_and_without_feature_selection(model=rfc, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, y_pred_without_fs=y_pred_without_fs, feature_importances=feature_importances_rfc, num_important_features=NUM_IMPORTANT_FEATURES)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "is_executing": true
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix for the random forest classifier without feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_without_fs, title='Random Forest - Confusion Matrix without Feature Selection', target_classes=target_classes,\n",
    "                     save_path=PLOTS_PATH + 'Random Forest Confusion Matrix without Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "is_executing": true
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix for the random forest classifier with feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_with_fs_rfc, title='Random Forest - Confusion Matrix without Feature Selection', target_classes=target_classes,\n",
    "                     save_path=PLOTS_PATH + 'Random Forest Confusion Matrix without Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Normalize the feature importance scores to the range [0, 1].\n",
    "important_features_rfc = get_normalized_important_features(feature_importances=feature_importances_rfc, feature_names=feature_names, num_important_features=NUM_IMPORTANT_FEATURES)\n",
    "\n",
    "# Save the feature importance scores to a pickle file.\n",
    "with open('../Feature-Importances/Feature-Importances-Random-Forest.pkl', 'wb') as f:\n",
    "    pickle.dump(feature_importances_rfc, f)\n",
    "\n",
    "# Plot the feature importance scores in a bar graph.\n",
    "plot_feature_importances(feature_importances=important_features_rfc, title=\"Random Forest - Visualizing Important Features\", save_path=PLOTS_PATH + 'Random Forest Important Features.png')"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "is_executing": true
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Random Forest Classifier - Hyperparameter Grid Search\n"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "outputs": [],
   "source": [
    "# Perform a hyperparameter grid search for selecting a good random forest configuration.\n",
    "parameters_grid_rfc_gs = {\n",
    "    'n_estimators' : [93, 100, 120],\n",
    "    'min_samples_leaf': [1, 2, 5],\n",
    "    'max_features': ['sqrt', 'log2'],\n",
    "    'bootstrap': [True, False]\n",
    "}\n",
    "\n",
    "rfc_gs = run_hyperparameter_grid_search(model=RandomForestClassifier(), parameters_grid=parameters_grid_rfc_gs, X_train=X_train, y_train=y_train)\n",
    "\n",
    "# Grid Search - Tuned hyperparameters for the random forest classifier:\n",
    "# bootstrap: False\n",
    "# max_features: sqrt\n",
    "# min_samples_leaf: 2\n",
    "# n_estimators: 120\n",
    "\n",
    "# Save the tuned random forest classifier.\n",
    "with open('../Binary-Objects/Random-Forest-Classifier-Grid-Search.pkl', 'wb') as f:\n",
    "    pickle.dump(rfc_gs, f)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Random Forest Classifier - Hyperparameter Randomized Search"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "outputs": [],
   "source": [
    "# Perform a hyperparameter randomized search for selecting a good random forest configuration.\n",
    "parameters_grid_rfc_rs = {'n_estimators': randint(40, 150),\n",
    "              'criterion': ['gini', 'entropy', 'log_loss'],\n",
    "              'max_depth': randint(1, 20),\n",
    "              'max_features': ['sqrt', 'log2', None],\n",
    "              'bootstrap': [True, False],\n",
    "              'oob_score': [True, False],\n",
    "              'max_samples': np.arange(0.0, 1.0, 0.1)\n",
    "              }\n",
    "\n",
    "rfc_rs = run_hyperparameter_randomized_search(model=RandomForestClassifier(), parameters_grid=parameters_grid_rfc_rs, X_train=X_train, y_train=y_train)\n",
    "\n",
    "# Save the tuned random forest classifier.\n",
    "with open('../Binary-Objects/Random-Forest-Classifier-Randomized-Search.pkl', 'wb') as f:\n",
    "    pickle.dump(rfc_rs, f)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "94yKkuR5yH59"
   },
   "source": [
    "# Extremely Randomized Trees"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Extremely Randomized Trees - Empirical Hyperparameter Tuning"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# This configuration turned empirically out to be the best configuration for the extremely randomized trees classifier.\n",
    "etc = ExtraTreesClassifier(n_estimators=100, bootstrap=False, random_state=42, n_jobs=-1)\n",
    "\n",
    "with open('../Binary-Objects/Extremely-Randomized-Trees-Classifier.pkl', 'wb') as f:\n",
    "    pickle.dump(etc, f)\n",
    "\n",
    "with open('../Binary-Objects/Extremely-Randomized-Trees-Classifier.pkl', 'rb') as f:\n",
    "    etc = pickle.load(f)\n",
    "\n",
    "# Fit the model on the training dataset with all features.\n",
    "etc.fit(X_train, y_train)\n",
    "\n",
    "# Get the feature importance scores.\n",
    "feature_importances_etc = etc.feature_importances_"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Extremely Randomized Trees - Hyperparameter Grid Search"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Perform a hyperparameter grid search for selecting a good configuration for the extremely randomized trees classifier.\n",
    "parameters_grid_etc_gs = {\n",
    "    'n_estimators' : [95, 100, 200, 500],\n",
    "    'min_samples_leaf': [2, 5, 10, 20, 30],\n",
    "    'max_features': [10, 20, 30, 40, 50, 53]\n",
    "}\n",
    "\n",
    "etc_gs = run_hyperparameter_grid_search(model=ExtraTreesClassifier(), parameters_grid=parameters_grid_etc_gs, X_train=X_train, y_train=y_train)\n",
    "\n",
    "# Randomized Search - Tuned hyperparameters for the extremely randomized trees classifier:\n",
    "# max_features: 10\n",
    "# min_samples_leaf: 2\n",
    "# n_estimators: 95\n",
    "\n",
    "# Save the tuned extremely randomized trees classifier.\n",
    "with open('../Binary-Objects/Random-Forest-Classifier-Grid-Search.pkl', 'wb') as f:\n",
    "    pickle.dump(etc_gs, f)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "is_executing": true
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Fit the extremely randomized trees classifier on the datasets with and without feature selection and print the results.\n",
    "y_pred_with_fs_etc = compare_results_with_and_without_feature_selection(model=etc, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, y_pred_without_fs=y_pred_without_fs, feature_importances=feature_importances_etc, num_important_features=NUM_IMPORTANT_FEATURES)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix for the extremely randomized trees classifier without feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_without_fs, title='Extremely Randomized Trees - Confusion Matrix without Feature Selection', target_classes=target_classes,\n",
    "                     save_path=PLOTS_PATH + 'Extremely Randomized Trees Confusion Matrix without Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix for the extremely randomized trees classifier with feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_with_fs_etc, title='Extremely Randomized Trees - Confusion Matrix with Feature Selection', target_classes=target_classes,\n",
    "                     save_path=PLOTS_PATH + 'Extremely Randomized Trees Confusion Matrix with Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Normalize the feature importance scores to the range [0, 1].\n",
    "important_features_etc = get_normalized_important_features(feature_importances=feature_importances_etc, feature_names=feature_names, num_important_features=NUM_IMPORTANT_FEATURES)\n",
    "\n",
    "# Save the feature importance scores to a pickle file.\n",
    "with open('../Feature-Importances/Feature-Importances-Extremely-Randomized-Trees.pkl', 'wb') as f:\n",
    "      pickle.dump(feature_importances_etc, f)\n",
    "\n",
    "# Plot the feature importance scores in a bar graph.\n",
    "plot_feature_importances(feature_importances=important_features_etc, title=\"Extremely Randomized Trees - Visualizing Important Features\", save_path=PLOTS_PATH + 'Extremely Randomized Trees Important Features.png')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Extreme Gradient Boosting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Perform a hyperparameter grid search for selecting a good configuration for the extreme gradient boosting classifier.\n",
    "parameters_grid = {\n",
    "    'n_estimators' : [93, 100, 120],\n",
    "    'min_samples_leaf': [1, 2, 5],\n",
    "    'max_features': ['sqrt', 'log2'],\n",
    "    'bootstrap': [True, False]\n",
    "}\n",
    "\n",
    "xgbc_gs = run_hyperparameter_grid_search(model=XGBClassifier(), parameters_grid=parameters_grid, X_train=X_train, y_train=y_train)\n",
    "\n",
    "# Grid Search - Tuned hyperparameters for the extreme gradient boosting classifier:\n",
    "# bootstrap: False\n",
    "# max_features: sqrt\n",
    "# min_samples_leaf: 1\n",
    "# n_estimators: 150\n",
    "\n",
    "# Create an extreme gradient boosting classifier according to the results of the grid search.\n",
    "xgbc = XGBClassifier(n_estimators=150, bootstrap=False, max_features='sqrt', min_samples_leaf=1, n_jobs=-1)\n",
    "\n",
    "with open('../Binary-Objects/Extreme-Gradient-Boosting-Classifier.pkl', 'wb') as f:\n",
    "    pickle.dump(xgbc, f)\n",
    "\n",
    "with open('../Binary-Objects/Extreme-Gradient-Boosting-Classifier.pkl', 'rb') as f:\n",
    "    xgbc = pickle.load(f)\n",
    "\n",
    "# Fit the model on the training dataset with all features.\n",
    "xgbc.fit(X_train, y_train)\n",
    "\n",
    "# Get the feature importance scores.\n",
    "feature_importances_xgbc = xgbc.feature_importances_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "C0NaECVMiK4M",
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "# Encode the labels as the extreme gradient boosting classifier expects the labels to be integers.\n",
    "label_encoder = LabelEncoder()\n",
    "\n",
    "# Fit the encoder to the labels, then transform them to integers.\n",
    "label_encoder = label_encoder.fit(y_train)\n",
    "y_train = label_encoder.transform(y_train)\n",
    "y_train = pd.DataFrame(y_train, columns=[TARGET_COLUMN_NAME])\n",
    "y_test = label_encoder.transform(y_test)\n",
    "y_test = pd.DataFrame(y_test, columns=[TARGET_COLUMN_NAME])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Accuracy with the Extreme Gradient Boosting classifier: 1.0\n"
     ]
    }
   ],
   "source": [
    "# Fit the extreme gradient boosting classifier on the datasets with and without feature selection and print the results.\n",
    "y_pred_with_fs_xgbc = compare_results_with_and_without_feature_selection(model=xgbc, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, y_pred_without_fs=y_pred_without_fs, feature_importances=feature_importances_xgbc, num_important_features=NUM_IMPORTANT_FEATURES)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix for the extreme gradient boosting classifier without feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_without_fs, title='Extreme Gradient Boosting - Confusion Matrix without Feature Selection', target_classes=target_classes,\n",
    "                     save_path=PLOTS_PATH + 'Extreme Gradient Boosting Confusion Matrix without Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Plot the confusion matrix for the extreme gradient boosting classifier with feature selection.\n",
    "plot_confusion_matrix(y_test=y_test, y_pred=y_pred_with_fs_xgbc, title='Extreme Gradient Boosting - Confusion Matrix with Feature Selection', target_classes=target_classes, save_path=PLOTS_PATH + 'Extreme Gradient Boosting Confusion Matrix with Feature Selection.png')"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "is_executing": true
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Normalize the feature importance scores to the range [0, 1].\n",
    "important_features_xgbc = get_normalized_important_features(feature_importances=feature_importances_xgbc, feature_names=feature_names, num_important_features=NUM_IMPORTANT_FEATURES)\n",
    "\n",
    "# Save the feature importance scores to a pickle file.\n",
    "with open('../Feature-Importances/Feature-Importances-Extreme-Gradient-Boosting.pkl', 'wb') as f:\n",
    "      pickle.dump(etc.feature_importances, f)\n",
    "\n",
    "# Plot the feature importance scores in a bar graph.\n",
    "plot_feature_importances(feature_importances=important_features_xgbc, title=\"Extreme Gradient Boosting - Visualizing Important Features\",\n",
    "                        save_path=PLOTS_PATH + 'Extreme Gradient Boosting Important Features.png')"
   ],
   "metadata": {
    "collapsed": false
   }
  }
 ],
 "metadata": {
  "accelerator": "TPU",
  "colab": {
   "machine_shape": "hm",
   "provenance": []
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
