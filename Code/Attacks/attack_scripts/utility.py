"""
This utility module implements utility functions that are used across multiple attacks scripts in this package.
These include, for example, functions for sending and storing packets, searching for tags,
modifying strings, and converting between different data types.

This module has been created as part of a bachelor's thesis on the topic
"Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations".
The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.
The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:
https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.

Caution: Please note that the code in this module does not include extensive input validation or error checking.
The inputs and function parameters have been provided by the author in a controlled environment.
Consider adding error checking and handling functionalities as needed.

Disclaimer: This code is provided "as-is", without any express or implied warranty. In no event shall the author or
contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.

Author: Ahmad Eynawi
Institute: Institute for Automation and Applied Informatics (IAI)
Department: Department of Informatics
University: Karlsruhe Institute of Technology (KIT)
Date: April 10, 2024
"""


import binascii
from scapy.all import sendp, Raw, wrpcap, rdpcap, sniff, sendpfast
from scapy.layers.l2 import Ether
from scapy.layers.inet import TCP, IP
import time
import re
import struct
import numpy as np
from constants import HEXADECIMAL_BASE


def accept_packet(packet):
    """
    Accept the packet and forward it.

    Parameters:
    packet: The packet to be accepted.

    Returns:
    None
    """

    packet.accept()


def drop_packet(packet):
    """
    Drop the packet.

    Parameters:
    packet: The packet to be dropped.

    Returns:
    None
    """

    packet.drop()


def capture_packets(packets_count, network_interface, filter):
    """
    Capture a specified number of packets on a given network interface with a given filter.

    Parameters:
    packets_count (int): The number of packets to capture.
    network_interface (str): The network interface to capture packets from.
    filter (str): The filter to apply for capturing packets.

    Returns:
    scapy.plist.PacketList: A list of captured packets.
    """

    return sniff(count=packets_count, iface=network_interface, filter=filter)


def save_packets(path, packets):
    """
    Save the captured packets to a file.

    Parameters:
    path (str): The file path to save the packets to.
    packets (scapy.plist.PacketList): The packets to be saved.

    Returns:
    None
    """

    wrpcap(path, packets, append=False)


def load_packets(path):
    """
    Load packets from a file.

    Parameters:
    path (str): The file path to load the packets from.

    Returns:
    scapy.plist.PacketList: A list of loaded packets.
    """

    return rdpcap(path)


def send_packet(packet, network_interface):
    """
    Send a packet on a specified network interface.

    Parameters:
    packet (scapy.packet.Packet): The packet to be sent.
    network_interface (str): The network interface to send the packet on.

    Returns:
    None
    """

    sendp(packet, iface=network_interface)


def send_packets(packets, sample_rate, iterations, network_interface):
    """
    Send packets at a specified rate and for a specified number of iterations on a given network interface.

    Parameters:
    packets (scapy.plist.PacketList): The packets to be sent.
    sample_rate (int): The number of packets to send per second.
    iterations (int): The number of times to loop through the packet list.
    network_interface (str): The network interface to send the packets on.

    Returns:
    None
    """

    sendpfast(x=packets, pps=sample_rate, loop=iterations, iface=network_interface)


def replay_packets(packets, number_of_replays, delay_between_replays, sample_rate, network_interface):
    """
    Replay packets a specified number of times with a delay between each replay on a given network interface.

    Parameters:
    packets (scapy.plist.PacketList): The packets to be replayed.
    number_of_replays (int): The number of times to replay the packets.
    delay_between_replays (int): The delay between each replay in seconds.
    sample_rate (int): The number of packets to send per second.
    network_interface (str): The network interface to send the packets on.

    Returns:
    None
    """

    for _ in range(number_of_replays):
        time.sleep(delay_between_replays)
        sendpfast(x=packets, pps=sample_rate, iface=network_interface)


def inject_packets(packets, number_of_injections, delay_between_packets, network_interface):
    """
    Inject packets a specified number of times with a delay between each packet on a given network interface.

    Parameters:
    packets (scapy.plist.PacketList): The packets to be injected.
    number_of_injections (int): The number of times to inject the packets.
    delay_between_packets (int): The delay between each packet injection in seconds.
    network_interface (str): The network interface to inject the packets on.

    Returns:
    None
    """

    for _ in range(number_of_injections):
        for packet in packets:
            send_packet(packet=packet, network_interface=network_interface)
            time.sleep(delay_between_packets)


def flood_ethernet_bus(packets_count, destination_mac, network_interface, payload):
    """
    Flood the Ethernet bus with packets.

    Parameters:
    packets_count (int): The number of packets to send.
    destination_mac (str): The destination MAC address for the packets.
    network_interface (str): The network interface to send the packets on.
    payload (str): The payload of the packets.

    Returns:
    None
    """

    frame = Ether(dst=destination_mac) / Raw(load=payload)
    sendpfast(x=frame, loop=packets_count, iface=network_interface)


def save_measurements(measurements, path):
    """
    Save measurements to a file.

    Parameters:
    measurements (list): A list of measurements to be saved.
    path (str): The file path to save the measurements to.

    Returns:
    None
    """

    array_2d = np.array(measurements)
    with open(path, 'w') as file:
        for row in array_2d:
            file.write(' '.join(map(str, row)) + '\n')


def modify_packets_header(packets, change_mac, change_ip, source_mac, source_ip, destination_mac, destination_ip):
    """
    Modify the header of packets.

    Parameters:
    packets (scapy.plist.PacketList): The packets to be modified.
    change_mac (bool): Flag to indicate if MAC addresses should be changed.
    change_ip (bool): Flag to indicate if IP addresses should be changed.
    source_mac (str): The new source MAC address.
    source_ip (str): The new source IP address.
    destination_mac (str): The new destination MAC address.
    destination_ip (str): The new destination IP address.

    Returns:
    list: A list of modified packets.
    """

    modified_packets = []
    for packet in packets:
        modified_packet = set_metadata(packet=packet, change_mac=change_mac, change_ip=change_ip,
                                       source_mac=source_mac, source_ip=source_ip, destination_mac=destination_mac,
                                       destination_ip=destination_ip)
        modified_packets.append(modified_packet)

    return modified_packets


def set_metadata(packet, change_mac=False, change_ip=False, change_tcp=False, source_mac=None, destination_mac=None,
                 ethernet_type=None, source_ip=None, destination_ip=None, source_port=None, destination_port=None):
    """
    Set the metadata (MAC, IP, TCP) of a packet.
    Header parameters that are not provided to the function are set to their original values.

    Parameters:
    packet (scapy.packet.Packet): The packet to be modified.
    change_mac (bool): Flag to indicate if MAC addresses should be changed.
    change_ip (bool): Flag to indicate if IP addresses should be changed.
    change_tcp (bool): Flag to indicate if TCP ports should be changed.
    source_mac (str): The new source MAC address.
    destination_mac (str): The new destination MAC address.
    ethernet_type (int): The new Ethernet type.
    source_ip (str): The new source IP address.
    destination_ip (str): The new destination IP address.
    source_port (int): The new source TCP port.
    destination_port (int): The new destination TCP port.

    Returns:
    scapy.packet.Packet: The modified packet with updated metadata.
    """
    if change_mac:
        if Ether in packet:
            packet[Ether].src = source_mac if source_mac is not None else packet[Ether].src
            packet[Ether].dst = destination_mac if destination_mac is not None else packet[Ether].dst
            packet[Ether].type = ethernet_type if ethernet_type is not None else packet[Ether].type
    if change_tcp:
        if TCP in packet:
            packet[TCP].sport = source_port if source_port is not None else packet[TCP].sport
            packet[TCP].dport = destination_port if destination_port is not None else packet[TCP].dport
            del packet[IP].chksum
    if change_ip:
        if IP in packet:
            packet[IP].src = source_ip if source_ip is not None else packet[IP].src
            packet[IP].dst = destination_ip if destination_ip is not None else packet[IP].dst
            del packet[IP].len
            del packet[IP].chksum
    return packet


def set_load(packet, load):
    """
    Set the payload of a packet.

    Parameters:
    packet (scapy.packet.Packet): The packet to be modified.
    load (bytes): The new payload to set.

    Returns:
    scapy.packet.Packet: The modified packet with updated payload.
    """

    packet[Raw].load = load
    return packet


def reset_packet_length_and_checksums(packet):
    """
    Reset the length and checksums of a packet.

    Parameters:
    packet (scapy.packet.Packet): The packet to be modified.

    Returns:
    scapy.packet.Packet: The modified packet with recalculated length and checksums.
    """

    # Delete packet length and checksums such that they get recalculated by Scapy after modifying the packet.
    del packet[IP].len
    del packet[IP].chksum
    del packet[TCP].chksum
    return packet


def concatenate_arrays(array1, array2):
    """
    Concatenate two 2D arrays row-wise.

    Parameters:
    array1 (list): The first 2D array.
    array2 (list): The second 2D array.

    Returns:
    list: The concatenated 2D array.
    """

    concatenated_array = [array1_row + array2_row for array1_row, array2_row in zip(array1, array2)]
    return concatenated_array


def find_substring_index(string, substring):
    """
    Find the index of the first occurrence of a substring in a string.

    Parameters:
    string (str): The string to search in.
    substring (str): The substring to search for.

    Returns:
    int: The index of the first occurrence of the substring, or -1 if the substring is not found.
    """

    return string.find(substring)


def find_substring_indices(string, substring):
    """
    Find all indices of a substring in a string.

    Parameters:
    string (str): The string to search in.
    substring (str): The substring to search for.

    Returns:
    list: A list of indices where the substring is found.
    """

    indices = []
    start = 0
    while True:
        start = string.find(substring, start)
        if start == -1:
            break
        indices.append(start)
        # Move past the last found substring to look for further occurrences of the given substring.
        start += 1
    return indices


def extract_substring(string, start_tag, end_tag):
    """
    Extract a substring between two tags.

    Parameters:
    string (str): The string to extract the substring from.
    start_tag (str): The starting tag.
    end_tag (str): The ending tag.

    Returns:
    str: The extracted substring, or None if the substring is not found.
    """

    if (start_index := find_substring_index(string, start_tag)) != -1:
        if (end_index := find_substring_index(string, end_tag)) != -1:
            substring = string[start_index + 1: end_index]
        else:
            substring = string[start_index:]
        return substring
    return None


def replace_substring(string, start_index, end_index, new_substring):
    """
    Replace a substring in a string with a new substring.

    Parameters:
    string (str): The original string.
    start_index (int): The start index of the substring to be replaced (inclusive).
    end_index (int): The end index of the substring to be replaced (exclusive).
    new_substring (str): The new substring to insert.

    Returns:
    bytes: The modified string as bytes.
    """

    byte_string = bytearray(string)
    byte_string[start_index:end_index] = new_substring
    return bytes(byte_string)


def search_and_replace_substring(string, start_substring, end_substring, regex, new_substring):
    """
    Search and replace a substring in a string using a regular expression.

    Parameters:
    string (str): The original string.
    start_substring (str): The starting substring.
    end_substring (str): The ending substring.
    regex (str): The regular expression to find the text between the start and end substrings.
    new_substring (str): The new substring to replace with.

    Returns:
    str: The modified string.
    """

    # Use the regular expression to find the text between the start and the end substring.
    matches = re.findall(start_substring + regex + end_substring, string)
    # Replace the old substring with the new substring.
    new_load = string.replace(matches[0], new_substring)
    return new_load


def inject_substring(string, index, new_substring):
    """
    Inject a substring at a specified index in a string.

    Parameters:
    string (str): The original string.
    index (int): The index to inject the new substring at.
    new_substring (str): The new substring to inject.

    Returns:
    bytes: The modified string as bytes.
    """

    new_load = bytearray(string)
    new_load = new_load[:index] + new_substring + new_load[index:]
    return bytes(new_load)


def print_iterable(iterable, number_of_elements_to_print):
    """
    Print a specified number of elements from an iterable.

    Parameters:
    iterable (iterable): The iterable to print elements from.
    number_of_elements_to_print (int): The number of elements to print.

    Returns:
    None
    """

    for i in range(number_of_elements_to_print):
        print(iterable[i])


def convert_string_to_hex(string):
    """
    Convert a string to a hexadecimal representation.

    Parameters:
    string (str): The string to convert.

    Returns:
    bytes: The hexadecimal representation of the string.
    """

    return binascii.hexlify(bytes(string))


def convert_string_to_bytes(string):
    """
    Convert a string to bytes.

    Parameters:
    string (str): The string to convert.

    Returns:
    bytes: The bytes representation of the string.
    """

    return bytes(string)


def convert_string_to_number(string):
    """
    Convert a string to a number (int or float).

    Parameters:
    string (str): The string to convert.

    Returns:
    int or float: The converted number, or None if the conversion fails.
    """

    try:
        return int(string)
    except ValueError:
        try:
            return float(string)
        except ValueError:
            return None


def convert_integer_to_2_bytes(integer):
    """
    Convert an integer to a 2-byte hexadecimal representation.

    Parameters:
    integer (int): The integer to convert.

    Returns:
    bytes: The 2-byte hexadecimal representation of the integer.
    """

    if integer < 0:
        integer += 2**16
    hex_string = '{:04x}'.format(integer)
    hex_bytes = bytes.fromhex(hex_string)
    return hex_bytes


def convert_integer_to_4_bytes(integer):
    """
    Convert an integer to a 4-byte hexadecimal representation.

    Parameters:
    integer (int): The integer to convert.

    Returns:
    bytes: The 4-byte hexadecimal representation of the integer.
    """

    if integer < 0:
        integer += 2**32
    hex_string = '{:08x}'.format(integer)
    hex_bytes = bytes.fromhex(hex_string)
    return hex_bytes


def convert_integer_to_1_byte_hex(integer):
    """
    Convert an integer to a 1-byte hexadecimal string.

    Parameters:
    integer (int): The integer to convert.

    Returns:
    str: The 1-byte hexadecimal string representation of the integer.
    """

    if integer < 0:
        integer += 2**32
    hex_string = '{:02x}'.format(integer)
    return hex_string


def convert_object_to_hex(object):
    """
    Convert an object to a hexadecimal string.

    Parameters:
    object (object): The object to convert.

    Returns:
    str: The hexadecimal string representation of the object.
    """

    object_bytes = bytes(object)
    return object_bytes.hex()


def convert_object_to_bytes(object):
    """
    Convert an object to bytes.

    Parameters:
    object (object): The object to convert.

    Returns:
    bytes: The bytes representation of the object.
    """

    return bytes(object)


def convert_bytes_to_string(bytes_object):
    """
    Convert bytes to a string.

    Parameters:
    bytes_object (bytes): The bytes to convert.

    Returns:
    str: The string representation of the bytes.
    """

    return bytes_object.decode('ascii')


def convert_bytes_to_hex(bytes_object):
    """
    Convert bytes to a hexadecimal string.

    Parameters:
    bytes_object (bytes): The bytes to convert.

    Returns:
    str: The hexadecimal string representation of the bytes.
    """

    return bytes_object.hex()


def convert_bytes_to_packet(packet_bytes):
    """
    Convert bytes to a packet.

    Parameters:
    packet_bytes (bytes): The bytes to convert.

    Returns:
    scapy.packet.Packet: The packet representation of the bytes.
    """

    return IP(packet_bytes)


def convert_byte_to_boolean(tripping_signal_byte):
    """
    Convert a byte to a boolean value.

    Parameters:
    tripping_signal_byte (int): The byte to convert.

    Returns:
    bool: The boolean representation of the byte, or None if invalid.
    """

    if tripping_signal_byte == 1:
        return True
    elif tripping_signal_byte == 0:
        return False
    else:
        return None


def convert_hex_to_bytes(hex_string):
    """
    Convert a hexadecimal string to bytes.

    Parameters:
    hex_string (str): The hexadecimal string to convert.

    Returns:
    bytes: The bytes representation of the hexadecimal string.
    """

    return bytes.fromhex(hex_string)


def convert_hex_to_integer(hex_string):
    """
    Convert a hexadecimal string to an integer.

    Parameters:
    hex_string (str): The hexadecimal string to convert.

    Returns:
    int: The integer representation of the hexadecimal string.
    """

    return int(hex_string, HEXADECIMAL_BASE)


def convert_4_byte_hex_to_float(hex_number):
    """
    Convert a 4-byte hexadecimal string to a float.

    Parameters:
    hex_number (str): The 4-byte hexadecimal string to convert.

    Returns:
    float: The float representation of the 4-byte hexadecimal string.
    """

    # Convert the hexadecimal string to bytes.
    hex_bytes = convert_hex_to_bytes(hex_number)
    # Unpack the bytes to a floating-point number using big-endian format.
    float_number = struct.unpack('>f', hex_bytes)[0]
    # Round the floating-point number to 6 decimal places.
    rounded_number = round(float_number, 6)
    return rounded_number


def convert_float_to_4_byte_hex(float_number):
    """
    Convert a float to a 4-byte hexadecimal string.

    Parameters:
    float_number (float): The float to convert.

    Returns:
    str: The 4-byte hexadecimal string representation of the float.
    """

    # Pack the floating-point number into 4 bytes using 'f' format character for single precision float and '>' for big-endian.
    packed_float = struct.pack('>f', float_number)
    # Convert the bytes object to a hexadecimal string.
    hex_string = packed_float.hex()
    return hex_string
