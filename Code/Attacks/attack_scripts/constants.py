"""
This module stores constants that are used across multiple attack scripts in this package.

This module has been created as part of a bachelor's thesis on the topic
"Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations".
The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.
The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:
https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.

Disclaimer: This code is provided "as-is", without any express or implied warranty. In no event shall the author or
contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.

Author: Ahmad Eynawi
Institute: Institute for Automation and Applied Informatics (IAI)
Department: Department of Informatics
University: Karlsruhe Institute of Technology (KIT)
Date: April 10, 2024
"""


# IP and MAC addresses and network-related constants.
BROADCAST_MAC_ADDRESS = "ff:ff:ff:ff:ff:ff"
NETWORK_INTERFACE = "eth0"

# Ethernet type 0x800 corresponds to the protocol "IPv4".
ETHERNET_TYPE_IP_PROTOCOL = 0x0800

# One byte consists of two hexadecimal digits.
# Each hexadecimal character corresponds to one index in the payload of a packet.
HEX_DIGITS_IN_BYTE = 2

# Tripping threshold of the REL670 protection relay.
TRIPPING_THRESHOLD = 770

# Other global constants.
HEXADECIMAL_BASE = 16



