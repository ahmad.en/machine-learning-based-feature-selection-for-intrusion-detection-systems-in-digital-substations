"""
This module implements function for crafting and injecting frames (e.g., SV frames) into a network.

This module has been created as part of a bachelor's thesis on the topic
"Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations".
The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.
The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:
https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.

Caution: Please note that the code in this module does not include extensive input validation or error checking.
The inputs and function parameters have been provided by the author in a controlled environment.
Consider adding error checking and handling functionalities as needed.

Disclaimer: This code is provided "as-is", without any express or implied warranty. In no event shall the author or
contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.

Author: Ahmad Eynawi
Institute: Institute for Automation and Applied Informatics (IAI)
Department: Department of Informatics
University: Karlsruhe Institute of Technology (KIT)
Date: April 10, 2024
"""


from scapy.all import rdpcap, Raw, sendp
import pandas as pd
from scapy.layers.l2 import Ether
from constants import NETWORK_INTERFACE


# Names of columns in SV frames.
SOURCE_MAC_COLUMN_NAME = 'Source'
DESTINATION_MAC_COLUMN_NAME = 'Destination'
ETHER_TYPE_COLUMN = 'EtherType'
PAYLOAD_COLUMN = 'Payload'



def inject_data_interactively():
    """
    Interactively inject data by asking the user for the path of the data file and whether to send a single frame or all frames.

    Returns:
    None
    """

    send_frames = 'y'
    while send_frames != 'n':
        path = input("Enter the path:")
        option = int(input("Enter 1 to send one frame or 2 to send all frames:"))
        if option == 1:
            frame_index = int(input("Enter the frame index:"))
            send_data(path=path, index=frame_index)
        elif option == 2:
            send_data(path=path)

        send_frames = input("Do you want to continue sending frames? [y/n]")


def send_data(path, index=None):
    """
    Send data from the specified file path. The function supports both pcap and csv files.

    Parameters:
    path (str): The path to the data file.
    index (int, optional): The index of the frame to send. If None, all frames will be sent.

    Returns:
    None
    """

    file_extension = path.split('.')[-1].lower()

    if file_extension in ['pcap', 'pcapng']:
        send_pcap_data(path, index)

    elif file_extension == 'csv':
        send_csv_data(path, index)

    print("\n[+][+] Data sent successfully [+][+]\n")


def send_pcap_data(path, index=None):
    """
    Send data from a pcap file.

    Parameters:
    path (str): The path to the pcap file.
    index (int, optional): The index of the frame to send. If None, all frames will be sent.

    Returns:
    None
    """

    frames = rdpcap(path)

    if index is not None:
        frame = frames[index]
        print("\nFrame content:")
        frame.show()
        send_frame(frame)

    else:
        for frame in frames:
            print("\nFrame content:")
            frame.show()
            send_frame(frame)


def send_csv_data(path, index=None):
    """
    Send data from a csv file.

    Parameters:
    path (str): The path to the csv file.
    index (int, optional): The index of the frame to send. If None, all frames will be sent.

    Returns:
    None
    """

    csv_data = pd.read_csv(path)

    if index is not None:
        src_mac, dst_mac, ether_type, payload = extract_frame_data(csv_data.iloc[index])
        frame = craft_ethernet_frame(src_mac, dst_mac, ether_type, payload)
        print("\nFrame content:")
        frame.show()
        send_frame(frame)

    else:
        for _, row_data in csv_data.iterrows():
            src_mac, dst_mac, ether_type, payload = extract_frame_data(row_data)
            frame = craft_ethernet_frame(src_mac, dst_mac, ether_type, payload)
            print("\nFrame content:")
            frame.show()
            send_frame(frame)


def send_frame(frame, interface=NETWORK_INTERFACE):
    """
    Send a single frame over the network.

    Parameters:
    frame (scapy.layers.l2.Ether): The Ethernet frame to send.
    interface (str): The network interface to use for sending the frame. Defaults to 'eth0'.

    Returns:
    None
    """

    sendp(frame, iface=interface)


def extract_frame_data(csv_row):
    """
    Extract frame data from a row of a CSV file.

    Parameters:
    csv_row (pd.Series): A row from a pandas DataFrame containing frame data.

    Returns:
    tuple: A tuple containing the source MAC address, destination MAC address, Ethernet type, and payload.
    """

    src_mac = csv_row[SOURCE_MAC_COLUMN_NAME]
    dst_mac = csv_row[DESTINATION_MAC_COLUMN_NAME]
    ether_type = int(csv_row[ETHER_TYPE_COLUMN], 16)
    payload = csv_row[PAYLOAD_COLUMN]
    return src_mac, dst_mac, ether_type, payload


def craft_ethernet_frame(source_mac, destination_mac, ether_type, payload):
    """
    Craft an Ethernet frame.

    Parameters:
    source_mac (str): The source MAC address.
    destination_mac (str): The destination MAC address.
    ether_type (int): The Ethernet type.
    payload (str): The payload data.

    Returns:
    scapy.layers.l2.Ether: The crafted Ethernet frame.
    """

    ethernet_frame = Ether(src=source_mac, dst=destination_mac, type=ether_type) / Raw(load=payload)
    return ethernet_frame


def print_frame(src_mac, dst_mac, ether_type, payload):
    """
    Print the contents of an Ethernet frame.

    Parameters:
    src_mac (str): The source MAC address.
    dst_mac (str): The destination MAC address.
    ether_type (int): The Ethernet type.
    payload (str): The payload data.

    Returns:
    None
    """

    print(f"MAC source address: {src_mac}")
    print(f"MAC destination address: {dst_mac}")
    print(f"Ethernet type: {ether_type}")
    print(f"Payload: {payload}")


def get_frame_by_id(csv_data, target_id):
    """
    Retrieve a frame by its ID from a CSV file.

    Parameters:
    csv_data (pd.DataFrame): The DataFrame containing the CSV data.
    target_id (int): The ID of the target frame.

    Returns:
    tuple: A tuple containing the source MAC address, destination MAC address, Ethernet type, and payload, or None if the frame is not found.
    """

    target_row = csv_data[csv_data['No.'] == target_id]

    if not target_row.empty:
        src_mac = target_row[SOURCE_MAC_COLUMN_NAME].values[0]
        dst_mac = target_row[DESTINATION_MAC_COLUMN_NAME].values[0]
        ether_type = int(target_row[ETHER_TYPE_COLUMN].values[0], 16)
        payload = target_row[PAYLOAD_COLUMN].values[0]
        return src_mac, dst_mac, ether_type, payload

    else:
        print(f"Row with No. {target_id} not found!")
        return None, None, None, None



# Some tests.
print("*** Data Injection Attack ***")

csv_file_path = "../csv_files/dummy_ethernet_frames_data.csv"
send_data(path=csv_file_path, index=0)

pcap_file_path = "../pcap_files/sv/sv_example_packet.pcap"
send_data(path=pcap_file_path)
